#!/bin/sh
#############################
# Copyright (C) 2009 Catharsis. All Rights Reserved.
# Author: Joze Castellano
#############################

CONFIG_FILE=config.xml
OLD_PATH=`dirname $0`

echo $OLD_PATH

if [ -z $CIS_HOME ]
then
	if [ ! -f $OLD_PATH/$CONFIG_FILE ]
	then
		echo "CIS_HOME property not found. config.xml in current directory either. exiting..."
		exit "Exiting"
	else
		CIS_HOME=$OLD_PATH
	fi
fi

cd $CIS_HOME

CCDIR=$CC_HOME
JMXPORT=9999
RMIPORT=1099
#JAVA_OPTS=$JAVA_OPTS:-Ddashboard.config=/prod/cis/dashboard-config.xml

#export CC_OPTS = ${CC_OPTS}:-debug -ccname BiblioCC

echo "Checking CIS Structure"

echo "Checking for project Biblio_UnitTEST"
ant -buildfile build_CIS.xml bootstrap -Dbootstrap.rootDir=$CIS_HOME -Dbootstrap.projectName=Biblio_UnitTest -Dbootstrap.projectUrlSVN=svn://jozechu.homeip.net/repos/bibliotheca/trunk -Dbootstrap.svnUser=joze -Dbootstrap.svnPass=prueba

echo "Checking for project Biblio_Droid_Client"
ant -buildfile build_CIS.xml bootstrap -Dbootstrap.rootDir=$CIS_HOME -Dbootstrap.projectName=Biblio_Droid_Client -Dbootstrap.projectUrlSVN=svn://jozechu.homeip.net/repos/bibliotheca/trunk -Dbootstrap.svnUser=joze -Dbootstrap.svnPass=prueba

echo "Checking for project Biblio_Python_BAF"
ant -buildfile build_CIS.xml bootstrap -Dbootstrap.rootDir=$CIS_HOME -Dbootstrap.projectName=Biblio_Python_BAF -Dbootstrap.projectUrlSVN=svn://jozechu.homeip.net/repos/bibliotheca/trunk/development/python/BAF -Dbootstrap.svnUser=joze -Dbootstrap.svnPass=prueba

echo "Launching CruiseControl"
$CCDIR/main/bin/cruisecontrol.sh -ccname [BiblioCC] -jmxport $JMXPORT -rmiport $RMIPORT
