<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>[BiblioTheca] Master Server</title>
		<meta name="author" content="arimo">
		<meta name="description" content="Master server of BiblioTheca" >
		<meta name="keywords" content="BiblioTheca Master Server Biblio" >
		<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" >
		<meta http-equiv="Content-Script-Type" content="text/javascript" >
		<meta http-equiv="Content-Style-Type" content="text/css" >
	</head>
	<body alink="#648988" vlink="#a7cac9" link="#a7cac9" text="#b0bdd9" bgcolor="#6c7d87">
	<h1 align="center">
	BiblioTheca Master Server
	</h1>
<?php
	require_once("../xml/class.xml_saxy_lite_parser.php");
	require_once("../lib.stdio.php");
	
	class ServerListReader {
	
		/** Private members */
		private $m_SAXYParser;
		
		/**
		 * Parses a server list and prints all stuff out
		 */
		function ParseServerList( $file ) {
			$sXMLFile = stdio::loadFile( $file );
			$this->m_SAXYParser = new SAXY_Lite_Parser();
			$this->m_SAXYParser->xml_set_element_handler(array(&$this, "configOpenTag"), array(&$this, "configCloseTag"));
			$this->m_SAXYParser->xml_set_character_data_handler(array(&$this, "configNodeContent"));
			$this->m_SAXYParser->parse($sXMLFile);	
		}
		
		/**
		 * Checks if a server is responding or not
		 */
		function checkUrl($url) {
			//ini_set('default_socket_timeout', 7);
			//$a = file_get_contents($url,FALSE,NULL,0,20);
			//return ( ($a!= "") && ($http_response_header!= "") );
			//$url=$url;
  			//$file=file($url);
  			return false;
		}
		
		/**
		 * Found and open tag
		 */
		function configOpenTag($parser, $name, $attributes) 
		{
			// Set current tag
			if ( $name == "MasterServer" )
			{
				$ServerRow = "<h2>{SERVER_NAME}</h2>";
				$ServerRow .= "<ul type=\"square\">";
				$ServerRow .= "<li><b>Tag:</b> {SERVER_TAG}</li>";
				$ServerRow .= "<li><b>Host:</b> {SERVER_HOST}</li>";
				$ServerRow .= "<li><b>Status:</b> {SERVER_STATUS}</li>";
				$ServerRow .= "</ul>";
				$Host="";
				$HostDown="<span style=\"color:red\">Down</span>";
				$HostOnline="<span style=\"color:green\">Online</span>";
				while (list ($key, $value) = each ($attributes)) 
				{
					if ( $key == "name" )
					{
						$ServerRow = str_replace("{SERVER_NAME}", $value, $ServerRow);	
					}
					else if ( $key == "tag" )
					{
						$ServerRow = str_replace("{SERVER_TAG}", $value, $ServerRow);	
					}
					else if ( $key == "host" )
					{
						$Host = $value;
						$ServerRow = str_replace("{SERVER_HOST}", $value, $ServerRow);	
					}
					
				}
				if ( $Host != "" )
				{
					if ( $this->checkUrl( $Host ) )
					{
						$ServerRow = str_replace("{SERVER_STATUS}", $HostOnline, $ServerRow);
					}
					else
					{
						$ServerRow = str_replace("{SERVER_STATUS}", $HostDown, $ServerRow);
					}
				}
				else
				{
					$ServerRow = str_replace("{SERVER_STATUS}", $HostDown, $ServerRow);
				}
				echo $ServerRow;
			}
		}
		
		/**
		 * Close tag found
		 */	
		function configCloseTag($parser, $name) 
		{
		}
		
		/**
		 * Parse node data
		 */	
		function configNodeContent($parser, $text) 
		{
		}
			
	}
	
	$Reader = new ServerListReader();
	$Reader->ParseServerList("serverlist.xml");

?>
	<p align="center">
		<a href="../">Back</a>
	</p>
	</body>
</html>