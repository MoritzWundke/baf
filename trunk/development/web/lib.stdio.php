<?php
////////////////////////////////////////////////////////////////////////////*/
/* System: K-SyS, DarkCultureGames.com
/* Author: Moritz Wundke B_Thax@DarkCultureGames.com
/* Copyright (C) 2007 Moritz Wudnke. All rights reserved
/*
/* stdio Class File
/*
/* This file is the standart in/out libary
/*
/* License: 
/*	This file is part of the K-SyS distribution, share it under
/* 	the same license. (View K-SyS license for more information
/*	under www.DarkCultureGames.com)
/* 
/* CodeStandart: 
/*	sXXX => A string variable
/*	aXXX => An array
/*		asXXX => Array of Strings
////////////////////////////////////////////////////////////////////////////*/

// Verify including only once, because users do not have to use include/require _once

// Make shure we include the file only once
if(!defined('K_SYS_STDIO'))
{
	define('K_SYS_STDIO','TRUE');
	class stdio {
		/*===========================================================*\
		loadFile($sPath)
			- Loads a file and returns it as a string
			- sPath => Path of the file to load
		\*===========================================================*/	
		public static function loadFile($sPath)
		{
			if (is_file($sPath) && is_readable($sPath) && file_exists($sPath)) {
				$output="";
				$file = fopen($sPath, "r");
				while(!feof($file)) {
					$output = $output . fgets($file, 4096);
				}
				fclose ($file);
				return $output;
			}
		}
		
		/*===========================================================*\
		getFileExtension($sFilename)
			- Reads a filename and returns it's extension
			- sFilename => File name to look for
		\*===========================================================*/
		public static function getFileExtension($sFilename)
		{
			return strtolower(strrchr($sFilename,"."));
		}
		
		/*===========================================================*\
		changeExtension($sFilename, $sNewExtension)
			- Reads a filename and returns it but witch a changed extension
			- sFilename => File name to look for
			- $sNewExtension => The new extension for the file
		\*===========================================================*/
		public static function changeExtension($sFilename, $sNewExtension)
		{
			return substr( $sFilename, 0, strripos($sFilename, ".") ).".".$sNewExtension;
		}
		
		/*===========================================================*\
		highlightCode($sSource, $return=false)
			- Function higlights code with linenumbers, source can be a file path or
			  a string.
			- sSource => Could be a Path or a String
			- return => If false the higlited code will be printed directly,
			  if true it will be returned
		\*===========================================================*/	
		public static function highlightCode($sSource, $return=false) {
			if(is_file($sSource) && is_readable($sSource))
				$sSource = stdio::loadFile($sSource);
			$sSource = highlight_string($sSource,1);
			$sSource = explode("<br />", $sSource);
			$sOutPut = "<ol>\r\n";
			if (!$return) {
				echo $sOutPut;
				$sOutPut = '';
			}
			foreach ($sSource as $line) {
				$line = trim($line,"\r\n");
				$sOutPut .= "<li>{$line}</li>\r\n";
				if (!$return) {
					echo $sOutPut;
					$sOutPut = '';
				}
			}
			$sOutPut .= "</ol>\r\n";
			if (!$return) {
				echo $sOutPut;
				$sOutPut = true;
			}
			
			return $sOutPut;
		}
		
		/*===========================================================*\
		compile($sFile)
			- This funtions evaluates an entire .php file and returns
			  it as a string.
			- sFile => Path of to the external script
			- return => Script prompt
		\*===========================================================*/	
		public static function compile($sFile) {
			if(is_file($sFile) && is_readable($sFile) && file_exists($sFile)) {
				ob_start();
				require $sFile;
				return ob_get_clean();
			} else {
				return "$sFile: No such file found!!";
			}
		}
	}
}
?>