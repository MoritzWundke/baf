package com.catharsis.tools.google;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Activity;

/**
 * The GoogleAPI class has only static functions that enable
 * us to query some internal android functionality:
 *  - Google Account linked to current android instalation
 * @author Moritz Wundke
 *
 */
public class GoogleAPI {
	
	/**
	 * Calls google internal API to request the account name.</br>
	 * The Activity which handles the returned value should make use of the following code in it's OnActivityResult method:</br>
	 * <pre>
	 * if (requestCode == usedRerequestCode) {
	 *   String key = "accounts";
	 *   System.out.println(key + ":" + Arrays.toString(data.getExtras().getStringArray(key)));
	 *   String accounts[] = data.getExtras().getStringArray(key);
	 *   if (accounts[0] != null)
	 *     usernameText.setText("You are : " + accounts[0].replace("@gmail.com",""));
	 * }
	 * </pre>
	 * <p style='text-indent: 1em'>System.out.println(resultCode);</p>
	 * @param activity - Activity that will we requested once we got the user data
	 * @param rerequestCode - Request code used, so once the activity got the returned value it knows from who it came
	 * @return true if we send the request and false if we couldn't
	 */
	public static boolean getAccount(Activity activity, int rerequestCode) {
		try {
			for (Method ele: Class.forName("com.google.android.googleapps.GoogleLoginService" /*"com.google.android.googleapps.GoogleLoginServiceHelper"*/).getMethods())
			{
				System.out.println(ele.toString());
				try {
				if(ele.getName().equals("getAccount"))
				{
					ele.invoke(null, activity, rerequestCode, true);
					return true;
				}
				} catch (IllegalArgumentException e) {
				e.printStackTrace();
				} catch (IllegalAccessException e) {
				e.printStackTrace();
				} catch (InvocationTargetException e) {
				e.printStackTrace();
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
