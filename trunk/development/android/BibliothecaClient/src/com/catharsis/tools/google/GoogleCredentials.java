package com.catharsis.tools.google;

/**
 * Holds google Credentials, user name and password 
 * (password can only be accessed if we set the right permission in the manifest!)
 * @author Moritz Wundke
 *
 */
public class GoogleCredentials {
	public String GoogleUsername;
	public String GoogleEmail;
	public String GooglrPassword;
}
