package com.catharsis.biblio;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.app.ProgressDialog;

import com.catharsis.net.xmlrpc.XMLRPCClient;
import com.catharsis.tools.crypt.BiblioCrypt;

public class BiblioLoginActivity extends Activity {

	private ProgressDialog mLoginDialog;

	private final int HANDLE_TEST		= 0;
	private final int HANDLE_ERROR		= HANDLE_TEST+1;
	private final int HANDLE_CONNECT	= HANDLE_TEST+2;
	
	private final String TokenUser		= "[$USER]";
	private final String TokenIP		= "[$IP]";
	private final String TokenPort 		= "[$PORT]";
	private final String ConnectionString = "http://[$USER]:@[$IP]:[$PORT]/";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        
        setContentView(R.layout.login);
        ((Button) findViewById(R.id.Login_Btn_SignIn)).setOnClickListener(mLoginClickListener);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.basic , menu);
        inflater.inflate(R.menu.account, menu);
        //inflater.inflate(R.menu.login, menu);

        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	int ID = item.getItemId();
        switch (ID) {
        	case R.id.MenuBasic_Open:
        		setContentView(R.layout.main);
            return true;
		case R.id.MenuAccount_Create:
                return true;
		case R.id.MenuLogin_Open:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    /** OnClickListener Callbacks. */
    OnClickListener mLoginClickListener = new OnClickListener(){

		public void onClick(View v) {
			// TODO Auto-generated method stub
			// Display an indeterminate Progress-Dialog
			mLoginDialog = ProgressDialog.show(BiblioLoginActivity.this,     
                      							"Connecting...", 
                      							"Talking with server...", 
                      							false);
            	
            Thread thread = new Thread(null,BackgroundLoginWorker, "BackgroundLogin");
            thread.start();
		}
    };
    
    
    private Handler mHandler = new Handler() {
    	
    	public void handleMessage(Message msg) {
    		
    		// Remove dialog
    		mLoginDialog.dismiss();

    		switch (msg.what) {
            	case HANDLE_TEST:
            		((TextView)findViewById(R.id.ConnectionStatus)).setText((String) msg.obj);
            		break;
            	case HANDLE_CONNECT:
            		//TODO: Fetch xmlrpc Client Object to the main Activity. 
            		((TextView)findViewById(R.id.ConnectionStatus)).setText(R.string.ConnectionStatusSucess);
            		break;
            	case HANDLE_ERROR:
            		((TextView)findViewById(R.id.ConnectionStatus)).setText((String) msg.obj);
            		break;
          }
          super.handleMessage(msg);
        }
      };
    
    private Runnable BackgroundLoginWorker = new Runnable() {
    	public void run () {
			try {
				// Manage connection, handle errors and report to this own view.
				String user = ((EditText)findViewById(R.id.Login_Edt_Username)).getText().toString();
				String passPlain = ((EditText)findViewById(R.id.Login_Edt_Password)).getText().toString();
				String serverAddress = ((EditText)findViewById(R.id.Login_Edt_ServerAddress)).getText().toString();
				String serverPort = ((EditText)findViewById(R.id.Login_Edt_Port)).getText().toString();
				
				//Encrypt Pass
				String passCrypt = BiblioCrypt.makeMD5(passPlain);
				
				//Compose Conection String
				String myConnectString = ConnectionString.replace(TokenUser, user);
				myConnectString = myConnectString.replace(TokenIP, serverAddress);
				myConnectString = myConnectString.replace(TokenPort, serverPort);
				
				// Create resources ( xlmrpc ... )
				XMLRPCClient client = new XMLRPCClient(myConnectString);
				
				// Trying to authenticate (do we need this?)
				client.setBasicAuthentication(user, passPlain);
				
				//Authenticating with ...
				ArrayList<Object> a = (ArrayList<Object>) client.call("login", user, passPlain);

				for (int i=0; i<a.size();i++){
					
					Object currObj = (Object) a.get(i);
					if (currObj != null) {
						
						String clssObj = currObj.getClass().toString();
					}
				}
				// Create result Message and pass it to the mHandler, It will fetch the client properly.
				Message msg = new Message();
				msg.obj = client;
				msg.what = HANDLE_CONNECT;
				mHandler.sendMessage(msg);
				
				//TODO: CONDITIONAL COMPILATION OR DEBUG FLAG
				if ( true ) {
					String result = (String)client.call("login", "0", user, passCrypt);

					Message msgTest = new Message();
					msgTest.obj = result;
					msgTest.what = HANDLE_TEST;
					mHandler.sendMessage(msgTest);
				}
				
			} catch (Exception e) {
				//CLEAR ALL RESOURCES
				Message msgError = new Message();
				msgError.obj = e.getCause();
				msgError.what = HANDLE_ERROR;
				mHandler.sendMessage(msgError);
			}
    	}
    };
}
