package com.catharsis.biblio;

import java.util.Arrays;

import com.catharsis.tools.google.GoogleAPI;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Bibliotheca extends Activity {
	
	private Button LoginButton;
	private boolean bIsConnected;
	private boolean bAutoLogin;
	
	private static int GOOGLE_ACOUNT_REQUEST_CODE = 123;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
        this.LoginButton = (Button)this.findViewById(R.id.Button01);
        this.LoginButton.setOnClickListener(LoginClickListener);
        
        this.bAutoLogin	  = false;
        this.bIsConnected = false;
        
        System.out.println("jodoooooooooo");
        
        // Get google acount
        GoogleAPI.getAccount(this, GOOGLE_ACOUNT_REQUEST_CODE);
    }

    /** TODO: OnClickListener - Dirty way to implement. Refactor It */
    OnClickListener LoginClickListener = new OnClickListener(){

		public void onClick(View v) {
			// TODO Auto-generated method stub
			StartLoginActivity(v);
		}
    };
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
		if (requestCode == GOOGLE_ACOUNT_REQUEST_CODE) {
			String key = "accounts";
			System.out.println(key + ":" + Arrays.toString(data.getExtras().getStringArray(key)));
			String accounts[] = data.getExtras().getStringArray(key);
			if (accounts[0] != null)
			{
				this.LoginButton.setText(accounts[0].replace("@gmail.com",""));
			}
		}
    }
    
    @Override
    public void onStart() {
    	
    	super.onStart();
    	//Check Connection Status and send us to LoginActivity
    	if (this.bAutoLogin && !this.bIsConnected) {
    		StartLoginActivity(this.findViewById(R.id.ViewMain));
    	}
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.basic , menu);
        inflater.inflate(R.menu.account, menu);
        inflater.inflate(R.menu.login, menu);

        return true;
    }
    
    /**
     * Called right before your activity's option menu is displayed.
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	int ID = item.getItemId();
        switch (ID) {
		case R.id.MenuAccount_Create:
                return true;
		case R.id.MenuLogin_Open:
                StartLoginActivity(findViewById(R.id.ViewMain));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void StartLoginActivity(View v){
    	
    	Intent NewActivity = new Intent(v.getContext(), BiblioLoginActivity.class);
        startActivityForResult(NewActivity, 0);
    	
    }
}