package com.catharsis.biblio.test;

import junit.framework.TestSuite;
import android.test.InstrumentationTestRunner;
//import android.test.InstrumentationTestSuite;

public class CustomInstrumentationTestRunner extends InstrumentationTestRunner {

    @Override
    public TestSuite getAllTests() {

//        InstrumentationTestSuite suite = new InstrumentationTestSuite(this);
//
//        suite.addTestSuite(FocusAfterRemovalTest.class);
//        suite.addTestSuite(RequestFocusTest.class);
//        suite.addTestSuite(RequestRectangleVisibleTest.class);
//        return suite;


    	AllTests theSuite = new AllTests();
    	
    	//theSuite.addTestSuite(theSuite.suite());
    	return (TestSuite) theSuite;
    }

    @Override
    public ClassLoader getLoader() {
        return CustomInstrumentationTestRunner.class.getClassLoader();
    }
}
