--
-- PostgreSQL database dump
--

-- Started on 2009-08-02 21:24:35 CEST

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 1853 (class 1262 OID 16385)
-- Name: biblio_content; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE biblio_content WITH TEMPLATE = template0 ENCODING = 'UTF8';


ALTER DATABASE biblio_content OWNER TO postgres;

\connect biblio_content

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1508 (class 1259 OID 16401)
-- Dependencies: 3
-- Name: biblioMedia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioMedia" (
    id bigint NOT NULL,
    "Name" text NOT NULL,
    "Description" text,
    "PublisherProfile_id" bigint NOT NULL,
    "MediaCategory_id" bigint NOT NULL
);


ALTER TABLE public."biblioMedia" OWNER TO postgres;

--
-- TOC entry 1856 (class 0 OID 0)
-- Dependencies: 1508
-- Name: TABLE "biblioMedia"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioMedia" IS 'Or Media, this table holds the media ''meta-data'', like the name, description, ... The content of this media is located in a separate table. Apart from it''s content, a media es related directly to a user profile which acts like it''s publisher.';


--
-- TOC entry 1507 (class 1259 OID 16399)
-- Dependencies: 1508 3
-- Name: BiblioMedia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "BiblioMedia_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."BiblioMedia_id_seq" OWNER TO postgres;

--
-- TOC entry 1857 (class 0 OID 0)
-- Dependencies: 1507
-- Name: BiblioMedia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "BiblioMedia_id_seq" OWNED BY "biblioMedia".id;


--
-- TOC entry 1515 (class 1259 OID 16538)
-- Dependencies: 3
-- Name: biblioMediaCategory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioMediaCategory" (
    id bigint NOT NULL,
    "Name" text NOT NULL,
    "Description" text,
    "SubCategory_id" bigint
);


ALTER TABLE public."biblioMediaCategory" OWNER TO postgres;

--
-- TOC entry 1858 (class 0 OID 0)
-- Dependencies: 1515
-- Name: TABLE "biblioMediaCategory"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioMediaCategory" IS 'A simple tree-like table to represent a tree-structured category hierarchy.';


--
-- TOC entry 1516 (class 1259 OID 16541)
-- Dependencies: 1515 3
-- Name: biblioMediaCategory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioMediaCategory_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioMediaCategory_id_seq" OWNER TO postgres;

--
-- TOC entry 1859 (class 0 OID 0)
-- Dependencies: 1516
-- Name: biblioMediaCategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioMediaCategory_id_seq" OWNED BY "biblioMediaCategory".id;


--
-- TOC entry 1512 (class 1259 OID 16468)
-- Dependencies: 3
-- Name: biblioMediaContent; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioMediaContent" (
    id bigint NOT NULL,
    partid bigint NOT NULL,
    size bigint NOT NULL,
    ownerid bigint NOT NULL,
    data text NOT NULL
);


ALTER TABLE public."biblioMediaContent" OWNER TO postgres;

--
-- TOC entry 1860 (class 0 OID 0)
-- Dependencies: 1512
-- Name: TABLE "biblioMediaContent"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioMediaContent" IS 'This table holds content of our media, every row is not an entire set of content data, it''s just splitted content. So to gather a whole media file we need to pickup all it''s parts';


--
-- TOC entry 1511 (class 1259 OID 16466)
-- Dependencies: 1512 3
-- Name: biblioMediaContent_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioMediaContent_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioMediaContent_id_seq" OWNER TO postgres;

--
-- TOC entry 1861 (class 0 OID 0)
-- Dependencies: 1511
-- Name: biblioMediaContent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioMediaContent_id_seq" OWNED BY "biblioMediaContent".id;


--
-- TOC entry 1520 (class 1259 OID 16592)
-- Dependencies: 3
-- Name: biblioMediaListReview; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioMediaListReview" (
    id bigint NOT NULL,
    "UserProfile_id" bigint NOT NULL,
    "Media_id" bigint NOT NULL,
    "Rating" integer NOT NULL,
    "Review" text NOT NULL
);


ALTER TABLE public."biblioMediaListReview" OWNER TO postgres;

--
-- TOC entry 1862 (class 0 OID 0)
-- Dependencies: 1520
-- Name: TABLE "biblioMediaListReview"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioMediaListReview" IS 'Same as the media review table but in a different one to split them.';


--
-- TOC entry 1519 (class 1259 OID 16590)
-- Dependencies: 3 1520
-- Name: biblioMediaListReview_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioMediaListReview_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioMediaListReview_id_seq" OWNER TO postgres;

--
-- TOC entry 1863 (class 0 OID 0)
-- Dependencies: 1519
-- Name: biblioMediaListReview_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioMediaListReview_id_seq" OWNED BY "biblioMediaListReview".id;


--
-- TOC entry 1526 (class 1259 OID 16658)
-- Dependencies: 3
-- Name: biblioMediaListShareTable; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioMediaListShareTable" (
    "MediaList_id" bigint NOT NULL,
    "UserProfile_id" bigint NOT NULL,
    "Permissions" integer NOT NULL
);


ALTER TABLE public."biblioMediaListShareTable" OWNER TO postgres;

--
-- TOC entry 1864 (class 0 OID 0)
-- Dependencies: 1526
-- Name: TABLE "biblioMediaListShareTable"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioMediaListShareTable" IS 'Table used to build share-like relation ships between a MediaList and a UserProfile. This relationship is not owner-like (user owns the list), it''s more like that the owner of the list shares it with the user. The owner of the list can also set several permissions to the user (read,write,...)';


--
-- TOC entry 1506 (class 1259 OID 16396)
-- Dependencies: 3
-- Name: biblioMediaPlayList; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioMediaPlayList" (
    "Name" text NOT NULL,
    id bigint NOT NULL,
    "Description" text,
    "UserProfile_id" bigint NOT NULL
);


ALTER TABLE public."biblioMediaPlayList" OWNER TO postgres;

--
-- TOC entry 1865 (class 0 OID 0)
-- Dependencies: 1506
-- Name: TABLE "biblioMediaPlayList"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioMediaPlayList" IS 'The media play list table holds all play lists created by our users, those lists are collections of media which user are able to share and rate.';


--
-- TOC entry 1510 (class 1259 OID 16432)
-- Dependencies: 3 1506
-- Name: biblioMediaPlayList_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioMediaPlayList_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioMediaPlayList_id_seq" OWNER TO postgres;

--
-- TOC entry 1866 (class 0 OID 0)
-- Dependencies: 1510
-- Name: biblioMediaPlayList_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioMediaPlayList_id_seq" OWNED BY "biblioMediaPlayList".id;


--
-- TOC entry 1517 (class 1259 OID 16565)
-- Dependencies: 3
-- Name: biblioMediaReview; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioMediaReview" (
    id bigint NOT NULL,
    "UserProfile_id" bigint NOT NULL,
    "Rating" integer NOT NULL,
    "Review" text NOT NULL,
    "Media_id" bigint NOT NULL
);


ALTER TABLE public."biblioMediaReview" OWNER TO postgres;

--
-- TOC entry 1867 (class 0 OID 0)
-- Dependencies: 1517
-- Name: TABLE "biblioMediaReview"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioMediaReview" IS 'Table storing comments and ratings for a specific media. A review is made a user.';


--
-- TOC entry 1518 (class 1259 OID 16568)
-- Dependencies: 3 1517
-- Name: biblioMediaReview_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioMediaReview_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioMediaReview_id_seq" OWNER TO postgres;

--
-- TOC entry 1868 (class 0 OID 0)
-- Dependencies: 1518
-- Name: biblioMediaReview_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioMediaReview_id_seq" OWNED BY "biblioMediaReview".id;


--
-- TOC entry 1523 (class 1259 OID 16628)
-- Dependencies: 3
-- Name: biblioPushNotifications; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioPushNotifications" (
    id bigint NOT NULL,
    "UserProfile_id" bigint NOT NULL,
    "Notifiaction" text NOT NULL
);


ALTER TABLE public."biblioPushNotifications" OWNER TO postgres;

--
-- TOC entry 1869 (class 0 OID 0)
-- Dependencies: 1523
-- Name: TABLE "biblioPushNotifications"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioPushNotifications" IS 'Tables that handles notification sending from biblio to it''s users. If no user is specified all users will get the notification.';


--
-- TOC entry 1522 (class 1259 OID 16626)
-- Dependencies: 1523 3
-- Name: biblioPushNotifications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioPushNotifications_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioPushNotifications_id_seq" OWNER TO postgres;

--
-- TOC entry 1870 (class 0 OID 0)
-- Dependencies: 1522
-- Name: biblioPushNotifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioPushNotifications_id_seq" OWNED BY "biblioPushNotifications".id;


--
-- TOC entry 1525 (class 1259 OID 16637)
-- Dependencies: 3
-- Name: biblioUserInbox; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioUserInbox" (
    id bigint NOT NULL,
    "UserTo_id" bigint NOT NULL,
    "UserFrom_id" bigint NOT NULL,
    "Subject" text,
    "Message" text NOT NULL
);


ALTER TABLE public."biblioUserInbox" OWNER TO postgres;

--
-- TOC entry 1871 (class 0 OID 0)
-- Dependencies: 1525
-- Name: TABLE "biblioUserInbox"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioUserInbox" IS 'Message inbox of our users.';


--
-- TOC entry 1524 (class 1259 OID 16635)
-- Dependencies: 1525 3
-- Name: biblioUserInbox_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioUserInbox_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioUserInbox_id_seq" OWNER TO postgres;

--
-- TOC entry 1872 (class 0 OID 0)
-- Dependencies: 1524
-- Name: biblioUserInbox_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioUserInbox_id_seq" OWNED BY "biblioUserInbox".id;


--
-- TOC entry 1505 (class 1259 OID 16386)
-- Dependencies: 3
-- Name: biblioUserProfile; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioUserProfile" (
    id bigint NOT NULL
);


ALTER TABLE public."biblioUserProfile" OWNER TO postgres;

--
-- TOC entry 1873 (class 0 OID 0)
-- Dependencies: 1505
-- Name: TABLE "biblioUserProfile"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblioUserProfile" IS 'The basic user profile, used to handle only user specific share information and holds all media which the user has in it''s account.';


--
-- TOC entry 1514 (class 1259 OID 16508)
-- Dependencies: 3
-- Name: biblio_Media_MediaContent; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblio_Media_MediaContent" (
    "Media_id" bigint NOT NULL,
    "MediaContent_id" bigint NOT NULL,
    "Lang" text NOT NULL
);


ALTER TABLE public."biblio_Media_MediaContent" OWNER TO postgres;

--
-- TOC entry 1874 (class 0 OID 0)
-- Dependencies: 1514
-- Name: TABLE "biblio_Media_MediaContent"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblio_Media_MediaContent" IS 'This table is responsible for the relationship between a specifc media and it''s localized content.';


--
-- TOC entry 1521 (class 1259 OID 16611)
-- Dependencies: 3
-- Name: biblio_UserFriends; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblio_UserFriends" (
    "User1_id" bigint NOT NULL,
    "User2_id" bigint NOT NULL
);


ALTER TABLE public."biblio_UserFriends" OWNER TO postgres;

--
-- TOC entry 1875 (class 0 OID 0)
-- Dependencies: 1521
-- Name: TABLE "biblio_UserFriends"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblio_UserFriends" IS 'Table that makes a friend relationship between user profiles';


--
-- TOC entry 1513 (class 1259 OID 16494)
-- Dependencies: 3
-- Name: biblio_UserProfile_Media; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblio_UserProfile_Media" (
    "UserProfile_id" bigint NOT NULL,
    "Media_id" bigint NOT NULL
);


ALTER TABLE public."biblio_UserProfile_Media" OWNER TO postgres;

--
-- TOC entry 1876 (class 0 OID 0)
-- Dependencies: 1513
-- Name: TABLE "biblio_UserProfile_Media"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "biblio_UserProfile_Media" IS 'Relation table between a user profile and one media. This relationship is used to set which media the user owned. The publisher relationship is a direct one between media and UserProfile.';


--
-- TOC entry 1509 (class 1259 OID 16412)
-- Dependencies: 3
-- Name: bilbio_MediaPlayList_Media; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "bilbio_MediaPlayList_Media" (
    "MediaPlayList_id" bigint NOT NULL,
    "Media_id" bigint NOT NULL
);


ALTER TABLE public."bilbio_MediaPlayList_Media" OWNER TO postgres;

--
-- TOC entry 1877 (class 0 OID 0)
-- Dependencies: 1509
-- Name: TABLE "bilbio_MediaPlayList_Media"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "bilbio_MediaPlayList_Media" IS 'Relationship table fro MediaPlayList to Media';


--
-- TOC entry 1794 (class 2604 OID 16404)
-- Dependencies: 1508 1507 1508
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioMedia" ALTER COLUMN id SET DEFAULT nextval('"BiblioMedia_id_seq"'::regclass);


--
-- TOC entry 1796 (class 2604 OID 16543)
-- Dependencies: 1516 1515
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioMediaCategory" ALTER COLUMN id SET DEFAULT nextval('"biblioMediaCategory_id_seq"'::regclass);


--
-- TOC entry 1795 (class 2604 OID 16471)
-- Dependencies: 1511 1512 1512
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioMediaContent" ALTER COLUMN id SET DEFAULT nextval('"biblioMediaContent_id_seq"'::regclass);


--
-- TOC entry 1798 (class 2604 OID 16595)
-- Dependencies: 1519 1520 1520
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioMediaListReview" ALTER COLUMN id SET DEFAULT nextval('"biblioMediaListReview_id_seq"'::regclass);


--
-- TOC entry 1793 (class 2604 OID 16434)
-- Dependencies: 1510 1506
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioMediaPlayList" ALTER COLUMN id SET DEFAULT nextval('"biblioMediaPlayList_id_seq"'::regclass);


--
-- TOC entry 1797 (class 2604 OID 16570)
-- Dependencies: 1518 1517
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioMediaReview" ALTER COLUMN id SET DEFAULT nextval('"biblioMediaReview_id_seq"'::regclass);


--
-- TOC entry 1799 (class 2604 OID 16631)
-- Dependencies: 1522 1523 1523
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioPushNotifications" ALTER COLUMN id SET DEFAULT nextval('"biblioPushNotifications_id_seq"'::regclass);


--
-- TOC entry 1800 (class 2604 OID 16640)
-- Dependencies: 1524 1525 1525
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioUserInbox" ALTER COLUMN id SET DEFAULT nextval('"biblioUserInbox_id_seq"'::regclass);


--
-- TOC entry 1804 (class 2606 OID 16443)
-- Dependencies: 1506 1506
-- Name: MediaPlayList_PK; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioMediaPlayList"
    ADD CONSTRAINT "MediaPlayList_PK" PRIMARY KEY (id);


--
-- TOC entry 1808 (class 2606 OID 16428)
-- Dependencies: 1509 1509 1509
-- Name: Media_MediaPlayList_PK; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "bilbio_MediaPlayList_Media"
    ADD CONSTRAINT "Media_MediaPlayList_PK" PRIMARY KEY ("MediaPlayList_id", "Media_id");


--
-- TOC entry 1816 (class 2606 OID 16554)
-- Dependencies: 1515 1515
-- Name: biblioMediaCategory_Name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioMediaCategory"
    ADD CONSTRAINT "biblioMediaCategory_Name_key" UNIQUE ("Name");


--
-- TOC entry 1818 (class 2606 OID 16552)
-- Dependencies: 1515 1515
-- Name: biblioMediaCategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioMediaCategory"
    ADD CONSTRAINT "biblioMediaCategory_pkey" PRIMARY KEY (id);


--
-- TOC entry 1810 (class 2606 OID 16476)
-- Dependencies: 1512 1512 1512 1512
-- Name: biblioMediaContent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioMediaContent"
    ADD CONSTRAINT "biblioMediaContent_pkey" PRIMARY KEY (id, partid, ownerid);


--
-- TOC entry 1822 (class 2606 OID 16600)
-- Dependencies: 1520 1520 1520 1520
-- Name: biblioMediaListReview_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioMediaListReview"
    ADD CONSTRAINT "biblioMediaListReview_pkey" PRIMARY KEY (id, "UserProfile_id", "Media_id");


--
-- TOC entry 1830 (class 2606 OID 16662)
-- Dependencies: 1526 1526 1526
-- Name: biblioMediaListShareTable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioMediaListShareTable"
    ADD CONSTRAINT "biblioMediaListShareTable_pkey" PRIMARY KEY ("MediaList_id", "UserProfile_id");


--
-- TOC entry 1820 (class 2606 OID 16579)
-- Dependencies: 1517 1517 1517 1517
-- Name: biblioMediaReview_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioMediaReview"
    ADD CONSTRAINT "biblioMediaReview_pkey" PRIMARY KEY (id, "UserProfile_id", "Media_id");


--
-- TOC entry 1806 (class 2606 OID 16478)
-- Dependencies: 1508 1508
-- Name: biblioMedia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioMedia"
    ADD CONSTRAINT "biblioMedia_pkey" PRIMARY KEY (id);


--
-- TOC entry 1826 (class 2606 OID 16644)
-- Dependencies: 1523 1523 1523
-- Name: biblioPushNotifications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioPushNotifications"
    ADD CONSTRAINT "biblioPushNotifications_pkey" PRIMARY KEY (id, "UserProfile_id");


--
-- TOC entry 1828 (class 2606 OID 16642)
-- Dependencies: 1525 1525
-- Name: biblioUserInbox_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioUserInbox"
    ADD CONSTRAINT "biblioUserInbox_pkey" PRIMARY KEY (id);


--
-- TOC entry 1802 (class 2606 OID 16500)
-- Dependencies: 1505 1505
-- Name: biblioUserProfile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioUserProfile"
    ADD CONSTRAINT "biblioUserProfile_pkey" PRIMARY KEY (id);


--
-- TOC entry 1814 (class 2606 OID 16522)
-- Dependencies: 1514 1514 1514 1514
-- Name: biblio_Media_MediaContent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblio_Media_MediaContent"
    ADD CONSTRAINT "biblio_Media_MediaContent_pkey" PRIMARY KEY ("Media_id", "MediaContent_id", "Lang");


--
-- TOC entry 1824 (class 2606 OID 16615)
-- Dependencies: 1521 1521 1521
-- Name: biblio_UserFriends_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblio_UserFriends"
    ADD CONSTRAINT "biblio_UserFriends_pkey" PRIMARY KEY ("User1_id", "User2_id");


--
-- TOC entry 1812 (class 2606 OID 16502)
-- Dependencies: 1513 1513 1513
-- Name: biblio_UserProfile_Media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblio_UserProfile_Media"
    ADD CONSTRAINT "biblio_UserProfile_Media_pkey" PRIMARY KEY ("UserProfile_id", "Media_id");


--
-- TOC entry 1840 (class 2606 OID 16560)
-- Dependencies: 1515 1515 1817
-- Name: biblioMediaCategory_SubCategory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaCategory"
    ADD CONSTRAINT "biblioMediaCategory_SubCategory_id_fkey" FOREIGN KEY ("SubCategory_id") REFERENCES "biblioMediaCategory"(id);


--
-- TOC entry 1836 (class 2606 OID 16479)
-- Dependencies: 1512 1805 1508
-- Name: biblioMediaContent_ownerid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaContent"
    ADD CONSTRAINT "biblioMediaContent_ownerid_fkey" FOREIGN KEY (ownerid) REFERENCES "biblioMedia"(id);


--
-- TOC entry 1844 (class 2606 OID 16606)
-- Dependencies: 1520 1805 1508
-- Name: biblioMediaListReview_Media_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaListReview"
    ADD CONSTRAINT "biblioMediaListReview_Media_id_fkey" FOREIGN KEY ("Media_id") REFERENCES "biblioMedia"(id);


--
-- TOC entry 1843 (class 2606 OID 16601)
-- Dependencies: 1801 1505 1520
-- Name: biblioMediaListReview_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaListReview"
    ADD CONSTRAINT "biblioMediaListReview_id_fkey" FOREIGN KEY (id) REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1849 (class 2606 OID 16663)
-- Dependencies: 1506 1803 1526
-- Name: biblioMediaListShareTable_MediaList_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaListShareTable"
    ADD CONSTRAINT "biblioMediaListShareTable_MediaList_id_fkey" FOREIGN KEY ("MediaList_id") REFERENCES "biblioMediaPlayList"(id);


--
-- TOC entry 1850 (class 2606 OID 16668)
-- Dependencies: 1526 1801 1505
-- Name: biblioMediaListShareTable_UserProfile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaListShareTable"
    ADD CONSTRAINT "biblioMediaListShareTable_UserProfile_id_fkey" FOREIGN KEY ("UserProfile_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1831 (class 2606 OID 16528)
-- Dependencies: 1801 1506 1505
-- Name: biblioMediaPlayList_UserProfile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaPlayList"
    ADD CONSTRAINT "biblioMediaPlayList_UserProfile_id_fkey" FOREIGN KEY ("UserProfile_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1841 (class 2606 OID 16580)
-- Dependencies: 1517 1508 1805
-- Name: biblioMediaReview_Media_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaReview"
    ADD CONSTRAINT "biblioMediaReview_Media_id_fkey" FOREIGN KEY ("Media_id") REFERENCES "biblioMedia"(id);


--
-- TOC entry 1842 (class 2606 OID 16585)
-- Dependencies: 1801 1517 1505
-- Name: biblioMediaReview_UserProfile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMediaReview"
    ADD CONSTRAINT "biblioMediaReview_UserProfile_id_fkey" FOREIGN KEY ("UserProfile_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1833 (class 2606 OID 16555)
-- Dependencies: 1508 1515 1817
-- Name: biblioMedia_MediaCategory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMedia"
    ADD CONSTRAINT "biblioMedia_MediaCategory_id_fkey" FOREIGN KEY ("MediaCategory_id") REFERENCES "biblioMediaCategory"(id);


--
-- TOC entry 1832 (class 2606 OID 16533)
-- Dependencies: 1801 1505 1508
-- Name: biblioMedia_PublisherProfile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioMedia"
    ADD CONSTRAINT "biblioMedia_PublisherProfile_id_fkey" FOREIGN KEY ("PublisherProfile_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1848 (class 2606 OID 16653)
-- Dependencies: 1505 1801 1525
-- Name: biblioUserInbox_UserFrom_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioUserInbox"
    ADD CONSTRAINT "biblioUserInbox_UserFrom_id_fkey" FOREIGN KEY ("UserFrom_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1847 (class 2606 OID 16648)
-- Dependencies: 1525 1801 1505
-- Name: biblioUserInbox_UserTo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblioUserInbox"
    ADD CONSTRAINT "biblioUserInbox_UserTo_id_fkey" FOREIGN KEY ("UserTo_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1839 (class 2606 OID 16513)
-- Dependencies: 1508 1514 1805
-- Name: biblio_Media_MediaContent_Media_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblio_Media_MediaContent"
    ADD CONSTRAINT "biblio_Media_MediaContent_Media_id_fkey" FOREIGN KEY ("Media_id") REFERENCES "biblioMedia"(id);


--
-- TOC entry 1845 (class 2606 OID 16616)
-- Dependencies: 1801 1505 1521
-- Name: biblio_UserFriends_User1_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblio_UserFriends"
    ADD CONSTRAINT "biblio_UserFriends_User1_id_fkey" FOREIGN KEY ("User1_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1846 (class 2606 OID 16621)
-- Dependencies: 1521 1801 1505
-- Name: biblio_UserFriends_User2_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblio_UserFriends"
    ADD CONSTRAINT "biblio_UserFriends_User2_id_fkey" FOREIGN KEY ("User2_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1838 (class 2606 OID 16523)
-- Dependencies: 1805 1513 1508
-- Name: biblio_UserProfile_Media_Media_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblio_UserProfile_Media"
    ADD CONSTRAINT "biblio_UserProfile_Media_Media_id_fkey" FOREIGN KEY ("Media_id") REFERENCES "biblioMedia"(id);


--
-- TOC entry 1837 (class 2606 OID 16503)
-- Dependencies: 1513 1801 1505
-- Name: biblio_UserProfile_Media_UserProfile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "biblio_UserProfile_Media"
    ADD CONSTRAINT "biblio_UserProfile_Media_UserProfile_id_fkey" FOREIGN KEY ("UserProfile_id") REFERENCES "biblioUserProfile"(id);


--
-- TOC entry 1835 (class 2606 OID 16489)
-- Dependencies: 1509 1506 1803
-- Name: bilbio_MediaPlayList_Media_MediaPlayList_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "bilbio_MediaPlayList_Media"
    ADD CONSTRAINT "bilbio_MediaPlayList_Media_MediaPlayList_id_fkey" FOREIGN KEY ("MediaPlayList_id") REFERENCES "biblioMediaPlayList"(id);


--
-- TOC entry 1834 (class 2606 OID 16484)
-- Dependencies: 1805 1509 1508
-- Name: bilbio_MediaPlayList_Media_Media_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "bilbio_MediaPlayList_Media"
    ADD CONSTRAINT "bilbio_MediaPlayList_Media_Media_id_fkey" FOREIGN KEY ("Media_id") REFERENCES "biblioMedia"(id);


--
-- TOC entry 1855 (class 0 OID 0)
-- Dependencies: 3
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2009-08-02 21:24:36 CEST

--
-- PostgreSQL database dump complete
--

