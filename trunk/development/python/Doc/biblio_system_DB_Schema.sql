--
-- PostgreSQL database dump
--

-- Started on 2009-08-02 21:24:20 CEST

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 1773 (class 1262 OID 16384)
-- Name: biblio_system; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE biblio_system WITH TEMPLATE = template0 ENCODING = 'UTF8';


ALTER DATABASE biblio_system OWNER TO postgres;

\connect biblio_system

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1483 (class 1259 OID 16720)
-- Dependencies: 3
-- Name: biblioIPBlackList; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioIPBlackList" (
    ip text NOT NULL
);


ALTER TABLE public."biblioIPBlackList" OWNER TO postgres;

--
-- TOC entry 1482 (class 1259 OID 16709)
-- Dependencies: 3
-- Name: biblioServers; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioServers" (
    id bigint NOT NULL,
    pid integer,
    port integer,
    ip text,
    servertype text
);


ALTER TABLE public."biblioServers" OWNER TO postgres;

--
-- TOC entry 1481 (class 1259 OID 16707)
-- Dependencies: 1482 3
-- Name: biblioServers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioServers_id_seq"
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioServers_id_seq" OWNER TO postgres;

--
-- TOC entry 1776 (class 0 OID 0)
-- Dependencies: 1481
-- Name: biblioServers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioServers_id_seq" OWNED BY "biblioServers".id;


--
-- TOC entry 1480 (class 1259 OID 16696)
-- Dependencies: 3
-- Name: biblioSessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioSessions" (
    id bigint NOT NULL,
    uid text,
    "user" text,
    ip text,
    permission integer NOT NULL,
    sessiontime numeric
);


ALTER TABLE public."biblioSessions" OWNER TO postgres;

--
-- TOC entry 1479 (class 1259 OID 16694)
-- Dependencies: 3 1480
-- Name: biblioSessions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioSessions_id_seq"
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioSessions_id_seq" OWNER TO postgres;

--
-- TOC entry 1777 (class 0 OID 0)
-- Dependencies: 1479
-- Name: biblioSessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioSessions_id_seq" OWNED BY "biblioSessions".id;


--
-- TOC entry 1478 (class 1259 OID 16686)
-- Dependencies: 3
-- Name: biblioSystemData; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioSystemData" (
    "Key" text NOT NULL,
    "Value" text NOT NULL
);


ALTER TABLE public."biblioSystemData" OWNER TO postgres;

--
-- TOC entry 1485 (class 1259 OID 16743)
-- Dependencies: 3
-- Name: biblioUserData; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "biblioUserData" (
    id bigint NOT NULL,
    "user" text NOT NULL,
    pass text NOT NULL,
    permissions integer NOT NULL
);


ALTER TABLE public."biblioUserData" OWNER TO postgres;

--
-- TOC entry 1484 (class 1259 OID 16741)
-- Dependencies: 1485 3
-- Name: biblioUserData_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "biblioUserData_id_seq"
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."biblioUserData_id_seq" OWNER TO postgres;

--
-- TOC entry 1778 (class 0 OID 0)
-- Dependencies: 1484
-- Name: biblioUserData_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "biblioUserData_id_seq" OWNED BY "biblioUserData".id;


--
-- TOC entry 1753 (class 2604 OID 16712)
-- Dependencies: 1482 1481 1482
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioServers" ALTER COLUMN id SET DEFAULT nextval('"biblioServers_id_seq"'::regclass);


--
-- TOC entry 1752 (class 2604 OID 16699)
-- Dependencies: 1479 1480 1480
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioSessions" ALTER COLUMN id SET DEFAULT nextval('"biblioSessions_id_seq"'::regclass);


--
-- TOC entry 1754 (class 2604 OID 16746)
-- Dependencies: 1485 1484 1485
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE "biblioUserData" ALTER COLUMN id SET DEFAULT nextval('"biblioUserData_id_seq"'::regclass);


--
-- TOC entry 1766 (class 2606 OID 16727)
-- Dependencies: 1483 1483
-- Name: biblioIPBlackList_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioIPBlackList"
    ADD CONSTRAINT "biblioIPBlackList_pkey" PRIMARY KEY (ip);


--
-- TOC entry 1762 (class 2606 OID 16717)
-- Dependencies: 1482 1482
-- Name: biblioServers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioServers"
    ADD CONSTRAINT "biblioServers_pkey" PRIMARY KEY (id);


--
-- TOC entry 1764 (class 2606 OID 16719)
-- Dependencies: 1482 1482
-- Name: biblioServers_port_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioServers"
    ADD CONSTRAINT "biblioServers_port_key" UNIQUE (port);


--
-- TOC entry 1758 (class 2606 OID 16706)
-- Dependencies: 1480 1480
-- Name: biblioSessions_ip_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioSessions"
    ADD CONSTRAINT "biblioSessions_ip_key" UNIQUE (ip);


--
-- TOC entry 1760 (class 2606 OID 16704)
-- Dependencies: 1480 1480
-- Name: biblioSessions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioSessions"
    ADD CONSTRAINT "biblioSessions_pkey" PRIMARY KEY (id);


--
-- TOC entry 1756 (class 2606 OID 16693)
-- Dependencies: 1478 1478
-- Name: biblioSystemData_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioSystemData"
    ADD CONSTRAINT "biblioSystemData_pkey" PRIMARY KEY ("Key");


--
-- TOC entry 1768 (class 2606 OID 16751)
-- Dependencies: 1485 1485
-- Name: biblioUserData_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioUserData"
    ADD CONSTRAINT "biblioUserData_pkey" PRIMARY KEY (id);


--
-- TOC entry 1770 (class 2606 OID 16753)
-- Dependencies: 1485 1485
-- Name: biblioUserData_user_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "biblioUserData"
    ADD CONSTRAINT "biblioUserData_user_key" UNIQUE ("user");


--
-- TOC entry 1775 (class 0 OID 0)
-- Dependencies: 3
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2009-08-02 21:24:20 CEST

--
-- PostgreSQL database dump complete
--

