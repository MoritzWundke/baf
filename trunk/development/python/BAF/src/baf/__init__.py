try:
    __version__ = __import__('pkg_resources').get_distribution('BibliothecaApplicationFramework').version
except Exception:
    __version__ = 0