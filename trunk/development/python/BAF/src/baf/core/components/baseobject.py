"""
    @copyright: 

    @author: Joze Castellano, Moritz Wundke

    @summary:
        Simple class that just handles environment persitence. This is the base object of all 
        classes under the environment, so basically the base of everything in the framework.
    
    @change:    
        - Created on 31/08/2009: Part of refactor process and framework definition
        - Update on 01/09/2009: Fixed syntax errors.
    
    @deprecated: ALL SYSTEM NOW BASES ON COMPONENTS!
"""

from baf.core.exceptions.commonexceptions import *

class BaseObject():
    """
    Base class for all classes that needs to handle an environment
    """
    Env = property ( _GetEnv, _SetEnv)
    
    def __init__(self, **kw):
        """
        Base contructor, just caches the environment

        Keyword arguments:
            env -- cached environment the logger will make use off (default 0.0)
        
        Throws:
            CMM_E01 -- Needs an env in it's argument list
        """
        self._env = None        
        self.Env = str(kw.get('env',None))

        # Do we got an rnv setup? Coz' we need one!
        if self.Env is None:
            raise CustomExceptions(code=CMM_E01)

    def _GetEnv(self):
        """
        Returns the environment
        """
        return self._env
    
    def _SetEnv(self, value):
        """
        Sets the environment
        """
        self._env = value
