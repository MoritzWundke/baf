# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 DarkCultureGames
# All rights reserved.
#
# DataBase Manager
#
# Author: Moritz Wundke <b_thax@darkculturegames.com>
# Change: 01/11/2009 - Created

from baf.core.components.component import Component, implements, ExtensionPoint
from baf.core.db.api import IDBEngine
from baf.core.config.config import ExtensionPointListItem

__all__ = ['DatabaseManager']

class DatabaseManager(Component):
    """
    Class that handles connection and high-level database access
    """
    engines = ExtensionPoint(IDBEngine)
    
    db_engines = ExtensionPointListItem('database', 'engine', IDBEngine, 'PostGreSQLEngine',
        """Available DB engines that we can use""")
    
    def initdb(self):
        """
        Initialize all engines we need
        """
        for engine in self.db_engines:
            engine.initdb()
    
    def dbshutdown(self):
        """
        Inform engines that we should shutdown
        """
        for engine in self.engines:
            engine.dbshutdown()