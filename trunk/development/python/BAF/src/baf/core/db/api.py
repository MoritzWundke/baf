# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 DarkCultureGames
# All rights reserved.
#
# API for DB connections
#
# Author: Moritz Wundke <b_thax@darkculturegames.com>
# Change: 20/10/2009 - Created

from baf.core.components.component import Interface

__all__ = ['IDBEngine']

class IDBEngine(Interface):
    """
    A DB engine represents a component that is used to create a sqlalchemy
    engine.
    """
    def get_engine():
        """
        Return a sqlalchemy engine
        """
    
    def initdb():
        """
        Called from the dbmanager once it gets initialized
        """
    
    def dbshutdown():
        """
        Called from the dbmanager once it gets shutdown
        """