"""
    @copyright:

    @author: Joze Castellano, Moritz Wundke

    @summary:
        Interfaces related to application development
    
    @change:   
        - Created on 17/09/2009: Part of refactor process and framework definition
"""

from baf.core.components.component import Component, implements, ExtensionPoint
from baf.core.application.api import IApp
from baf.core.exceptions.baseerror import BaseError
from baf.core.util.texttransforms import _

# Some constants used here
APP_RET_CODE_SUCCESS = 0
APP_RET_CODE_NONE = None
SUCCESS_RET_CODES = [APP_RET_CODE_SUCCESS, APP_RET_CODE_NONE]

DEFAULT_APPLICATION = 'DefaultApplication'

__all__ = ['DefaultApplication', 'ApplicationManager','ApplicationNotImplementedError']

class ApplicationNotImplementedError(BaseError):
    """
    Error used when an app dose not implement a specifc methos
    """
    title="[Config Error]"

class DefaultApplication(Component):
    implements(IApp)

    def app_main(self):
        """
        Default Main implementation
        """
        self.log.info("Starting Default Application...");
        self.log.info("Closing Default Application...");
        return APP_RET_CODE_SUCCESS
    
class ApplicationManager(Component):
    """
    The application manager is a component that just handles a list of available applications
    """
    applications = ExtensionPoint(IApp)

    def launch(self):
        for app in self.applications:
            app.app_main()
