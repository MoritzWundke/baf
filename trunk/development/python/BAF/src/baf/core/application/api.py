# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 DarkCultureGames
# All rights reserved.
#
# Interfaces related to application development
#
# Author: Moritz Wundke <b_thax@darkculturegames.com>
# Change: 17/09/2009 - Created
#         04/10/2009 - Header changed

from baf.core.components.component import Interface

__all__ = ['IApp']

class IApp(Interface):
    """
    Extension point that defines a component that will be handled as an application.
    Applications are components that run in an BAF environment
    """
    
    def app_main():
        """
        Main entry point for the application. The app should return a return code or None
        """