#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 DarkCultureGames
# All rights reserved.

from setuptools import setup, find_packages

setup(
    name = 'BibliothecaApplicationFramework',
    version = '0.0.1',
    description = 'Bibliotheca Application Framework',
    long_description = """BAF is high-scaled python application framework for generic use.""",
    author = 'DarkCultureGames',
    author_email = 'info@darkculturegames.com',
    license = 'BSD',
    url = 'http://baf.darkculturegames.com/',
    download_url = 'http://baf.darkculturegames.com/',
    classifiers = [
        'Environment :: General Purpose',
        'Framework :: BAF',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Application Framework',
    ],
    
    # Exclude stuff: exclude=['*.tests']
    packages = find_packages(),
    
    # None by now but who knows
    #package_data = {},

    #test_suite = 'bibliotheca.tools.unittest.TestBibliotheca',
    zip_safe = False,

    install_requires = [
        'setuptools>=0.6b1',
        'sqlalchemy>=0.5.6',
        'psycopg2>=2.0.12',
        'pysqlite>=2.5.5',
        'Twisted>=8.2.0'
    ],
    
    # example: 'Pygments': ['Pygments>=0.6'],
    #extras_require = { },

    entry_points = """
        [console_scripts]
        baf-admin = baf.core.environment.envadmin:main

        [baf.components]
        baf.core.environment.envadmin = baf.core.environment.envadmin
        baf.core.application = baf.core.application
        baf.core.db.dbengines = baf.core.db.dbengines
    """,
)
