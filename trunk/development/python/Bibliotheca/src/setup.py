#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 DarkCultureGames
# All rights reserved.

from setuptools import setup, find_packages

setup(
    name = 'bibliotheca',
    version = '0.0.1',
    description = 'Bibliotheca Application Framework',
    long_description = """BAF is high-scaled python application framework for generic use.""",
    author = 'DarkCultureGames',
    author_email = 'info@darkculturegames.com',
    license = 'BSD',
    url = 'http://bibliotheca.darkculturegames.com/',
    download_url = 'http://bibliotheca.darkculturegames.com/',
    classifiers = [
        'Environment :: General Purpose',
        'Framework :: BAF',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Application Framework',
    ],
    
    # Exclude stuff: exclude=['*.tests']
    packages = find_packages(),
    
    # None by now but who knows
    #package_data = {},

    test_suite = 'bibliotheca.tools.unittest.TestBibliotheca',
    zip_safe = False,

    install_requires = [
        'setuptools>=0.6b1',
        'PyGreSQL>=4.0'
    ],
    
    # example: 'Pygments': ['Pygments>=0.6'],
    #extras_require = { },

    entry_points = """
        [console_scripts]
        biblio-admin = bibliotheca.system.core.environment.envadmin:main

        [biblio.components]
        bibliotheca.system.core.environment.envadmin = bibliotheca.system.core.environment.envadmin
        bibliotheca.system.core.application = bibliotheca.system.core.application
    """,
)