'''
Created on 11/07/2009

@author: Moss, Joze
'''

#Bibliotheca imports
from baseXMLRPCRes import BaseXMLRPCRes
from bibliotheca.net.asynctasks.register import * 

__STR__ = "ContentXMLRPC"

class ContentXMLRPC(BaseXMLRPCRes):
    """
    XMLRPC Resource for a content server
    """
    def __init__(self, env):  
        """
        Will initialize all async task handlers
        """
        # Dictionary that holds all registered handlers
        self._AsyncTaskHanlders = register_async_tasks(env)
        
        # Call our super
        BaseXMLRPCRes.__init__(self, env)
        
    def __str__(self):
        return __STR__
    
    def xmlrpc_asynctask(self, request, session, asynctask, KeysValues):
        """
        Entry method to start any async process
        """
        AsyncTaskDict = {}
        AsyncTaskReturnCode = ASYNC_RETURN_CODE_NONE
        
        # process task
        for AsyncTask in self._AsyncTaskHanlders:
            if AsyncTask.shoudld_process( asynctask ):
                AsyncTaskReturnCode, AsyncTaskDict = AsyncTask.process( KeysValues )
                break
            
        return AsyncTaskReturnCode, AsyncTaskDict 
        