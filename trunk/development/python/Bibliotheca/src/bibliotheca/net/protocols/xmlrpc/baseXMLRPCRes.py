'''
Created on 11/07/2009

@author: Moss
'''

# twisted imports
from twisted.web import xmlrpc
from twisted.web import server 
#from twisted.web import http

from twisted.internet import reactor
from twisted.internet import  defer

#===============================================================================
# # Database handling
# #from twisted.enterprise import adbapi
# 
# python imports
from xmlrpclib import * 
# #import sqlite3
import threading
# #import time
# #import os
# import hashlib
# 
# # Bibliotheca imports
from bibliotheca.common.base import BaseObject
from bibliotheca.common.http import *
from  bibliotheca.common.types import *
# from bibliotheca.net.server.provider.providerutil import *
# from bibliotheca.net.server.provider.systemprovider import SystemProvider
# from serverexceptions import *
# import bibliotheca.net.asynctasks.register
#===============================================================================

class BaseXMLRPCRes(xmlrpc.XMLRPC, BaseObject):
    """
    Biblio XML-RPC Resource provider
    """    
    def __init__(self, env):        
        BaseObject.__init__(self, environment=env )
        
        # Dict holding our call permissions
        self._CALL_PRMISSIONS = {}
        
        self._SetupEnvironment()
        xmlrpc.XMLRPC.__init__(self)        
        
    def _SetupEnvironment(self):
        # Setup all permissions
        self._AddPermission('shutdown', ADMIN)
        self._AddPermission('echo', GUEST)
        self._AddPermission('ping', GUEST)
        self._AddPermission('login', GUEST)
        self._AddPermission('logout', USER)
        self._AddPermission('serverstatus', ADMIN)
        self._AddPermission('close', ADMIN)
        self._AddPermission('open', ADMIN)    
        
        # Setup basic black lists
        for IP in BLOCKED_IPS:
            self.Env.SystemProvider.bann_ip(IP)
        for IP in VALID_IPS:
            self.Env.SystemProvider.unbann_ip(IP)    
    
    # Verify is the session is able to call this method, always true if it's an admin!
    def _CheckCallPermission(self, method, session):
        return self.CheckPermission(method, session)
    
    def _IsClosedForIP(self, IP):
        """
        # If the server is closed the IP must be the local host! If not the ip is blocked
        """
        if self._env.IsClosed():
            if IP == '127.0.0.1':
                return False
            return True
        return False
    
    def _IsIPBlocked(self, IP):
        """
        Check if the IP should be blocked or not
        """
        return self.Env.SystemProvider.is_ip_blocked(IP)
    
    def _AddPermission(self,method, permission=GUEST):
        """
        Add permissions to our local rpc calls
        """
        if not method.lower() in self._CALL_PRMISSIONS:
            self._CALL_PRMISSIONS[method] = permission
    
    def CheckPermission(self,method, session):
        """
        Check if the session has access to the required method
        """
        if self._sessionIsValid(session):
            return method in self._CALL_PRMISSIONS\
                and session.GetSessionPermission() <= self._CALL_PRMISSIONS[method] or session.GetSessionPermission() == ADMIN
        return False
    
    def GetPassToken(self, user):
        return self.Env.SystemProvider.GetPassToken(user)
    
    def _SetSite(self, value):
        self._Site = value
    
    def _GetSite(self):
        return self._Site
    
    Site = property (_GetSite, _SetSite)
        
    # Raise a HTTP Fault and send it to the client
    def RaiseHTTPFault(self, request, code, msg=""):
        if msg == "":
            msg = RESPONSES[code]
        f = xmlrpc.Fault(code, msg)
        self._cbRender(f, request)
    
    def _sessionIsValid(self, Session ):
        """
        Evaluates a session dictionary if it's valid or not
        """
        SessionData = Session.GetSessionData()
        return SessionData is not None and \
            SESSIONDATA_DBID in SessionData and \
            SESSIONDATA_UID in SessionData and \
            SESSIONDATA_USER in SessionData and \
            SESSIONDATA_IP in SessionData
    
    def _renderThread(self, request, session ):
        if self._sessionIsValid(session):
            user = session.GetSessionUser()
            passwd = request.getPassword()
            
            request.content.seek(0, 0)
            request.setHeader("content-type", "text/xml")
            try:
                raw_args, functionPath = loads(request.content.read())
            except Exception, e:
                f = Fault(self.FAILURE, "Can't deserialize input: %s" % (e,))
                self._cbRender(f, request)
            else:
                OutMsg = "User '%s' requests '%s'" % (user, functionPath)
                if raw_args != None:
                    OutMsg = "%s with %s" % (OutMsg, raw_args)                    
                self.Debug(OutMsg)
                
                # The first argument is the response,               
                args_final = [request, session]
                
                # The first argument should always be the session id!
                args = raw_args[1:]
                args_final.extend(args)
                
                try:
                    if self._CheckCallPermission(functionPath, session):
                        function = self._getFunction(functionPath)                    
                    else:
                        self.RaiseHTTPFault(request, HTTP_PERMISSION_REQUIRED)
                        return
                except xmlrpc.NoSuchFunction, f:
                    self.RaiseHTTPFault(request, HTTP_NO_SUCH_FUNCTION)
                    return                 
                except xmlrpc.Fault, f:
                    self._cbRender(f, request)
                else:
                    defer.maybeDeferred(function, *args_final).addErrback(
                        self._ebRender
                    ).addCallback(
                        self._cbRender, request
                    )
                self.Print("Request finished")
        else:
            self.RaiseHTTPFault(request, HTTP_SESSION_INVALID)

    # Shut down the server and infrom clients    
    def _ShutDown(self, request):       
        # Stop Server
        reactor.stop()        

    def render(self, request):
        """ Overridden 'render' method which takes care of
        creating client processes in separate threads"""        
        # Are we authorized?
        user = request.getUser()
        ip = request.getClientIP()
        
        # First check if the server is closed!
        if not self._IsClosedForIP(ip):
            # Check IP!
            if not self._IsIPBlocked(ip):      
                # Start Sessions, will assign a guest session, or recover an old one
                RequestSession = self.StartSession(request)
                
                if RequestSession:    
                    # Start the process thread!      
                    self.Print("Starting Client Thread")
                    t = threading.Thread(target=self._renderThread, name="renderThread", args=[request, RequestSession])
                    t.start()
                else:
                    self.Warn("IP tried to access with an invalid session: %s" % (ip))
                    self.RaiseHTTPFault(request, HTTP_SESSION_INVALID)
            else:
                self.Warn("Blocked IP was trying to access server: %s" % (ip))
                self.RaiseHTTPFault(request, HTTP_IP_BLOCKED)
        else:
            self.Warn("IP was trying to access a closed server: %s" % (ip))
            self.RaiseHTTPFault(request, HTTP_SERVER_IS_CLOSED)
        return server.NOT_DONE_YET   

    #---Session and authentication
    
    def StartSession(self, request):
        try:
            # Try to recover a session
            return self.Env.SystemProvider.RecoverSession(request)                     
        except Exception, e:
            self.Print("Could not recover session, normally because none was found! %s" % (e))
            # create a new 'guest' session
            return self.Env.SystemProvider.InsertSession(request.getClientIP())
    
    #----------------------------------------
    # Remote Calls
    #----------------------------------------        
    def xmlrpc_shutdown(self, request, session):
        self._ShutDown(request)
        return;

    def xmlrpc_echo(self, request, session, x):
        return x
     
    def xmlrpc_ping(self, request, session):
        return 'OK'
        
    def xmlrpc_serverstatus(self, request, session):
        StatusDict = {} 
        try:
            StatusDict['ServerEnv'] = {}
            StatusDict['ServerEnv']['EnvPath'] = self._env.GetEnvPath()
            StatusDict['ServerEnv']['IsLogging'] = self._env.IsLogging()
            StatusDict['ServerEnv']['IsReleaseMode'] = self._env.IsReleaseMode()
            StatusDict['ServerEnv']['Version'] = self._env.GetVersion()
            StatusDict['ServerEnv']['DBVersion'] = self._env.GetDBVersion()
            try:
                StatusDict['ServerLog'] = {}
                StatusDict['ServerLog']['FileName'] = self._env.GetConfig('env', "logfile")
                File = open(self._env.GetLogFilePath(),'r')
                StatusDict['ServerLog']['FileContent'] = File.read()
                File.close()
            except Exception, e:
                StatusDict['ServerLog'] = {}
                StatusDict['ServerLog']['FileError'] = '%s' % e                
            StatusDict['ServerStatus'] = 'OK'
        except:
            StatusDict['ServerStatus'] = 'FAILED'
        return StatusDict
    
    def xmlrpc_close(self, request, session):
        # closing a server is only letting it respond to the local host
        self._env.Close()
        return "Server Closed - Can now only be opened by an admin from the localhost!"
    
    def xmlrpc_open(self, request, session):
        # open server again to server to anyone
        self._env.Open()
        return "Server Opened - Serving the world again!"
        