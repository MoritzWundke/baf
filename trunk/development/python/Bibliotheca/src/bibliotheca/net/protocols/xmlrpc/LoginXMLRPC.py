'''
Created on 11/07/2009

@author: Moss, Joze
'''

#Bibliotheca imports
from baseXMLRPCRes import BaseXMLRPCRes
from bibliotheca.common.http import *

__STR__ = "LoginXMLRPC"

class LoginXMLRPC(BaseXMLRPCRes):
    """
    XMLRPC Resource for a login server
    
    @change: 2 August 2009 
        - Password Hash will be done in client.
        - Assign a content server to that user.
    """
    def __init__(self, env):        
        BaseXMLRPCRes.__init__(self, env)
        
    def __str__(self):
        return __STR__
        
    def xmlrpc_login(self, request, session, User, Pass):
        """
        Will try to log a specific user in. Apart from login we will assign
        a content server to that user
        @return (LOGIN_CODE, SESSION_UID, CONTENT_SERVER_IP)
        """
        self.Debug("Loggin in...")
        try:
            # The password is hashed in client side!
            #HashPass = hashlib.md5(Pass).hexdigest()
            bLoggedIn, session = self.Env.SystemProvider.Login(request, ("%s:%s") % (User, Pass), session, User)        
            if bLoggedIn:
                # We are logged in :D, so search for a suitable content server and extract it's IP
                content_server_host = self.Env.SystemProvider.get_best_content_server()
                self.Debug("User '%s' will use content server running on '%s'" % (User,content_server_host))
                # return all
                return (HTTP_LOGIN_OK,session.GetSessionUID(),content_server_host)
            else:
                return (HTTP_LOGIN_FAILED,None,"")
        except Exception, e:
            self.Error("Failed: %s - %s " % (User,e),'Login')
            return (HTTP_LOGIN_FAILED,None,"")
      
    def xmlrpc_logout(self, request, session):
        """
        Will try to logout a specific user.
        @return: LOGOUT_CODE
        """
        try:
            bLoggedOut = self.Env.SystemProvider.Logout(session.GetSessionUID(), request.getUser())
            if bLoggedOut:
                return HTTP_LOGOUT_OK
            else:
                return HTTP_LOGOUT_FAILED
        except Exception, e:
            self.Error("Failed: %s - %s " % (self.Env.SystemProvider.sessionData[SESSIONDATA_UID],e),'Logout')
            return HTTP_LOGOUT_FAILED
        
