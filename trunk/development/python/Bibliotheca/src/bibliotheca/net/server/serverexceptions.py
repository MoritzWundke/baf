class NoSavedSessionFound(Exception):
    """
    Raised when trying to recover a session taken an UID and an Username
    """
    
class SystemProviderNotCreated(Exception):
    """
    Raised on unsuccesfull SystemProvider creation
    """

class OsProcessCouldNotBeenDeamonized(Exception):
    """
    Raised on unsuccesfull Daemonization operation
    """