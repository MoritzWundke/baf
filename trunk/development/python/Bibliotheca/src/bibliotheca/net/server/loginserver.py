#===============================================================================
# # twisted imports
# from twisted.web import xmlrpc, server, http
# from twisted.internet import defer
from twisted.internet import reactor

from bibliotheca.common.baseserver import *
from bibliotheca.net.provider.systemprovider import SystemProvider
from serverexceptions import *
from bibliotheca.net.protocols.xmlrpc.LoginXMLRPC import LoginXMLRPC
from serverarquitecture import BiblioSite
        
class BiblioLoginServer(BiblioBaseServer):
    """
    Our main biblio ligin server, will create an environment and start a server.
    Also will register itself so that we can stop it in case we need it
    """
    def __init__(self, **kw):
        
        self.serverport = kw.get('portNumber', 8080)
        daemonMode      = kw.get('daemonMode', False)
        basePath        = kw.get('basePath', False)
        
        # Initialize all basic stuff
        # TODO: Prefixes could be parametrized with serverType
        BiblioBaseServer.__init__(self, path=basePath, \
                                        serverType=S_LOGIN,\
                                        serverMode=daemonMode,\
                                        configfile="login_server_%s"%self.serverport, \
                                        cat="LoginServer",\
                                        newenv="server",\
                                        logid="loginserver_%s"%self.serverport)
        
    def _SetupEnvironment(self):
        """ Build the content provider """
        # TODO: Should workaround defining only the metaclass for the ContentProvider
        BiblioBaseServer._SetupEnvironment(self)        
        self.Env.RegisterSystemProvider(SystemProvider(environment=self.Env))
        self.Print ("Environment succesfully attached")
        
    def _CreateResources(self):
        """ Resources required on Init """
        
        BiblioBaseServer._CreateResources(self)
        #Create Resource
        self._XMLRPCObj = LoginXMLRPC(self._env)
        self._SetSite(BiblioSite(self, self._XMLRPCObj))
#        self._env.SetSite(self._Site)
#        self._XMLRPCResource.SetSite(self._Site)
        reactor.listenTCP(self.serverport, self._GetSite())  

    def _ShowServerInfo(self):
        """ Creation Info about Servers """
        BiblioBaseServer._ShowServerInfo(self)
        
