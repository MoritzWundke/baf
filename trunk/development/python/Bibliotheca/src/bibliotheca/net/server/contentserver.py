#twisted imports
from twisted.internet import reactor

# Bibliotheca imports
from bibliotheca.common.baseserver import *
from bibliotheca.net.provider.contentprovider import ContentServerProvider 
from bibliotheca.net.server.serverexceptions import *
from bibliotheca.net.protocols.xmlrpc.ContentXMLRPC import ContentXMLRPC
from serverarquitecture import BiblioSite

class BiblioContentServer(BiblioBaseServer):
    """
    A content server, this type of servers will make the actual work in biblio.
    """
    def __init__(self, **kw):
        
        self.serverport = kw.get('portNumber', 8081)
        daemonMode = kw.get('daemonMode', False)
        basePath = kw.get('basePath', False)
        
        # Initialize all basic stuff
        # TODO: Prefixes could be parametrized with serverType
        BiblioBaseServer.__init__(self, path=basePath, \
                                        serverType=S_CONTENT, \
                                        serverMode=daemonMode, \
                                        configfile="content_server_%s" % self.serverport, \
                                        cat="ContentServer", \
                                        newenv="server", \
                                        logid="contentserver_%s" % self.serverport)
#===============================================================================
#    def __init__(self, BasePath, PortNumber = 8081):
#        
#        self.serverport = PortNumber
#        
#        # Initialize all basic stuff        
#        BiblioBaseServer.__init__(self, path=BasePath, serverType=S_CONTENT,configfile="content_server_%s"%self.serverport, cat="ContentServer",newenv="server",logid="contentserver_%s"%self.serverport)
#===============================================================================

    def _SetupEnvironment(self):
        """ Build the content provider """        
        # TODO: Should workaround defining only the metaclass for the ContentProvider
        BiblioBaseServer._SetupEnvironment(self)       
        self._env.RegisterSystemProvider(ContentServerProvider(self._env))
        self.Print ("Environment succesfully attached")
        
    def _CreateResources(self):
        """ Resources required on Init """
        
        BiblioBaseServer._CreateResources(self)
        #Create Resource
        self._XMLRPCObj = ContentXMLRPC(self._env)
        self._SetSite(BiblioSite(self, self._XMLRPCObj))
#        self._env.SetSite(self._Site)
#        self._XMLRPCResource.SetSite(self._Site)
        reactor.listenTCP(self.serverport, self._GetSite())  
        
    def _ShowServerInfo(self):
        """ Creation Info about Servers """        
        BiblioBaseServer._ShowServerInfo(self)

        
