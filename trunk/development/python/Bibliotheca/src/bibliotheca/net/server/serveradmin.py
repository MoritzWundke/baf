'''
Created on 10/07/2009

@author: Moss, Joze
'''
# python imports
import subprocess
import xmlrpclib
import getpass
import sys
import hashlib
 
#Bibliotheca imports
from bibliotheca.net.server.serverexceptions import *
from bibliotheca.common.baseserver import *
from bibliotheca.common.modes import *;
from bibliotheca.net.provider.providerutil import * 
from bibliotheca.net.provider.systemprovider import SystemProvider
from bibliotheca.common.http import *
from bibliotheca import *;


class BiblioServerAdmin(BiblioBaseServer):
    """ Biblio shell server admin
    connect using: p = ServerProxy('http://user:pass@localhost:8080')"""
    
    def __init__(self, BasePath, PortNumber = 8080):
        
        self.serverport = PortNumber
        self.HashedPass = ""
        self.user = ""
        self.sessionUID = 0
        self.contentserverhost = ""

        BiblioBaseServer.__init__(self, path=BasePath, serverType=S_ADMIN, configfile="server_admin", cat="Admin",newenv="client",logid="serveradmin")
        self.Print("Initializing admin...")
        self._ShowServerInfo()
        
    def _SetupEnvironment(self):        
        # Build the content provider
        BiblioBaseServer._SetupEnvironment(self)             
        self.Env.RegisterSystemProvider(SystemProvider(environment=self.Env))
        self.Print ("Environment succesfully attached")
        
    def _ShowServerInfo(self):
        """ Creation Info about Servers """
        self.Print("Path: %s" % self._env.GetEnvPath() )
        self.Print("Server Version: %s" % self._env.GetVersion() )
        self.Print("Server db Version: %s" % self._env.GetDBVersion() )
        self.Print("started\n")
    
    def PrintCopyright(self):
        print("\n[Welcome to Bibliotheca Admin]")
        print("\n Type 'help' for more info\n")

    def _ParseCommand(self, CommandList ):
        bConsumed = False
        if len(CommandList) > 0:
            # exit or quit
            if self._IsCommands(CommandList[0], "quit") or self._IsCommands(CommandList[0], "exit"):
                self._Exit()
            # help
            if self._IsCommands(CommandList[0], "help"):
                self._PrintHelp()
                bConsumed = True
            try:
                # createuser
                if self._IsCommands(CommandList[0], "createuser"): 
                    self._CreateUser()
                    bConsumed = True
                if self._IsCommands(CommandList[0], "deleteuser"): 
                    self._DeleteUser()
                    bConsumed = True
                if self._IsCommands(CommandList[0], "updateuser"): 
                    self._UpdateUser()
                    bConsumed = True
                if self._IsCommands(CommandList[0], "getuserlist"): 
                    self._GetUserList()
                    bConsumed = True
                if self._IsCommands(CommandList[0], "connect"):
                    self._Connect()
                    bConsumed = True
                if self._IsCommands(CommandList[0], "listservers"):
                    self._ListServers()
                    bConsumed = True
                if self._IsCommands(CommandList[0], "killserver"):
                    self._KillServer()
                    bConsumed = True
                if self._IsCommands(CommandList[0], "runserver"):
                    self._RunServer()
                    bConsumed = True
#===============================================================================
#                if self._IsCommands(CommandList[0], "TestConnection"):
#                    self._TestConn()
#                    bConsumed = True
#===============================================================================

                if not bConsumed:
                    self.Print("Command not found! See help for more info")
            except:
                self.Print("Bad command! See help for more info")
            
    def _ParseServerCommands(self, Server, CommandList):
        bConsumed = False
        bExit = False
        try:
            if len(CommandList) > 0:
                self.Print(" Session UID: %s" % (self.sessionUID))
                if self._IsCommands(CommandList[0], "quit") or self._IsCommands(CommandList[0], "exit"):
                    bExit = True
                    bConsumed = True
                if self._IsCommands(CommandList[0], "shutdown"):
                    Server.shutdown(self.sessionUID)                    
                    bConsumed = True
                
                if self._IsCommands(CommandList[0], "echo"):
                    self.Print(Server.echo(self.sessionUID,CommandList[1]),"ServerResponse")
                    bConsumed = True
                    
                if self._IsCommands(CommandList[0], "ping"):
                    self.Print(Server.ping(self.sessionUID),"ServerResponse")
                    bConsumed = True
                
                if self._IsCommands(CommandList[0], "login"):
                    loginRes = Server.login(self.sessionUID,self.user, self.HashedPass)
                    # Safe the new session ID so we can recover our session in the server
                    self.sessionUID = loginRes[1]
                    self.Print(loginRes,"ServerResponse")                    
                    bConsumed = True
                    
                if self._IsCommands(CommandList[0], "logout"):
                    self.Print(Server.logout(self.sessionUID),"ServerResponse")
                    # Once we logged out we just clear our old session ID
                    self.sessionUID = 0
                    bConsumed = True
                
                if self._IsCommands(CommandList[0], "close"):
                    self.Print(Server.close(self.sessionUID),"ServerResponse")
                    bConsumed = True
                
                if self._IsCommands(CommandList[0], "open"):
                    self.Print(Server.open(self.sessionUID),"ServerResponse")
                    bConsumed = True
                    
                if not bConsumed:
                    self.Print("server command not found")
                    
        except xmlrpclib.ProtocolError, e:
            if e.errcode == -1:
                self.Print("Server Closed")
                bExit = True
        except xmlrpclib.Fault, e:
            self.Print("Fault Received:")
            self.Print(" code: %s" % (e.faultCode))
            self.Print(" faultString: %s" % (e.faultString))
            if e.faultCode == HTTP_IP_BLOCKED:
                bExit = True
        except Exception, e:
            self.Print("Unknown exception %s" % (e))
            bExit = True
        return bExit
            
    def _IsCommands(self, command, token):
        return command.lower() == token.lower()
        
    def HandleInput(self):
        self.PrintCopyright()
        while 1:
            Command = raw_input()
            self._ParseCommand(Command.split())
    
    #----------------------------------------
    # All commands that can me executed
    #----------------------------------------
    def _Exit(self):
        sys.exit()
    
    def _PrintHelp(self):
        print("\n[Help]")
        print("\n=========================================================")
        print("                    Command List")
        print("=========================================================")
        print("\n [General Commands]")
        print(" - help: Print help info")
        print(" - exit: Exit program")
        print(" - quit: Exit program")
        print("\n [User Handling]")
        print(" - createuser: Create a user [ADMIN|PUBLISHER|USER|GUEST]")
        print(" - updateuser: Update a user")
        print(" - deleteuser: Delete a user")
        print(" - getuserlist: queries the whole user list")
        print("\n [Server Handling]")
        print(" - ListServers: List all running servers")
        print(" - KillServer: Kill a running server")
        print(" - runserver: Start a new server instance")
        print(" - connect: Connect to a Biblio login server and execute server commands")
#===============================================================================
#        print(" - TestConnection: Output current connection properties")
#===============================================================================
    
    def _GetUserList(self):
        try:       
            # Delete the user
            UserList = self.Env.SystemProvider.GetUserList()      
            if UserList:
                for User in UserList:
                    print("[%i] Name: %s | permissions: %s" % (User[0], User[1], GetPermissionsString(User[3])))
            else:
                print("User list is empty!")
        except Exception, e:
            print("Error: %s" % (e))
            
    def _DeleteUser(self):
        try:       
            # User Name
            user = raw_input("Username> ") 
            
            # Delete the user
            bResult = self.Env.SystemProvider.DeleteUserByName(user)      
            if bResult:
                print("User Deleted")
            else:
                print("Failed to delete user!")
        except Exception, e:
            print("Error: %s" % (e))
               
    def _UpdateUser(self):
        try:       
            # User Name
            user = raw_input("Username> ")
            
            Id = self.Env.SystemProvider.GetUserID(user)            
            if Id:                
                # User Pass
                password = getpass.getpass("Password> ")
                
                # permissions
                permission = "USER"
                prompt = "Permission [%s]> " % (permission)
                permission = raw_input(prompt).strip() or permission
                finalPermissions = GetPermissions(permission)  
        
                # Create User send create user call
                bResult = self.Env.SystemProvider.UpdateUser(Id, user, password, finalPermissions)      
                if bResult:
                    print("User Updated")
                else:
                    print("Failed to update user!")
            else:
                print("User not found")
        except Exception, e:
            print("Error: %s" % (e))
    
    def _CreateUser(self):
        try:       
            # User Name
            user = raw_input("Username> ")
            
            # User Pass
            password = getpass.getpass("Password> ")
            
            # permissions
            permission = "USER"
            prompt = "Permission [%s] [ADMIN|PUBLISHER|USER|GUEST] > " % (permission)
            permission = raw_input(prompt).strip() or permission
            finalPermissions = GetPermissions(permission)  
    
            # Create User send create user call
            bResult = self.Env.SystemProvider.CreateUser(user, password, finalPermissions)      
            if bResult:
                print("User Created")
            else:
                print("Failed to create user!")
        except Exception, e:
            print("Error: %s" % (e))
            
    def _ListServers(self):
        try:
            # If we come here we just shutdown well!
            ServerList = self.Env.SystemProvider.get_all_servers()
            if ServerList:
                for Server in ServerList:
                    print("[%s] ip: %s | port: %s | pid: %s" % (Server[4],Server[3],Server[2],Server[1]))
            else:
                print("No server running!")
        except Exception, e:
            print("Error catching server list: %s" % (e)) 
    
    def _KillServer(self):
        try:
            pid = raw_input("PID> ")
            # If we come here we just shutdown well!
            self.Env.SystemProvider.release_server(int(pid))
            os.kill(int(pid),9)
            print("Server with pid '%i' killed!" % (int(pid)))
        except Exception, e:
            print("Error Killing Server: %s" % (e))
            
    def _RunServer(self):
        try:
            default = 'y'
            prompt = "run login server? [y/n] [%s]> " % (default)
            answer = raw_input(prompt).strip().lower()
            
            daemonDefault = 'n'
            daemonPrompt = "run in daemon mode? [y/n] [%s]> " % (daemonDefault)
            daemonAnswer = raw_input(daemonPrompt).strip().lower()
                        
            if not bool(len(daemonAnswer)) or daemonAnswer == daemonDefault:
                daemonMode = OPT_EMPTY
            else:
                daemonMode = OPT_DAEMON[0]
            
            if not bool(len(answer)) or answer == default:
                serverMode = OPT_sLogin[0]
            else:
                serverMode = OPT_sContent[0]
                
            Process = subprocess.Popen(["python","bibliotheca.py", daemonMode, serverMode,"&"])
            
#===============================================================================
#            procParams = """
# return code = %s
# process ID = %s
# parent process ID = %s
# process group ID = %s
# session ID = %s
# user ID = %s
# effective user ID = %s
# real group ID = %s
# effective group ID = %s
# """ % (Process, Process.pid, "-1", "-1", Process.getsid(0),
#   Process.getuid(), Process.geteuid(), Process.getgid(), Process.getegid())
#            
#            print procParams
#===============================================================================
            
        except Exception, e:
            print("Error staring Server: %s" % (e))
            
#===============================================================================
#    def _TestConn(self):
#        try:
#            print self._
#        except Exception, e:
#            print("Error staring Server: %s" % (e))  
#===============================================================================
                
    def _Connect(self):
        try:
            # Get IP
            ip = "localhost"
            prompt = "IP [%s]> " % (ip)
            ip = raw_input(prompt).strip() or ip
            
            # Get port            
            port = self.serverport
            prompt = "PORT [%s]> " % (port)
            port = raw_input(prompt).strip() or port
            
            # Get User Name
            user = raw_input("Username> ")
            
            # Get pass
            Pass = getpass.getpass("Password> ")  
            # Pass needs to be hashed!
            self.HashedPass = hashlib.md5(Pass).hexdigest()
            self.user = user
            
            # should be login?
            login = "y"
            prompt = "Login? [y/n] [%s]> " % (login)
            login = raw_input(prompt).strip() or login
            
            # Build connect string
            ConnectString = "http://%s:@%s:%s/" % (self.user,ip,port)
            self.Debug("ConnectionString:<%s>"% ConnectString)
            
            # Connect and start with command handling
            s = xmlrpclib.Server(ConnectString)
            
            if login.lower() == "y":
                loginRes = s.login(0, self.user, self.HashedPass)
                if loginRes[0] == HTTP_LOGIN_FAILED:
                    self.Print("Login Failed!")
                    self.Debug("Login Failed!")
                else:
                    self.sessionUID = loginRes[1]

                    self.Debug("Login OK! - SessionID(%s)" % (self.sessionUID))            

                    self.contentserverhost = loginRes[2]
                    #self.contentserverhost = "localhost:8081"
                    print("Login OK! - SessionID(%s) ContentServer(%s)" % (self.sessionUID,self.contentserverhost))            
                    
                    # Try to make a ping to the content server
                    # just to check if it's running :D
                    if self.contentserverhost != "":
                        # ping me!
                        content_server = xmlrpclib.Server( "http://%s:@%s/" % (self.user,self.contentserverhost) )
                        server_response = ""
                        try:
                            server_response = content_server.ping(self.sessionUID)
                        except Exception, e:
                            print("ERROR: could not make a ping to the content server: %s" % (e))
                            server_response = "failed"
                        print("Content server response: %s" % (server_response))    
                    else:
                        print("No content server online! So no content server related features available!")
            while 1:
                Command = raw_input()
                if self._ParseServerCommands(s,Command.split()):
                    break;
        except Exception, e:
            print("Error Connecting: %s" % (e))        
                
        print("disconnected from server")
