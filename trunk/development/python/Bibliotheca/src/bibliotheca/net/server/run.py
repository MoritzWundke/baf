'''
Created on 10/07/2009

@author: Moss

@summary: Worker module where we define each server entry point
'''

from bibliotheca.net.server.loginserver import BiblioLoginServer
from bibliotheca.net.server.contentserver import BiblioContentServer
from bibliotheca.net.server.serveradmin import BiblioServerAdmin

def RunLoginServer( **kw ):    
    # Start a login server if we can!
    BiblioLoginServer( **kw )

def RunContentServer( **kw ):    
    #TODO: Convert params to keywords too as Login server do
    # Start a content server if we can!
    BiblioContentServer( **kw )

def RunServerAdmin( BasePath ):
    #TODO: Convert params to keywords too as Login server do
    # Start a login server if we can!
    ServerAdmin = BiblioServerAdmin( BasePath )
    ServerAdmin.HandleInput()
