# twisted imports
from twisted.web import server

from bibliotheca.common.types import *

class BiblioSession(server.Session):
    """
    Represent a session for every biblio server
    """
    # Session data assoc indexes
    SESSIONDATA_DBID = 'dbid'
    SESSIONDATA_UID = 'uid'
    SESSIONDATA_USER = 'user'
    SESSIONDATA_IP = 'ip'
    SESSIONDATA_PERMISSION = 'perm'
    
    def __init__(self, site, uid):
        # Before we do anything set the env
        self._env = site.GetEnv()
        
        server.Session.__init__(self, site, uid)

        # Create our session dictionary!
        self._session_data = {}
        self._session_data[self.SESSIONDATA_DBID] = 0
        self._session_data[self.SESSIONDATA_UID] = 0
        self._session_data[self.SESSIONDATA_USER] = ""
        self._session_data[self.SESSIONDATA_IP] = ""
        self._session_data[self.SESSIONDATA_PERMISSION] = INVALID
    
    def SetEnv(self, NewEnv):
        self._env = NewEnv
    
    def DumpSessionData(self):
        #MyTextDump = "| UID: %s | USER: %s | IP: %s | PERMISSION: %i |" % (self._session_data[self.SESSIONDATA_UID], self._session_data[self.SESSIONDATA_USER], self._session_data[self.SESSIONDATA_IP], self._session_data[self.SESSIONDATA_PERMISSION])
        return "| UID: %s | USER: %s | IP: %s | PERMISSION: %i |" % (self._session_data[self.SESSIONDATA_UID], self._session_data[self.SESSIONDATA_USER], self._session_data[self.SESSIONDATA_IP], self._session_data[self.SESSIONDATA_PERMISSION])
        
    def SaveSessionData(self, DBData ):
        """
        Transforms session data taken from the db into a readable dictionary
        """
        if DBData:
            self._session_data[self.SESSIONDATA_DBID] = DBData[0]
            self._session_data[self.SESSIONDATA_UID] = DBData[1]
            self._session_data[self.SESSIONDATA_USER] = DBData[2]
            self._session_data[self.SESSIONDATA_IP] = DBData[3]
            self._session_data[self.SESSIONDATA_PERMISSION] = DBData[4]
    
    def GetSessionData(self):
        return self._session_data
    
    def GetSessionDBID(self):
        return self._session_data[self.SESSIONDATA_DBID]
    
    def GetSessionUID(self):
        return self._session_data[self.SESSIONDATA_UID]
    
    def GetSessionUser(self):
        return self._session_data[self.SESSIONDATA_USER]
    
    def GetSessionIP(self):
        return self._session_data[self.SESSIONDATA_IP]
    
    def GetSessionPermission(self):
        return self._session_data[self.SESSIONDATA_PERMISSION]

    def touch(self):
        """
        Notify session modification. Twsited base class will just change it's own
        value, but our sessions are handeled between servers, so we touch it and
        save the new time value or session table in the system db
        """
        try:
            self._env.SystemProvider.TouchSessionByUID(self.GetSessionUID(),self._getTime())
        except:
            pass
    
    def GetLastModified(self):
        """
        Will return the time when we last modified the session
        """
        try:
            return self._env.SystemProvider.GetLastModifiedByUID(self.GetSessionUID())
        except:
            return self._getTime()

    def checkExpired(self):
        """
        Is it time for me to expire?

        If I haven't been touched in fifteen minutes, I will call my
        expire method.
        """
        # If I haven't been touched in 15 minutes:
        if self._getTime() - self.GetLastModified() > self.sessionTimeout:
            if self.uid in self.site.sessions:
                self.expire()
        
    

class BiblioRequest(server.Request):
    """
    A user's session with a system.

    This utility class contains no functionality, but is used to
    represent a session.

    @ivar sessionTimeout: timeout of a session, in seconds.
    @ivar loopFactory: factory for creating L{task.LoopingCall}. Mainly for
        testing.
    """
    pass

class BiblioSite(server.Site):
    """
    A web site: manage log, sessions, and resources.
    
    @ivar counter: increment value used for generating unique sessions ID.
    @ivar requestFactory: factory creating requests objects. Default to
        L{Request}.
    @ivar displayTracebacks: if set, Twisted internal errors are displayed on
        rendered pages. Default to C{True}.
    @ivar sessionFactory: factory for sessions objects. Default to L{Session}.
    @ivar sessionCheckTime: interval between each check of session expiration.
    """
    requestFactory = BiblioRequest
    sessionFactory = BiblioSession
    
    def __init__(self, parentserver, resource, logPath=None, timeout=60*60*12):
        self.parentserver = parentserver
        server.Site.__init__(self, resource, logPath, timeout)
    
    def GetEnv(self):
        """
        Access the current environment :D
        """
        return self.parentserver.Env
    
    def makeSession(self):
        """
        Generate a new Session instance, and store it for future reference.
        Also assigns the right environment for the session.
        """
        session = server.Site.makeSession(self)
        session.SetEnv(self.GetEnv())
        return session

    def getSession(self, uid):
        """
        Get a previously generated session, by its unique ID.
        This raises a KeyError if the session is not found.
        Also assigns the right environment for the session.
        """
        session = server.Site.getSession(self, uid)
        session.SetEnv(self.GetEnv())
        return session
        
    def __str__(self):
        return "BiblioSite"
