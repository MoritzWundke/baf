'''
Created on 10/07/2009

@author: Moss, Joze
'''

import bibliotheca.common.types

def GetPermissions(PermString):
    # build previleges
    if PermString.upper() == "ADMIN":
        return bibliotheca.common.types.ADMIN
    elif PermString.upper() == "PUBLISHER":
        return bibliotheca.common.types.PUBLISHER
    elif PermString.upper() == "USER":
        return bibliotheca.common.types.USER
    else:
        return bibliotheca.common.types.GUEST

def GetPermissionsString(PermInt):
    # build previleges
    if PermInt == bibliotheca.common.types.ADMIN:
        return "ADMIN"
    elif PermInt == bibliotheca.common.types.PUBLISHER:
        return "PUBLISHER"
    elif PermInt == bibliotheca.common.types.USER:
        return "USER"
    else:
        return "GUEST"