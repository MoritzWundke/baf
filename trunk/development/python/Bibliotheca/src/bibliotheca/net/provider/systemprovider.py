'''
Created on 10/07/2009

@author: Moss, Joze
'''
# Python imports
import hashlib
import xmlrpclib
import os
import subprocess
import getpass

#Bibliotheca imports
from bibliotheca.common.types import *
from bibliotheca.common.baseprovider import *
#TODO: Check it out ... this should be handled by the DatabaseInterface Class
from bibliotheca.database.systemDBdefinitions import *
from bibliotheca.net.server.serverexceptions import *
from bibliotheca.net.provider.providerutil import *
from bibliotheca.common.env.enviromentObjects import *

class SystemProvider(BaseProvider):
    """
    Provides access to the system database, used for sessions, settings and more
    Child classes will access more than just the system db!
    """
    
    def __init__(self, **kw):
        
        env = kw.get('environment', None)
        sectName = kw.get('sectionName', Sect_SystemProvider)
        
        BaseProvider.__init__(self, cat=sectName, environment=env, sectionName=sectName)

        self.Env.DBManager.SanityCheck(serverName=self.Env.GetConfig(Sect_SystemProvider, "serverName"))
        self.ConnectionString = self.Env.GetConfig(Sect_SystemProvider, "connectionstring")
        
    def GetCurrentSchemaVersion(self):
        # returns the current version of the db schema we're using
        return self._env.GetDBVersion()
    
    def _UpdateSchemas(self, schemaVersion):
        # Update schemas if the versions are not equal
        oldVersion = self.get_system_value("dbversion")
        self._UpdateSchemasWorker(oldVersion, self.GetCurrentSchemaVersion())
                
        # Set the new versions
        self.set_system_value("dbversion", self.GetCurrentSchemaVersion())
        self.set_system_value("version", self.Env.GetVersion())
    
    def _UpdateSchemasWorker(self, oldVersion, newVersion):
        self.Print("Update DB From '%s' to '%s'" % (oldVersion, newVersion))
        # Here we should update everything between versions
    
    #########################
    # Access the system db  #
    #########################
    def get_system_value(self, key):
        """
        Get's the value of a system key
        """
        if not self.IsDBInitialized:
            self.Error(self.get_system_value.__name__)
            return
            
        SystemValue = self.get_scalar(self.DB, "SELECT \"Value\" FROM \"%s\" WHERE \"Key\"='%s';", 0, SYSTEM_DATATABLE, key)
        return SystemValue

    def set_system_value(self, key, value):
        """
        Set's the value of a system key
        """
        if not self.IsDBInitialized:
            self.Error(self.get_system_value.__name__)
            return
        
        if self.get_system_value(key):
            self.execute_non_query(self.DB, "UPDATE \"%s\" SET \"Value\"='%s' WHERE \"Key\"='%s'",
                SYSTEM_DATATABLE, value, key)        
        else:
            self.execute_non_query(self.DB, "INSERT INTO \"%s\" (\"Value\", \"Key\") VALUES ('%s', '%s');",
                SYSTEM_DATATABLE, value, key)
    
    ###################################
    # Access the system IP Blacklist  #
    ###################################
    def is_ip_blocked(self, IP):
        """
        Looks into the IP blacklist table to see if the specified IP should be blocked or not
        """
        if self.IsDBInitialized:
            if self.Env.IsUsingBlackIPBlacklist():
                #TODO: Convert all this regular server querys to Stored procedures. More clean, reliable and secure.
                #      Found and error here with field
                BlockedIP = self.get_scalar(self.DB, "SELECT ip FROM \"%s\" WHERE ip='%s';", 0, SYSTEM_SERVER_BLACKLIST, IP)
                if BlockedIP:
                    return True
            
            # We're not using any black list, so all ok :P
            return False
        
        # IF THERE IS NO DB CONNECTION WE BLACK ALL!
        return True

    def bann_ip(self, IP):
        """
        Add an IP into our black list
        """
        if self.IsDBInitialized:
            try:
                self.execute_non_query(self.DB, "INSERT INTO \"%s\" (ip) VALUES ('%s');",
                    SYSTEM_SERVER_BLACKLIST, IP)
            except:
                # Already there? mhhh nothing to do :P
                pass
    
    def unbann_ip(self, IP):
        """
        Remove an IP from our black list
        """
        if self.IsDBInitialized:
            try:
                self.execute_non_query(self.DB, "DELETE FROM \"%s\" WHERE ip='%s';",
                    SYSTEM_SERVER_BLACKLIST, IP)
            except:
                # Not there? mhhh nothing to do :P
                pass
    
    #########################
    # Access the server db  #
    #########################
    def get_server(self, port):
        """
        Get's the pid of the server currently running on a port
        """
        if self.IsDBInitialized:
            try:
                serversql = "SELECT pid, ip FROM \"%s\" WHERE port=%i;" % (SYSTEM_SERVERTABLE, port)
                rs = self.get_result_set(self.DB, serversql)
                if rs:
                    return rs.rows[0][0], rs.rows[0][1]
            except Exception, e:
                self.Error("Error registering server: %s" % (e))
        return None, None

    def set_server(self, port, pid, ip, type):
        """
        Set's the value of a system key
        """
        print "setSERVER"
        if self.IsDBInitialized:
            if self.get_server(port)[0]:
                return False 
            else:
                self.execute_non_query(self.DB, "INSERT INTO \"%s\" (port, pid, ip, servertype) VALUES (%i, %i, '%s', '%s');",
                    SYSTEM_SERVERTABLE, port, pid, ip, type)
                return True
    
    def release_server(self, pid):
        """
        will clean a server port
        """
        if self.IsDBInitialized:
            self.execute_non_query(self.DB, "DELETE FROM \"%s\" WHERE pid=%i;",
                    SYSTEM_SERVERTABLE, pid)
            
    #TODO: Create a PL/PG FUNCION TO HANDLE THIS KIND OF REQUEST!
    def get_all_servers(self):
        """
        Return all current servers registered
        @return: [[id, pid, port, ip, servertype],...]
        """
        serversql = "SELECT * FROM \"%s\"" % (SYSTEM_SERVERTABLE)
        rs = self.get_result_set(self.DB, serversql)
        if rs:  
            return rs.rows
        return None
    
    def get_best_content_server(self):
        """
        Will return the best content server to be assigned to a user.
        @return: str(IP:PORT)
        """
        serversql = "SELECT ip, port FROM \"%s\" WHERE servertype='%s'" % (SYSTEM_SERVERTABLE,S_CONTENT)
        rs = self.get_result_set(self.DB, serversql)
        if rs and len(rs.rows) > 0:
            host = "%s:%s" % (rs.rows[0][0],rs.rows[0][1])
            return host
        return ""
            
                
    #########################
    # Session handling      #
    #########################
    
    # Try to login a user
    def Login(self, request, passtoken, session, User=""):
        if self.IsDBInitialized:      
            # passtoken is what the user has given us to verify if it's him
            if User == "":
                User = request.getUser()
            SavedToken = self.GetPassToken(User)
            if passtoken == SavedToken and session != None:
                # Get stored session data
                UserData = self.GetUser(User)
                
                if UserData[1] != session.GetSessionUser():
                    # Delete Session
                    session.expire()
                    
                    # Create a new session and insert it into the db
                    session = self.InsertSession(request.getClientIP(), UserData[1], UserData[3])

                    # User Changed so create new session and drop the old one
                    self.Print("'%s' has been logged: '%s'" % (User, session.GetSessionData()), "ContentProvider-Login")
                else:
                    self.Print("'%s' is already logged in: '%s'" % (User, session.GetSessionData()), "ContentProvider-Login")
                return True, session
            else:
                # Delete session
                session.expire()
                self.Print("Failed login for '%s' from '%s'" % (User, request.getClientIP()), "ContentProvider-Login")
                return False, session
        
    # Log a user out, this will delete the session!    
    def Logout(self, UID, USER):
        if self.IsDBInitialized:
            try:
                Session = self.RecoverSessionFromUID(UID, USER)
                Session.expire()
                self.Print("Session logged out successful: '%s' " % (Session.GetSessionUser()), "ContentProvider-Logout")
                return True;
            except Exception, e:
                self.Error("Failed to logout: %s" % (e), "ContentProvider-Logout")
                return False
    
    # Clear all active sessions!
    def ClearAllSessions(self):
        if self.IsDBInitialized:
            sql = "DELETE FROM \"%s\"" % (SYSTEM_SESSIONTABLE)
            self.execute_non_query(self.DB, sql)
    
    # There can be only one session active per UID in the session table
    def GetSession(self, UID, USER):
        if self.IsDBInitialized:
            sql = "SELECT * FROM \"%s\" WHERE uid='%s' and \"user\"='%s'" % (SYSTEM_SESSIONTABLE, UID, USER)
            ResultSet = self.get_all(self.DB, sql)[1]
            for Session in ResultSet:
                return Session
            return None
    
    # Get all possible sessions for a sepcific user
    def GetSessionByUser(self, USER):
        if self.IsDBInitialized:
            sql = "SELECT * FROM \"%s\" WHERE \"user\"='%s'" % (SYSTEM_SESSIONTABLE, USER)
            ResultSet = self.get_all(self.DB, sql)[1]

            return ResultSet
    
    # Get all possible sessions for a sepcific user
    def GetSessionByIP(self, IP):
        if self.IsDBInitialized:
            sql = "SELECT * FROM \"%s\" WHERE ip='%s'" % (SYSTEM_SESSIONTABLE, IP)
            ResultSet = self.get_all(self.DB, sql)[1]

            return ResultSet
    
    def RecoverSession(self, request):
        if self.IsDBInitialized:
            # We use the UID from the request to recover tge right session from our DB!
            args, functionPath = xmlrpclib.loads(request.content.read())
            return self.RecoverSessionFromUID(args[0], request.getUser())
    
#===============================================================================
#    def _get_session_object(self, UID):
#        """
#        Returns a valid session object
#        """
#        return BaseProvider._get_session_object(self, UID)
#===============================================================================
    
    def RecoverSessionFromUID(self, UID, USER):
        if self.IsDBInitialized:
            
            self.Debug("[%s]" % (self.RecoverSessionFromUID.__name__))
            # Get and set session       
            s = self.SessionObject(UID)
           
            # Recover a session from the db and save it's data     
            Session = self.GetSession(UID, USER)
            if not Session:
                raise NoSavedSessionFound            
            s.SaveSessionData(Session)
            
            # Touch Session!
            s.touch()
            
            # Save environment, when recovering or creating a session we set the right env
            #s.SetEnv(self.Env)
                
            self.Print("Session recovered: '%s'" % (Session), "ContentProvider-Session")
            
            return s
    
    def InsertSession(self, IP, User=GUEST_NAME, Permission=GUEST):
        if self.IsDBInitialized:
            # Delete all sessions which came from the same IP
            self.ClearSessionByIP(IP)
            
            # Delete all sessions which are using this user!
            self.ClearSessionByUSER(User)
            
            self.Debug(self.Env)
            self.Debug(self.Env.Site)
            # Get a new session for the request
            s = self.Env.Site.makeSession()
            SessionData = None
            
            # Save environment, when recovering or creating a session we set the right env
            #s.SetEnv(self.Env);
            
            # Register the expire callback
            def cbExpire():
                self.ClearSessionByUID(s.uid)
            s.notifyOnExpire(cbExpire)
            
            # Insert the new session
            sql = "INSERT INTO \"%s\" (\"user\", uid, ip, permission) VALUES ('%s', '%s', '%s',%i); " % (SYSTEM_SESSIONTABLE, User, s.uid, IP, Permission)    
            if self.execute_non_query(self.DB, sql):
                SessionData = self.GetSession(s.uid, User)
                self.Print("Session created and stored: '%s'" % (SessionData), "ContentProvider-Session")
            else:
                self.Print("Could not store session for '%s' on '%s'" % (User, IP), "ContentProvider-Session")
            
            s.SaveSessionData(SessionData)

            # Touch Session!
            s.touch()
            
            return s

    # Clear a session using it's UID    
    def ClearSessionByUID(self, UID):
        if self.IsDBInitialized:
            sql = "DELETE FROM \"%s\" WHERE uid='%s'" % (SYSTEM_SESSIONTABLE, UID)
            if self.execute_non_query(self.DB, sql):
                self.Print("Session delete for UID '%s'" % (UID), "ContentProvider-Session")
                return True
            else:
                self.Print("Failed to delete session for UID '%s'" % (UID), "ContentProvider-Session")
                return False
    
    def TouchSessionByUID(self, UID, TouchTime):
        """
        Will renew a session in the db
        """
        try:
            if self.IsDBInitialized:
                sql = "UPDATE \"%s\" SET sessiontime='%s' WHERE uid='%s'" % (SYSTEM_SESSIONTABLE, TouchTime, UID)
                if not self.execute_non_query(self.DB, sql):
                    self.Print("Failed to touch session with UID '%s'" % (UID), "ContentProvider-Session")
        except:
            pass
    
    def GetLastModifiedByUID(self, UID):
        """
        Will renew a session in the db
        """
        try:
            if self.IsDBInitialized:
                TouchTime = float(self.get_scalar(self.DB, "SELECT sessiontime FROM \"%s\" WHERE uid='%s';", 0, SYSTEM_SESSIONTABLE, UID))
                return TouchTime
        except:
            self.Print("Failed to recover last modified time for session with UID '%s'" % (UID), "ContentProvider-Session")
        return 0
    
    # Clear a session using a username   
    def ClearSessionByUSER(self, USER):
        # Get all UID's for this user
        ResultSet = self.GetSessionByUser(USER)
        
        # Make all those session expire!
        for SessionRecord in ResultSet:
            self.Debug("[USER]Clearing SessionRecord %s" % SessionRecord)
            try:
                s = self.Env.Site.getSession(SessionRecord[1])
                s.expire()
            except Exception, e:
                print("ClearSessionByUSER ERROR: %s" % (3))
            
    
    # Clear a session using a username   
    def ClearSessionByIP(self, IP):
        # Get all UID's for this user
        ResultSet = self.GetSessionByIP(IP)
        
        # Make all those session expire!
        for SessionRecord in ResultSet:
            self.Debug("[IP]Clearing SessionRecord %s" % SessionRecord)
            try:
                s = self.Env.Site.getSession(SessionRecord[1])
                s.expire()
            except Exception, e:
                print("ClearSessionByIP ERROR: %s" % (3))
            
    #########################
    # User Handling         #
    #########################
    def CreateUser(self, User, Password, Permission):
        if self.IsDBInitialized:
            # Creates a new user and adds it to the db
            Password = hashlib.md5(Password).hexdigest()
            sql = "INSERT INTO \"%s\" (\"user\", pass, permissions) VALUES ('%s', '%s', %i);" % (SYSTEM_USERTABLE, User, Password, Permission)
            if self.execute_non_query(self.DB, sql):
                self.Print("User '%s' created" % (User), "ContentProvider-User")
                return True
            else:
                self.Print("User '%s' could not be created!" % (User), "ContentProvider-User")
                return False
    
    def UpdateUser(self, id, User, Password, Permission):
        if self.IsDBInitialized:
            sql = "UPDATE \"%s\" SET \"user\"='%s', pass='%s', permissions=%i WHERE id=%i" % (SYSTEM_USERTABLE, User, Password, Permission, id)
            if self.execute_non_query(self.DB, sql):
                self.Print("User '%s' updated" % (User), "ContentProvider-User")
                return True
            else:
                self.Print("User '%s' could not be updated!" % (User), "ContentProvider-User")
                return False
        
    def DeleteUser(self, id):
        if self.IsDBInitialized:
            sql = "DELETE FROM \"%s\" WHERE id=%i" % (SYSTEM_USERTABLE, id)
            if self.execute_non_query(self.DB, sql):
                self.Print("User '%s' deleted" % (id), "ContentProvider-User")
                return True
            else:
                self.Print("User '%s' could not be deleted!" % (id), "ContentProvider-User")
                return False
        
    def DeleteUserByName(self, User):
        try:
            Id = self.GetUserID(User)
            return self.DeleteUser(Id)
        except Exception, e:
            # User could not be deleted!
            #self.Error("Error Deleting User: '%s'" % (e))
            return False
    
    # Returns a pass token used to authenticate the user    
    def GetPassToken(self, User):
        if self.IsDBInitialized:
            #TODO: Convert all this regular server querys to Stored procedures. More clean, reliable and secure.
            HashedPassword = self.get_scalar(self.DB, "SELECT pass FROM \"%s\" WHERE \"user\"='%s';", 0, SYSTEM_USERTABLE, User)
            return "%s:%s" % (User, HashedPassword)
    
    # Return the permissions of a user
    def GetUserPermission(self, User):
        if self.IsDBInitialized:
            Permission = int(self.get_scalar(self.DB, "SELECT permissions FROM \"%s\" WHERE \"user\"='%s';", 0, SYSTEM_USERTABLE, User))
            return Permission
    
    # Return the permissions of a user
    def GetUserID(self, User):
        if self.IsDBInitialized:
            UserID = int(self.get_scalar(self.DB, "SELECT id FROM \"%s\" WHERE \"user\"='%s';", 0, SYSTEM_USERTABLE, User))
            return UserID
    
    def GetUser(self, User):
        if self.IsDBInitialized:
            sql = "SELECT * FROM \"%s\" WHERE \"user\"='%s'" % (SYSTEM_USERTABLE, User)
            ResultSet = self.get_all(self.DB, sql)[1]
            for UserData in ResultSet:
                return UserData
            return None
    
    def GetUserList(self):
        """
        Return the list of all users
        """
        if self.IsDBInitialized:
            sql = "SELECT * FROM \"%s\"" % (SYSTEM_USERTABLE)
            ResultSet = self.get_all(self.DB, sql)[1]
            return ResultSet
        
