'''
Created on 27/04/2009

@author: Moss, Joze
'''
# Python imports

from bibliotheca.net.provider.systemprovider import *
#===============================================================================
# from bibliotheca.net.server.serverexceptions import *
#===============================================================================
from bibliotheca.net.server.serverarquitecture import BiblioSession

#Bibliotheca imports
from bibliotheca.common.types import *
from bibliotheca.common.baseprovider import BaseProvider
#===============================================================================
# from bibliotheca.common.dbhelper import ResultSet
#===============================================================================
#TODO: Check it out ... this should be handled by the DatabaseInterface Class
#===============================================================================
# from bibliotheca.database.systemDBdefinitions import *
# from bibliotheca.database.contentDBdefinitions import *
# from bibliotheca.net.server.serverexceptions import *
#===============================================================================

class ContentServerProvider(SystemProvider):
    """
    The provider user for a content server, will recover sessions and not create them
    also will be used to access the content database to respond content requests from
    the user.
    TODO: We need to make the ContentServer
    """
    
#    def __init__(self, **kw):
#        
#        env = kw.get('environment', None)
#        sectName = kw.get('sectionName', Sect_SystemProvider)
#        
#        BaseProvider.__init__(self, cat=sectName, environment=env, sectionName=sectName)
#
#        self.Env.DBManager.SanityCheck(serverName=self.Env.GetConfig(Sect_SystemProvider, "serverName"))
#        self.ConnectionString = self.Env.GetConfig(Sect_SystemProvider, "connectionstring")
#    
#    def _SetupConnectionStrings(self):
#        # This will setup the conection to the system db
#        SystemProvider._SetupConnectionStrings(self)
#        
#        # So now setup the content db connection
#        self._ContentDBConnectionString = self.Env.GetConfig("Database", "connectionstring")
#        
#        if not self._ContentDBConnectionString:
#            self.Error("No 'connectionstring' in 'Database' config section!")
#            self.Warn("Using default connection string <%s>" % (CONNECTSTRING_CONTENT))
#            self._ContentDBConnectionString = CONNECTSTRING_CONTENT
            
#===============================================================================
#    def _InitializeDBConnection(self):
#        # Initialize the system db connection
#        SystemProvider._InitializeDBConnection(self)
#        
#        # No do the same for the content db connection
#        retries = 2
#        
#        # We still need another connection
#        self.IsDBInitialized = False
# 
#        while ( retries > 0 and not self.IsDBInitialized ):
#            try:
#                self.Print("Connecting to <%s>" % (self._ContentDBConnectionString))
#                self.ContentDB = pgdb.connect (dsn=self._ContentDBConnectionString)
#                self.IsDBInitialized = True
#            except Exception, e:
#                retries = retries - 1
#                self.Error("[%i]Failed to connect to content db using 'dns=%s'\nError: %s\n" %  \
#                           (retries, self._ContentDBConnectionString,e))
#                self._CreateDB()
#        
#        self.Print(" Current connection status:"+str(self.IsDBInitialized))
#===============================================================================
#===============================================================================
#    
#    def _InitializeSchemas(self):
#        SystemProvider._InitializeSchemas(self)
#        
#        # Try to initialize the content db schema
#        # TODO: take it from a file!
#        if not self.IsDBInitialized:
#            self.Error("(content) Schemas could not be checked during initialization.")
#===============================================================================
    
    #########################
    # Session handling      #
    #########################
    
    # Try to login a user
    def Login(self, request, passtoken, session, User=""):
        """ Only implemented by the SystemProvider, a content provider only recovers sessions! """
        pass
        
    # Log a user out, this will delete the session!    
    def Logout(self, UID, USER):
        """ Only implemented by the SystemProvider, a content provider only recovers sessions! """
        pass
    
    #TODO: Check this out!
    def _get_session_object(self, UID):
        """
        Returns a valid session object.
        Here we just create an empty session, later we will fill it with data
        """
        NewSession = BiblioSession(self.Env.Site, UID)
        return NewSession
    
    def InsertSession(self, IP, User=GUEST_NAME, Permission=GUEST):
        """ Only implemented by the SystemProvider, a content provider only recovers sessions! """
        pass
    
    def ClearSessionByUID(self, UID):
        """ Only implemented by the SystemProvider, a content provider only recovers sessions! """
        pass
    
    def ClearSessionByUSER(self, USER):
        """ Only implemented by the SystemProvider, a content provider only recovers sessions! """
        pass
    
    def ClearSessionByIP(self, IP):
        """ Only implemented by the SystemProvider, a content provider only recovers sessions! """
        pass