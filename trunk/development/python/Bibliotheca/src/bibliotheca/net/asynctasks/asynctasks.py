from bibliotheca.common.base import *
from bibliotheca.common.http import *

class BilbioAsyncTask(BaseObject):
    """
    base class that defines a async task handler
    """
    def __init__(self, env, AsyncTaskName=""):
        # Get the task name we will respond to
        self._RespondTask = AsyncTaskName.lower()
        
        # Initialize the base object
        BaseObject.__init__(self, cat="AsyncTask_%s"%self._RespondTask, environment=env)
        
    def initialize(self, args):
        pass
    
    def shoudld_process(self, AsyncTaskName):
        return self._RespondTask == AsyncTaskName.lower()
    
    def process(self, KeysValues):
        AsyncTaskReturnDict = {}
        return ASYNC_RETURN_CODE_NONE, AsyncTaskReturnDict
    
    def end(self, args):
        pass
    
class MyNewAsyncTask(BilbioAsyncTask):
    """
    Sample class that can be used as a start for creating new async tasks
    """
    def __init__(self, env, AsyncTaskName=""):
        # Call super
        BilbioAsyncTask.__init__(self, env, AsyncTaskName)
        
        # Now we can create database stuff and all that
        
    def initialize(self, args):
        pass
    
    def process(self, KeysValues):
        AsyncTaskReturnDict = {}
        return ASYNC_RETURN_CODE_MyNewAsyncTask, AsyncTaskReturnDict
    
    def end(self, args):
        pass