from asynctasks import *

def register_async_tasks(env):
    """
    Method called from a content server once it will get initialized.
    Here we register what async tasks handlers will be used for every
    client request.
    {'AsyncTaskName':handlerObject}
    """
    AsyncTaskDict = {}
    
    # Create an fill the dictionary
    
    # This is the way we fill up the task handlers
    AsyncTaskDict['MyNewAsyncTask'] = MyNewAsyncTask(env,'MyNewAsyncTask')
    
    return AsyncTaskDict