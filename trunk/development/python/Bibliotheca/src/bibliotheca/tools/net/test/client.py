# Client usin standart python lib
import xmlrpclib
s = xmlrpclib.Server("http://user:pass@192.168.69.112:8080/")
try:
    s.echo("hi")
except Exception, e:
    print("Error: %s" % (e))

# Client using twisted cleint
from twisted.web.xmlrpc import Proxy
from twisted.internet import reactor

def printValue(value):
    print(repr(value))
    reactor.stop()
    
def printError(error):
    print("Error: %s" % (error))
    reactor.stop()

proxy = Proxy("http://user:pass@192.168.69.112:8080/")
proxy.callRemote('echo', 'hi').addCallbacks(printValue,printError)
reactor.run()