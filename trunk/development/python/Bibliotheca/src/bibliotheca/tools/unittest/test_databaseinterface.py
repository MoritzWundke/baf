'''
Created on 17/08/2009

@author: joze

@summary

    This is the very first unit test for our project (yey!!)
    
    Some interesting things:
        -    All your test should start with "test" prefix in order to let the system found it.
             That can be changed overriding TestLoader.testMethodPrefix
        -    All test got a Setup and TearDown 
'''
import unittest

from bibliotheca.database.platforms.dbiPostgres import *

class TestDatabaseInterface(unittest.TestCase):

    def setUp(self):
        ''' Set up any thing before each test
        '''
        pass

    def tearDown(self):
        ''' Reset property after each test
        '''
        self.dbi = None
    
    def testExistingInterfaces(self):
        ''' Checking all DB Interfaces defined.
        '''
        for adbi in AvailableDBIs:
            if adbi == DBI_Postgres:
#===============================================================================
#                from bibliotheca.database.platforms.dbiPostgres import *
#===============================================================================
                self.dbi = PGInterface(owner=self)
                self.assertTrue(isinstance(self.dbi, PGInterface))
            else:
                self.fail("There is a DBI Available which is not included on tests")

    def suite(self):
        ''' return the official setup of tests by this module. This way we can ENABLE or DISABLE
            any test and dont fuckup the CIS i.e.
        '''
        tests = ['testExistingInterfaces',\
                  ]

        #TODO:   This should be subclassed with a superclass for biblio project 
        #        By checking the value of the self.availableTest != None  
        # Activate this return if you plans to disable any test
        #return unittest.TestSuite(map(self._class__, tests))
        # Use this other to return all tests
        return unittest.TestLoader().loadTestsFromTestCase( self )
    
class SuiteDatabaseInterface(unittest.TestSuite):

    def suite(self):
        '''
            Here we compound the complete test suite for all the modules defined on the project
        '''
        loader = unittest.TestLoader()
        loader.sortTestMethodsUsing = None
        suitesContainer = []
        suitesContainer.append( loader.loadTestsFromTestCase( TestDatabaseInterface ))
                                
        return SuiteDatabaseInterface(suitesContainer)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()