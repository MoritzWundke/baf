'''
Created on 17/08/2009

@author: joze

@summary

    This is the very first unit test for our project (yey!!)
    
    Some interesting things:
        -    All your test should start with "test" prefix in order to let the system found it.
             That can be changed overriding TestLoader.testMethodPrefix
        -    All test got a Setup and TearDown 
'''
import unittest
import sys

class TestBasics(unittest.TestCase):
    def setUp(self):
        ''' Set up any thing before each test
        '''
        pass

    def tearDown(self):
        ''' Reset property after each test
        '''
        pass
        
    #@unittest.skipIf(sys.version_info < (2, 6), "python 2.6 required. Upgrade your python")
    def testPythonVersion(self):
        ''' Python 2.6 or superior
        '''
        self.failIf(sys.version_info < (2, 6, 0), "python 2.6 required. Upgrade your python")
    
    #@unittest.skipUnless(sys.platform.startswith("linux"), "Bibliotheca server requires Linux")
    def testPlatform(self):
        ''' Platform is Linux
        '''
        self.failUnless(sys.platform.startswith("linux"), "Bibliotheca server requires Linux")
    
    def suite(self):
        ''' return the official setup of tests by this module. This way we can ENABLE or DISABLE
            any test and dont fuckup the CIS i.e.
        '''
        tests = ['testPythonVersion',\
                 'testPlatform',\
                  ]

        #TODO:   This should be subclassed with a superclass for biblio project 
        #        By checking the value of the self.availableTest != None  
        # Activate this return if you plans to disable any test
        return unittest.TestSuite(map(self._class__, tests))
        # Use this other to return all tests
        #return unittest.TestLoader().loadTestsFromTestCase( self )
        
class SuiteBasic(unittest.TestSuite):

    def suite(self):
        '''
            Here we compound the complete test suite for all the modules defined on the project
        '''
        loader = unittest.TestLoader()
        loader.sortTestMethodsUsing = None
        suitesContainer = []
        suitesContainer.append( loader.loadTestsFromTestCase( TestBasics ))
                                
        return SuiteBasic(suitesContainer)
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()