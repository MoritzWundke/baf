'''
Created on 17/08/2009

@author: joze
'''
import unittest

from bibliotheca.tools.unittest.test_objects import *
from bibliotheca.tools.unittest.test_xmlrunner import *

class TestBibliotheca(unittest.TestSuite):

    def suite(self):
        '''
            Here we compound the complete test suite for all the modules defined on the project
        '''
#===============================================================================
#        loader = unittest.TestLoader()
#        loader.sortTestMethodsUsing = None
#        suitesContainer = []
#        suitesContainer.append( loader.loadTestsFromTestCase( TestBasics ))
#        suitesContainer.append( loader.loadTestsFromTestCase( TestDatabaseInterface ))
#===============================================================================
        suitesContainer = []
        
        for suiteModule in AllTestSuites:
            suitesContainer.append(suiteModule.suite())
                                
        return TestBibliotheca(suitesContainer)

def main(**kw):

    print "Doing tests..."
    print kw

    if kw.get('xmlOutput',False):
        #run XMLRunner
        print "XML Runner..."
        print AllTestSuites
        
        #Remove any argument from the system...it collide with some behaviour on tests
        sys.argv = sys.argv[0]
        
        for suite in AllTestSuites:
            print suite
            newDict = dict({'suite': suite})
            newDict.update(kw)
            print "NewDict", newDict
            xmlRunner = XMLTestProgram(**newDict)

    else:
        #run default TextRunner
        print "Text Runner..."
        unittest.TextTestRunner(verbosity=kw.get('verbosity',2)).run(TestBibliotheca().suite())
    
if __name__ == "__main__":
    import sys
#    print "TEST ARGUMENTS", sys.argv
    main(sys.argv)
