'''
Created on 28/08/2009

@author: joze

@summary: Hub module where any test is defined and included into any test process defined over the project
'''

from bibliotheca.tools.unittest.test_basics import *
from bibliotheca.tools.unittest.test_databaseinterface import *

AllTestSuites = [   SuiteBasic(),\
                    SuiteDatabaseInterface(),\
                ]