'''
Created on 10/08/2009

@author: joze

@summary: Module to handle subprocess calls in a standard way
'''

import subprocess

# Flags
shCMD_flagRoot      = 'requireRoot'
shCMD_flagUser      = 'requireUser'
shCMD_flagString    = 'asString'
shCMD_requireRoot   = 'sudo'
shCMD_userName      = 'userName'


def ComposeCmnd(Cmnd, **kw):
    """ Depending on the classes and options, return the proper exec command
    """
    if kw[shCMD_flagRoot]:
        # Adding superuser flag 
        Cmnd = [shCMD_requireRoot,] + Cmnd
    else:
        if (kw[shCMD_flagUser] and not kw[shCMD_userName] is None):
            Cmnd = [shCMD_requireRoot + ' -u ' + kw[shCMD_userName],] + Cmnd
    
    if not kw[shCMD_flagString]:    
        # return Cmnd as a List
        return Cmnd
    
    # Return Cmnd as a string
    argString = ''
    for foo in Cmnd:
        argString += foo + ' '
    
    return argString.strip()

def runCmnd(Cmnd, **kw):
    """ Run safely calls to the SO
    
        This basic structure executes command, wait if for finish and return the stdout result.
        
        Exceptions should be caught by the module calls this function
    """

    # Building composer flags in different object to avoid problems with default Ops
    flags = {}
    flags[shCMD_flagRoot]   = kw.pop(shCMD_flagRoot, False)
    flags[shCMD_flagString] = kw.pop(shCMD_flagString, False)
    flags[shCMD_flagUser]   = kw.pop(shCMD_flagUser, False)
    flags[shCMD_userName]   = kw.pop(shCMD_userName, None)
    
    # Composing the final exec string
    CMND = ComposeCmnd(Cmnd, **flags)

    # Standard out
    DefaultOpts = { 'stdout' : subprocess.PIPE }
    # Have read on subprocess doc that Shell should be true on Cmnds passed as strings 
    DefaultOpts['shell'] = isinstance(CMND, str)
    
    # Add keywords given by user to Popen call
    if len(kw) > 0:
        DefaultOpts.update(kw)
        
    #TODO: Check this in the future, dont want this process to fuckup somehting
    process = subprocess.Popen(CMND, **DefaultOpts)
    process.wait()
    return process.stdout
