'''
Created on 12/08/2009

@author: joze

@summary:     This Object would let us to handle different arquitectures and also will help us to abstract Database Operations
            required by our system.
            Keeping it simple and easier to understand more over than specifics implementation of postgres ;)
'''
from bibliotheca.common.moduleCodes import *

DBI_Postgres = MODULE_POSTGRES_CODE

AvailableDBIs = [ DBI_Postgres, ]

fieldInit_Owner = 'owner' # The current server which is going to use the interface

class DBInterface(object):
    '''
        Database Manager Class
    '''
    def __init__(self, **kw):
        '''
        Constructor
        '''
        self._server = kw.get('owner', None)
    
        #Checking is everything Ok with requested server.
        # REMEMBER THIS OPERATIONS SHOULD BE WITH A PROVIDER!
        #self.SanityCheck()
        
    #--- -->METHODS
    def SanityCheck(self, **kw):
        ''' This method ensure we made all local checks necessary to ensure server reliability and security
        '''
        raise NotImplementedError
    
    def InitializeConnection(self, **kw):
        ''' Test connection with the current provider
        '''
        raise NotImplementedError
    
    def InitializeDatabaseSuperUser(self, **kw):
        ''' Tries to setup user superuser
        '''
        raise NotImplementedError
        
    def CreateDatabase(self, **kw):
        ''' Once the cluster is known to be ready, on a fresh install or server switch we should 
            give it a way to rebuild its DB and Schema
        '''
        raise NotImplementedError
    
    def CreateSchema(self, **kw):
        ''' Once the cluster is known to be ready, on a fresh install or server switch we should 
            give it a way to rebuild its DB and Schema
        '''
        raise NotImplementedError
    
    #--- -->PROPERTIES
    def _GetServerObject(self):
        return self._server
    
    Server = property (_GetServerObject)
     