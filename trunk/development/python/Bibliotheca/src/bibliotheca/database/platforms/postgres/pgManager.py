'''
Created on 05/08/2009

@author: joze

@summary: This module will provide methods to interact clusters hosted by the local machine.

    Available Methods:
    pgInit
    pgCreateCluster
    pgStartCluster
    pgStopCluster
    pgRestartCluster

    Roadmap:
    pgDumpDB ( schema, data, both)
    pgImportDB
'''
import re

from pgExceptions import *
from bibliotheca.tools.shell.runCmnd import *

#===============================================================================
# MODULE CONSTANTS
#===============================================================================
#TODO: These constants should be moved to any other place where they could be configured by user
PATH_TO_BBDDs = '/prod/BBDDs'
PG_VERSION='8.3' 

#TODO: Shell Flags should be placed in a proper module
SHELL_Flag_SuperUser = 'sudo'

# CLUSTER Commands
pgCMD_ListClusters = 'pg_lsclusters'
pgCMD_CreateCluster = 'pg_createcluster'
pgCMD_ManageCluster = 'pg_ctlcluster'

pgCMD_MC_start = 'start'
pgCMD_MC_stop = 'stop'
pgCMD_MC_restart = 'restart'

pgCMD_ManageClusterCommands = [ pgCMD_MC_start, pgCMD_MC_stop, pgCMD_MC_restart]

# DB Commands
pgCMD_CreateDB  = 'createdb'
pgCMD_Console   = 'psql'

# Root Flagged Operations
pgCMD_RequireROOT = [pgCMD_CreateCluster,pgCMD_ListClusters]
pgCMD_RequireDBUser = [pgCMD_CreateDB,pgCMD_Console]
pgCMD_AsString    = [pgCMD_CreateCluster,pgCMD_ListClusters,pgCMD_CreateDB, pgCMD_Console]


pgLC_Header_Version = 'Version'
pgLC_Header_Cluster = 'Cluster'
pgLC_Header_Port    = 'Port'
pgLC_Header_Status  = 'Status'
pgLC_Header_Owner   = 'Owner'
pgLC_Header_Data    = 'Data'
pgLC_Header_Log     = 'Log'

# FIELDS
PGC_USER        = 'pgUser'
PGC_SERVERNAME  = 'pgServerName'
PGC_SERVERPORT  = 'pgServerPort'

# DEFAULT VALUES
DEFAULT_PG_USER = 'postgres'
#===============================================================================

#--- -->CLUSTER METHODS
def pgGetExistingClusters(server=None):
    """
        List Existing Clusters
    """
    
    try:
        result = runCmnd([pgCMD_ListClusters,], \
                         asString=pgCMD_ListClusters in pgCMD_AsString, \
                         requireRoot=pgCMD_ListClusters in pgCMD_RequireROOT)
        
    except Exception, e:
        raise pgException(code=PG_E03, message=e)
        result.close()
        return []

    header = [b.strip(' \n') for b in result.readline().split() if re.search('^[A-Z]',b)] 
    data   = []
    while True:
        line=result.readline()
        if line=='':
            break
        data.append([b.strip(' \n') for b in line.split()])

    if server:
        server.Debug(header)
        server.Debug(data)
    return header,data

def pgCheckExistingCluster(server, header, data, serverName, serverPort):
    """
        Check Cluster Existence
    """
    found = [item for row in data if serverName in row for item in row]

    if len(found) == 0:
        server.Debug("Cluster <%s> not found..." % (serverName))
        return None
        
    if not serverPort is None:  
        if found[header.index(pgLC_Header_Port)] != serverPort:
            raise pgException(code=PG_E04)

        PortDuplicated = [item for row in data if serverPort in row and serverName not in row for item in row]
        if len(PortDuplicated) > 0:
            raise pgException(code=PG_E05)
        
    server.Debug('[%s] - Server found' % (pgCheckExistingCluster.__name__))
    server.Debug(['%s:%s' % (header[index],found[index]) for index in range(len(header))])
    return dict([(header[index],found[index]) for index in range(len(header))])

def pgCreateCluster(server, serverName, serverPort):
    """
        Here we create any desired cluster following Postgres 8.3 tools.
        For further documentation see 'pg_createcluster --help'
        As our servers do nothing without our admin module running(at least with commands at start up),
        our server should remain off until they get launch implicity by the admin program(together with twisted server)
        
        Current prototype syntax: 'pg_createcluster -start-conf manual -d PATH_TO_BBBBs/clusterName -p DESIRED_PORT_IF_ANY 8.3 CLUSTERNAME'
        
        Options not implemented yet:
            - cluster owner and superuser
            - group for data files !! (check this as soon as possible. Will help a lot on a fresh installation!)
            - path to desired log directory
            - encoding and locales
    """
    
    arg = []
    arg.append(pgCMD_CreateCluster)
    arg.append('--start-conf manual')
    arg.append('-d %s/%s' %(PATH_TO_BBDDs, serverName))
    
    if not serverPort is None:
         arg.append('-p %s' % (serverPort))
    
    arg.append(PG_VERSION)
    arg.append(serverName)
    
    server.Debug(arg)
               
    try:
        result = runCmnd(arg, \
                         asString=pgCMD_CreateCluster in pgCMD_AsString, \
                         requireRoot=pgCMD_CreateCluster in pgCMD_RequireROOT)
        
    except Exception,e:
        raise pgException(code=PG_E06)
        result.close()
    
    data   = []
    while True:
        line=result.readline()
        if line=='':
            break
        data.append(line)
        
    server.Debug(data)
    result.close()

def pgManageCluster(server, serverName, **kw):
    """
        Here we create any desired cluster following Postgres 8.3 tools.
        For further documentation see 'pg_createcluster --help'
        As our servers do nothing without our admin module running(at least with commands at start up),
        our server should remain off until they get launch implicity by the admin program(together with twisted server)
        
        Current prototype syntax: 'pg_createcluster -start-conf manual -d PATH_TO_BBBBs/clusterName -p DESIRED_PORT_IF_ANY 8.3 CLUSTERNAME'
        
        Options not implemented yet:
            - cluster owner and superuser
            - group for data files !! (check this as soon as possible. Will help a lot on a fresh installation!)
            - path to desired log directory
            - encoding and locales
    """
    
    CMND = kw.get('command', None)
    
    if not CMND or not CMND in pgCMD_ManageClusterCommands:
        raise pgException(code=PG_E07) 
    
    arg = []
    arg.append(pgCMD_ManageCluster)
    arg.append(PG_VERSION)
    arg.append(serverName)
    arg.append(CMND)
    
    server.Debug(arg)
                
    try:
        result = runCmnd(arg, \
                         asString=pgCMD_CreateCluster in pgCMD_AsString, \
                         requireRoot=pgCMD_CreateCluster in pgCMD_RequireROOT)
    except Exception,e:
        raise pgException(code=PG_E06)
        result.close()
    
def pgInit(server, **kw):
    """ Initialization procedure for the entire Cluster management.
    """
    
    if server is None:
        raise pgException(code=PG_E01)
    
    pgUser = kw.get(PGC_USER, DEFAULT_PG_USER)
    pgServerName = kw.get(PGC_SERVERNAME, None)
    pgServerPort = kw.get(PGC_SERVERPORT, None)
    
    if pgServerName is None:
        raise pgException(code=PG_E02)
#===============================================================================
#     else:
#         pgServerName += datetime.datetime.now().strftime('%Y%m%j%H%M%S')
#===============================================================================
    
    #result from 'pg_lsclusters' command
    header, data = pgGetExistingClusters()
    #Dictionary with an existing server values
    serverData = pgCheckExistingCluster(server, header, data, pgServerName, pgServerPort)
    
    if serverData is None:
        pgCreateCluster(server, pgServerName, pgServerPort)
        header, data = pgGetExistingClusters()
        serverData = pgCheckExistingCluster(server, header, data, pgServerName, pgServerPort)
        #TODO: CLUSTER CONF.FILE
        # Here we should modify, add or delete any param we need to make cluster works as desire
    
        #TODO: CERTIFICATES ... HERE WE SHOULD PROVIDE REQUIRED CERTIFICATES AND KEYS

    #RETURN SERVER CONFIGURATION
    return serverData
    

#--- -->DATABASE METHODS
def pgCreateDB(**kw):
    ''' 
    Uso:
  createdb [OPCIoN]... [NOMBRE] [DESCRIPCIoN]

Opciones:
  -D, --tablespace=TBLSPC   tablespace por omision de la base de datos
  -E, --encoding=CODIFICACIoN codificacion para la base de datos
  -O, --owner=DUEnyO         usuario que sera dueno de la base de datos
  -T, --template=PLANTILLA     plantilla de base de datos a copiar
  -e, --echo                mostrar las ordenes enviadas al servidor
  --help                    mostrar esta ayuda y salir
  --version                 mostrar el numero de version y salir

Opciones de conexion:
  -h, --host=ANFITRIoN      nombre del servidor anfitrion de la base de datos o directorio del socket
  -p, --port=PUERTO         puerto del servidor
  -U, --username=NOMBREDEUSUARIO    nombre de usuario para la conexion
  -W, --password            forzar la peticion de contrasenya
    '''
    
    arg = []
    arg.append(pgCMD_CreateDB)
  
    port = kw.get('port', None)
    if port is None:
        raise  pgException(code=PG_E09)
    arg.append('-p %s' % (port))
    
    owner = DEFAULT_PG_USER
    arg.append('-O %s' % (owner))
    
       
    database = kw.get('databaseName', None)
    if database is None:
        raise  pgException(code=PG_E10)
    arg.append(database)
    
    #if any
    arg.append(kw.get('description',''))
    
    try:
        result = runCmnd(arg, \
                         asString=True, \
                         requireUser=True,\
                         userName=DEFAULT_PG_USER)
    except Exception,e:
        raise pgException(code=PG_E11, message=e)
        result.close()
        
def pgCreateSchema(**kw):
    ''' 
        We will create any schema by loading a sql file through the psql console command.
        #TODO: Schema versioning
        We need to make a crc check of the file and autoincrement a schema version value on each change.
        
        example: sudo -u USER psql -p CLUSTER_PORT -f provider_schema_file.sql
    '''
    
    arg = []
    arg.append(pgCMD_Console)
  
    port = kw.get('port', None)
    if port is None:
        raise  pgException(code=PG_E09)
    arg.append('-p %s' % (port))
    
    provider = kw.get('provider', None)
    if provider is None:
        raise pgException(code=PG_E15)
    
    SchemaSqlFile = kw.get('schemaFile', None)
    if SchemaSqlFile is None:
        raise  pgException(code=PG_E16)
    
    arg.append('-f %s' % (SchemaSqlFile))
    
    database = kw.get('databaseName', None)
    if database is None:
        raise  pgException(code=PG_E10)
    arg.append(database)

    try:
        result = runCmnd(arg, \
                         asString=True, \
                         requireUser=True,\
                         userName=DEFAULT_PG_USER)
    except Exception,e:
        raise pgException(code=PG_E14, message=e)
        result.close()
    
def pgInitializeDBSuperUser(**kw):
    ''' We launch a console command to update superuser with the given password
    
        example: sudo -u postgres psql -p 5433 -c "alter user postgres with encrypted password '12345'"
    '''
    
    arg = []
    arg.append(pgCMD_Console)
  
    port = kw.get('port', None)
    if port is None:
        raise  pgException(code=PG_E09)
    arg.append('-p %s' % (port))
    
    plainPass = kw.get('plainPass', None)
    if plainPass is None:
        raise  pgException(code=PG_E13)
    
    user = kw.get('userName', DEFAULT_PG_USER)
    arg.append('-c "alter user %s with encrypted password \'%s\'"' % (user,plainPass))
    
    try:
        result = runCmnd(arg, \
                         asString=True, \
                         requireUser=True,\
                         userName=DEFAULT_PG_USER)
    except Exception,e:
        raise pgException(code=PG_E14, message=e)
        result.close()
        