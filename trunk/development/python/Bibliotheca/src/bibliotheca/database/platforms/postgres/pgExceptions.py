'''
Created on 05/08/2009

@author: joze

@summary: Module where the PostGres interaction exceptions will be handled
'''

from bibliotheca.common.bibExceptions.bibliothecaCustomExceptions import *
from bibliotheca.common.moduleCodes import *

#ERR0R DESCRIPTION
PG_E00 = ['CODE_PostgresUndefinedError', 'Undefined error occurred while postgres managing operation']
PG_E01 = ['CODE_ServerNotProvided', 'Server object was not provided or its null during postgres managing operations']
PG_E02 = ['CODE_ServerNameNotProvided', 'Operation could not be completed. Server name was not provided']
PG_E03 = ['CODE_ListingClusterError', '']
PG_E04 = ['CODE_ClusterPortDoesNotMatch', 'Desired port does not match with current configuration.Please Check postmaster.conf']
PG_E05 = ['CODE_ClusterPortDuplicated', 'Desired port is being used at least by one other server.Please Check postmaster.conf']
PG_E06 = ['CODE_CannotCreateCluster', 'Need superuser access to create clusters.\n' +\
                                        'Run your pg_createcluster command by yourself or ' + \
                                        'Add your user to biblioadmin group and add these lines to sudoers file:' + \
                                        'User_Alias      BIBLIOTHEKA = YOUR_USER_NAME' + \
                                        'Cmnd_Alias      PG_MANAGER = /usr/bin/pg_createcluster' + \
                                        'BIBLIOTHEKA ALL = NOPASSWD : PG_MANAGER' + \
                                        '%biblioadmin ALL = NOPASSWD : PG_MANAGER']
PG_E07 = ['CODE_ClusterManagingCommandError', 'Command provided during cluster management unknown or not given']
PG_E08 = ['CODE_ConnectionStringError', 'Connection string malformed or not present on config file']
PG_E09 = ['CODE_CouldNotCreateDatabase', 'Port value not provided']
PG_E10 = ['CODE_CouldNotCreateDatabase', 'Database value not provided']
PG_E11 = ['CODE_UnknownErrorDuringDatabaseCreation', '']
PG_E12 = ['CODE_UserNameNotPresent', 'The connection string is malformed']
PG_E13 = ['CODE_PlainPasswordNotPresent', 'The connection string is malformed']
PG_E14 = ['CODE_ErrorDuringConsoleCommand', '']
PG_E15 = ['CODE_ProviderObjectNotPresent', 'Provider Object required not present during an Schema load']
PG_E16 = ['CODE_SchemaFileNotPresent', 'Property that defines Schema file for this provider is invalid or is None']

#ERROR LISTING
PG_ERROR_CODES = [PG_E00, \
                  PG_E01, \
                  PG_E02, \
                  PG_E03, \
                  PG_E04, \
                  PG_E05, \
                  PG_E06, \
                  PG_E07, \
                  PG_E08, \
                  PG_E09, \
                  PG_E10, \
                  PG_E11, \
                  PG_E12, \
                  PG_E13, \
                  PG_E14, \
                  PG_E15, \
                  PG_E16, \
                  ]

DEFAULT_ERROR_CODE = PG_E00

class pgException(bibCustomExceptions):
    """
    PostgreSQL management Exceptions
    """
    def __init__(self, **kw):
        """ CONSTRUCTOR
        """
        MODULE_CODE = getModuleCode(MODULE_POSTGRES_CODE)
        
        code = kw.get('code', DEFAULT_ERROR_CODE)
        
        if not code in PG_ERROR_CODES:
            code = DEFAULT_ERROR_CODE
            
        message = kw.get('message', code[1])
        bibCustomExceptions.__init__(self, \
                                     moduleCode = MODULE_CODE, \
                                     exceptionCode = PG_ERROR_CODES.index(code), \
                                     exceptionMessage = message)
    
