'''
Created on 12/08/2009

@author: joze

@summary: Postgres Interface for database interaction
'''
# Python imports
import pgdb
import re

# Bibliotheca imports
from bibliotheca.database.databaseInterface import *
from bibliotheca.database.dbhelper import *
from bibliotheca.database.platforms.postgres.pgManager import *
from bibliotheca.database.dbiExceptions import *
from bibliotheca.common.env.enviromentObjects import *

class PGInterface(DBInterface, DBHelper):
    '''
    classdocs
    '''

    def __init__(self, **kw):
        '''
        Constructor
        '''
        DBInterface.__init__(self, **kw)
        
    def SanityCheck(self, **kw):
        ''' This method ensure we made all local checks necessary to ensure server reliability and security
        '''
        serverName = kw.get('serverName', None)
        self.serverData = pgInit(self.Server, pgServerName=serverName)
        
        if self.serverData and self.serverData[pgLC_Header_Status] != 'online':
            # Starting Cluster
            pgManageCluster(self.Server, serverName, command=pgCMD_MC_start)
        
    def InitializeConnection(self, **kw):
        ''' Test connection with the current provider
        '''
        
        provider = kw.get('provider', None)
        
        if not provider:
            raise dbiException(code=DBI_E02)
        
        connString = provider.ConnectionString
        if not connString:
            raise dbiException(code=DBI_E03)
        
        hostString = self.Server.Env.GetDictionaryObject(FCBD_HOST, section=provider.SectionName)
        isLocalCluster = self.Server.Env.GetDictionaryObject(FCBD_ISLOCALSERVER, section=provider.SectionName)
        
        if not isLocalCluster and hostString is None:
            raise dbiException(code=DBI_E04)
        
        # if Host string is not defined, but local, we can found which port our server is running
        # Need that due to our multicluster system that have clusters running on different ports
        if not hostString:
            serverName = self.serverData[pgLC_Header_Cluster]
            serverPort = self.serverData[pgLC_Header_Port]
            hostString = '%s:%s' % ('localhost', serverPort)

        # We're only capable to createDB is if its local server
        # TODO: Add ssh capability on this layer
        retries = 3 if isLocalCluster else 1
        while ( retries > 0 and not provider.DB ):
            try:
                self.Server.Print("[%i] - Connecting to <%s|%s>" % (retries, hostString,connString))
                provider.DB = pgdb.connect (dsn=connString,host=hostString)
            except Exception, e:
                retries = retries - 1
                if retries == 0 :
                    self.Server.Error("[%i]Failed to connect to system db using 'dns=%s'\n'host=%s'\nError: %s\n" %  \
                                      (retries, connString,hostString, e))
                errorMessage = str(e)
                if re.search("password", errorMessage):
                    self.Server.Warn("New cluster. SuperUser password should be initialized.\nPlease, Change it as soon as possible.")
                    self.InitializeDatabaseSuperUser(provider=provider,hostString=hostString)
                else:
                    #Cluster OK but not DB or Schema. Should create it!
                    self.Server.Warn("New cluster. Creating required DB for this provider.")
                    self.CreateDatabase(provider=provider,hostString=hostString)
                    self.Server.Warn("New cluster. Creating Schema for this provider.")
                    self.CreateSchema(provider=provider,hostString=hostString)
              
        self.Server.Print("Current connection status:"+str(True if provider.DB else False))
        
    def InitializeDatabaseSuperUser(self, **kw):
        ''' Tries to setup user superuser
        '''
        provider = kw.get('provider', None)
        hostPort = kw.get('hostString', None).split(':')
        pgInitializeDBSuperUser(port=hostPort[1],
                                userName=provider.ConnectionString.split(':')[2],\
                                plainPass=provider.ConnectionString.split(':')[3])
        
    def CreateDatabase(self, **kw):
        ''' Once the cluster is known to be ready, on a fresh install or server switch we should 
            give it a way to rebuild its DB and Schema
        '''

        provider = kw.get('provider', None)
        hostPort = kw.get('hostString', None).split(':')       
        pgCreateDB(provider=provider,\
                   port=hostPort[1],\
                   databaseName=provider.ConnectionString.split(':')[1])
        
    def CreateSchema(self, **kw):
        ''' Once the cluster is known to be ready, on a fresh install or server switch we should 
            give it a way to rebuild its DB and Schema
        '''
        
        provider = kw.get('provider', None)
        hostPort = kw.get('hostString', None).split(':')
        pgCreateSchema(provider=provider,\
                       port=hostPort[1],\
                       databaseName=provider.ConnectionString.split(':')[1],\
                       schemaFile=self.Server.Env.GetDictionaryObject(FCBD_SCHEMAFILE,\
                                                                         section=provider.SectionName,\
                                                                         default=FCBD_SCHEMAFILE_V[1]))
        