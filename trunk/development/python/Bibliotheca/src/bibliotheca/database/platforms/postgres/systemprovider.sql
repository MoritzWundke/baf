/*
Created on 13/08/2009

@author: Joze
*/

CREATE TABLE "biblioUserData"
(
  id bigserial NOT NULL,
  username text NOT NULL,
  pass text NOT NULL,
  permissions integer NOT NULL,
  CONSTRAINT "biblioUserData_pkey" PRIMARY KEY (id),
  CONSTRAINT "biblioUserData_user_key" UNIQUE (username)
)
WITH (OIDS=FALSE);

---ALTER TABLE "biblioUserData" OWNER TO postgres;

CREATE TABLE "biblioSystemData"
(
  "Key" text NOT NULL,
  "Value" text NOT NULL,
  CONSTRAINT "biblioSystemData_pkey" PRIMARY KEY ("Key"),
  CONSTRAINT "biblioSystemData_Key_key" UNIQUE ("Key")
)
WITH (OIDS=FALSE);
---ALTER TABLE "biblioSystemData" OWNER TO postgres;

CREATE TABLE "biblioSessions"
(
  id bigserial NOT NULL,
  uid text,
  username text,
  ip text,
  permission integer NOT NULL,
  sessiontime numeric,
  CONSTRAINT "biblioSessions_pkey" PRIMARY KEY (id),
  CONSTRAINT "biblioSessions_ip_key" UNIQUE (ip)
)
WITH (OIDS=FALSE);
---ALTER TABLE "biblioSessions" OWNER TO postgres;

CREATE TABLE "biblioServers"
(
  id bigserial NOT NULL,
  pid integer,
  port integer,
  ip text,
  servertype text,
  CONSTRAINT "biblioServers_pkey" PRIMARY KEY (id),
  CONSTRAINT "biblioServers_port_key" UNIQUE (port)
)
WITH (OIDS=FALSE);
---ALTER TABLE "biblioServers" OWNER TO postgres;

CREATE TABLE "biblioIPBlackList"
(
  ip text NOT NULL,
  CONSTRAINT "biblioIPBlackList_pkey" PRIMARY KEY (ip)
)
WITH (OIDS=FALSE);
---ALTER TABLE "biblioIPBlackList" OWNER TO postgres;

