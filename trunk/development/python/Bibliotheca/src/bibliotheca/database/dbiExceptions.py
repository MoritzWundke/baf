'''
Created on 12/08/2009

@author: joze

@summary: Module where the Database Interface exceptions will be handled
'''

from bibliotheca.common.bibExceptions.bibliothecaCustomExceptions import *
from bibliotheca.common.moduleCodes import *
from bibliotheca.database.databaseInterface import *

#ERR0R DESCRIPTION
DBI_E00 = ['CODE_DatabaseInterfaceUndefinedError', 'Undefined error occurred while DBI managing operation']
DBI_E01 = ['CODE_UnknownInterface','Interface not provided or unknown value.' +\
                                    'Please fix your DatabaseInterfaceClass value in config.\n' +\
                                    'Available values: %s' % (AvailableDBIs)
                                    ]
DBI_E02 = ['CODE_ProviderCannotInitializeConnection', 'Provider object was not ready to stabilish a connection using DBI']
DBI_E03 = ['CODE_ConnectionStingNotValid', 'Provider object does not have a valid connection string']
DBI_E04 = ['CODE_UnknownHost', 'Provider does not know non-local host to connect. Please, use host property on your provider connection section.']

#ERROR LISTING
DBI_ERROR_CODES = [DBI_E00, \
                   DBI_E01, \
                   DBI_E02, \
                   DBI_E03, \
                   DBI_E04, \
                  ]

DEFAULT_ERROR_CODE = DBI_E00

class dbiException(bibCustomExceptions):
    """
    DBI management Exceptions
    """
    def __init__(self, **kw):
        """ CONSTRUCTOR
        """
        MODULE_CODE     = getModuleCode(MODULE_DBI_CODE) 
        code = kw.get('code', DEFAULT_ERROR_CODE)
        
        if not code in DBI_ERROR_CODES:
            code = DEFAULT_ERROR_CODE
            
        message = kw.get('message', code[1])
        bibCustomExceptions.__init__(self, \
                                     moduleCode = MODULE_CODE, \
                                     exceptionCode = DBI_ERROR_CODES.index(code), \
                                     exceptionMessage = message)