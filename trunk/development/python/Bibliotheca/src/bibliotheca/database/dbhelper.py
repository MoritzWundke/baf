import pgdb

class ResultSet:
    """ the result of calling getResultSet """
    def __init__ (self, (columnDescription, rows)):
        self.columnDescription, self.rows = columnDescription, rows
        self.columnMap = self.get_column_map()

    def get_column_map ( self ):
        """This function will take the result set from getAll and will
        return a hash of the column names to their index """
        h = {}
        i = 0
        if self.columnDescription:
            for col in self.columnDescription:
                h[ col[0] ] = i
                i+=1
        return h;

    def value(self, col, row ):
        """ given a row(list or idx) and a column( name or idx ), retrieve the appropriate value"""
        tcol = type(col)
        trow = type(row)
        if tcol == str:
            if(trow == list or trow == tuple):
                return row[self.columnMap[col]]
            elif(trow == int):
                return self.rows[row][self.columnMap[col]]
            else:
                print ("rs.value Type Failed col:%s  row:%s" % (type(col), type(row)))
        elif tcol == int:
            if(trow == list or trow == tuple):
                return row[col]
            elif(trow == int):
                return self.rows[row][col]
            else:
                print ("rs.value Type Failed col:%s  row:%s" % (type(col), type(row)))
        else:
            print ("rs.value Type Failed col:%s  row:%s" % (type(col), type(row)))

    def json_out(self):
        json = "[%s]" % ',\r\n'. join(
            [("{%s}" % ','.join(
            ["'%s':'%s'" %
             (key, str(self.value(val, row)).
              replace("'","\\'").
              replace('"','\\"').
              replace('\r','\\r').
              replace('\n','\\n'))
             for (key, val) in self.columnMap.items()]))
             for row in self.rows])
        #mylog.debug('serializing to json : %s'% json)
        return json
    
class DBHelper():
    """
    Simple database helper class
    """

    def get_all(self, db, sql, params=""):
        """
        Executes the query and returns the (description, data)
        """
        if self.IsDBInitialized:
            cur = db.cursor()
            desc  = None
            data = None
            try:
                cur.execute(sql)
                data = list(cur.fetchall())
                desc = cur.description
                db.commit();
            except Exception, e:
                self.Error('There was a problem executing sql:%s \n \
                    with parameters:%s\nException:%s'%(sql, params, e))
                db.rollback();
            
            return (desc, data)

    def execute_non_query(self, db, sql, *params):
        """
        Executes the query on the given project
        """
        if self.IsDBInitialized:
            cur = db.cursor()
            try:
                cur.execute(sql % params)
                db.commit()
                Status = True
            except Exception, e:
                self.Error('There was a problem executing sql:%s \n \
                    with parameters:%s\nException:%s'%(sql, params, e))
                db.rollback();
                Status = False
            return Status

    def get_first_row(self, db, sql,*params):
        """
        Returns the first row of the query results as a tuple of values (or None)
        """
        if self.IsDBInitialized:
            cur = db.cursor()
            data = None;
            try:
                cur.execute(sql % params)
                data = cur.fetchone();
                db.commit();
            except Exception, e:
                self.Error('There was a problem executing sql:%s \n \
                    with parameters:%s\nException:%s'%(sql, params, e))
                db.rollback()
            return data;

    def get_scalar(self, db, sql, col=0, *params):
        """
        Gets a single value (in the specified column) from the result set of the query
        """
        data = self.get_first_row(db, sql, *params);
        if data:
            return data[col]
        else:
            return None;
    
    def execute_in_trans(self, db, *args):
        """
        Executes a several pairs of (sql,params) taken from *args
        """
        if self.IsDBInitialized():
            success = True
            cur = db.cursor()
            try:
                for sql, params in args:
                    cur.execute(sql % params)
                db.commit()
            except Exception, e:
                self.Error('There was a problem executing sql:%s \n \
                    with parameters:%s\nException:%s'%(sql, params, e))
                db.rollback();
                success = False
            return success
        
    def Command(self, conn, cmd, bWithinTransaction=True):
        """
        Execute a single command and check status
        """
        print pgdb.version
        print cmd
        
        if not bWithinTransaction:
            print "Setting autocommit on connection"
            conn.autocommit = int(1)
#===============================================================================
#        print "Autocommit[%i]" % (conn.autoCommit)
#===============================================================================
        try:
            currentCursor = conn.cursor()
            currentCursor.autocommit = 1
            print "Autocommit[%i]" % (currentCursor.autocommit)
            currentCursor.execute(cmd)
        except Exception, e:
            print e
            print Exception
            currentCursor = None
            
        if not bWithinTransaction:
            conn.autocommit = 0
            
        return currentCursor
        
    def DbExists(self, conn, value):
        """
        Checks if a Db exists
        """
        if self.IsDBInitialized:
            sql = "SELECT * FROM %s Where datname='%s';" % ('pg_database',value);

            currentCursor = self.Command(conn, sql)
            result = currentCursor.fetchall()
            print result, len(result)
            
            if result and len(result) > 0:
                print "before True"
                return True
       
        print "before False"
        return False
    
    def TableExists(self, db, table):
        """
        Checks if table exists in specified db
        """
        
        sql = "SELECT * FROM \"%s\" LIMIT 1" % table
        cur = db.cursor()
        has_table = True;
        try:
            cur.execute(sql)
            db.commit()
        except Exception, e:
            print e
            has_table = False
            db.rollback()
        return has_table

    def get_column_as_list(self, db, sql, col=0):
        """
        Get's a list of all values in a specified column
        """
        if self.IsDBInitialized():
            data = self.get_all(db, sql)[1] or ()
            return [valueList[col] for valueList in data]

    def get_result_set(self, db,sql):
        """
        Executes the query and returns a Result Set
        @return: ResultSet
        """
        tpl = self.get_all(db, sql);
        if tpl and tpl[0] and tpl[1]:
            return ResultSet(tpl)
        else:
            return None
