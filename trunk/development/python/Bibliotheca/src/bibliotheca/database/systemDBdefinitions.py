'''
Created on 10/07/2009

@author: Moss, joze
'''


CONNECTSTRING_SYSTEM = 'localhost:biblio_system:postgres:12345'
CONNECTSTRING_CONTENT = 'localhost:biblio_content:postgres:12345'

SYSTEM_USERTABLE = "biblioUserData"
SYSTEM_USERTABLE_SCHEMA = """
CREATE TABLE "biblioUserData"
(
  id bigserial NOT NULL,
  "user" text NOT NULL,
  pass text NOT NULL,
  permissions integer NOT NULL,
  CONSTRAINT "biblioUserData_pkey" PRIMARY KEY (id),
  CONSTRAINT "biblioUserData_user_key" UNIQUE ("user")
)
WITH (OIDS=FALSE);
ALTER TABLE "biblioUserData" OWNER TO postgres;
"""

SYSTEM_DATATABLE = "biblioSystemData"
SYSTEM_DATATABLE_SCHEMA = """
CREATE TABLE "biblioSystemData"
(
  "Key" text NOT NULL,
  "Value" text NOT NULL,
  CONSTRAINT "biblioSystemData_pkey" PRIMARY KEY ("Key"),
  CONSTRAINT "biblioSystemData_Key_key" UNIQUE ("Key")
)
WITH (OIDS=FALSE);
ALTER TABLE "biblioSystemData" OWNER TO postgres;
"""

SYSTEM_SESSIONTABLE = "biblioSessions"
SYSTEM_SESSIONTABLE_SCHEMA = """
CREATE TABLE "biblioSessions"
(
  id bigserial NOT NULL,
  uid text,
  "user" text,
  ip text,
  permission integer NOT NULL,
  sessiontime numeric,
  CONSTRAINT "biblioSessions_pkey" PRIMARY KEY (id),
  CONSTRAINT "biblioSessions_ip_key" UNIQUE (ip)
)
WITH (OIDS=FALSE);
ALTER TABLE "biblioSessions" OWNER TO postgres;
"""

SYSTEM_SERVERTABLE = "biblioServers"
SYSTEM_SERVERTABLE_SCHEMA = """
CREATE TABLE "biblioServers"
(
  id bigserial NOT NULL,
  pid integer,
  port integer,
  ip text,
  servertype text,
  CONSTRAINT "biblioServers_pkey" PRIMARY KEY (id),
  CONSTRAINT "biblioServers_port_key" UNIQUE (port)
)
WITH (OIDS=FALSE);
ALTER TABLE "biblioServers" OWNER TO postgres;
"""

SYSTEM_SERVER_BLACKLIST = "biblioIPBlackList"
SYSTEM_SERVER_BLACKLIST_SCHEMA = """
CREATE TABLE "biblioIPBlackList"
(
  ip text NOT NULL,
  CONSTRAINT "biblioIPBlackList_pkey" PRIMARY KEY (ip)
)
WITH (OIDS=FALSE);
ALTER TABLE "biblioIPBlackList" OWNER TO postgres;

"""