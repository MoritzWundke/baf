# Bibliotheca imports
from bibliotheca.common.base import BaseObject
from bibliotheca.database.databaseInterface import *
from bibliotheca.database.dbiExceptions import *
# INTERFACES
from bibliotheca.database.platforms.dbiPostgres import *

# Python Imports
import os

# twisted imports
from twisted.internet import reactor #, defer

S_LOGIN     = 'login'
S_CONTENT   = 'content'
S_ADMIN     = 'admin'

class BiblioBaseServer(BaseObject):
    
    def __init__(self, **kw):
        """ Base Initialization for servers"""
        
        """
            When want to check on ubuntu if postgres is installed
            dpkg-query -W -f='${Package}-${Version}\n' "postgresql"
        """
#===============================================================================
#        try:
#===============================================================================
        if True:
            BaseObject.__init__(self, **kw)
    
            self._serverType    = kw.get('serverType',None)
            self._serverMode    = kw.get('serverMode', None)
            
#===============================================================================
#            if self._daemonMode:
#                # Temporary point for DaemonServer
#                os.open(self.Env.logFile, os.O_CREAT|os.O_APPEND|os.O_RDONLY) # stdin
#                os.open(self.Env.logFile, os.O_CREAT|os.O_APPEND|os.O_RDWR)   # stdout
#                os.open(self.Env.logFile, os.O_CREAT|os.O_APPEND|os.O_RDWR)   # stderr
#                print("Restoring STDs in Daemon mode")
#===============================================================================
            
            self.Print("\n\n========================= [ STARTING %s SERVER ] =========================\n" % (self.ServerType.upper()))
        
    #===============================================================================
    #        # TODO: Make a InitMessage work properly
    #        self.Print(self.__initMessage)
    #===============================================================================
            self._SetupEnvironment()
            
            # Nothing to do here for Administrative servers( Not yet!)
            if self.ServerType == S_ADMIN:
                return
            
            # Check if server already exists         
            if not self._OnlineCheck():
                
                CheckPID, CheckIP = self.Env.SystemProvider.get_server(self.serverport)
    
                if CheckPID is not None:
                    self.Error("Can not create content server: There is already a content server running!")
                    return
                
                self._RegisterServer()
                 
                self._CreateResources()
                self._ShowServerInfo()
                
                exit = reactor.run()
                        
                # If we come here we exited well :P
                self.Print( "port '%i' available again!" % (self.serverport) )
                self.Env.SystemProvider.release_server(os.getpid())
            else:
                self.Error("There is a sever running on this port!")
            
#===============================================================================
#        except Exception, e:
#             self.Error("Error creating %s server: %s" % (self._serverType,e) )
#             
#             # If we come here something went wrong, so try to release the port
#             if self.Env.SystemProvider:
#                 self.Env.SystemProvider.release_server(os.getpid())
#===============================================================================

             
    def _GetServerType(self):        
        """ Server Type handling """
        return self._serverType
    
    def _SetServerType(self,value):        
        """ Server Type handling """
        self._serverType = value

    ServerType = property(_GetServerType, _SetServerType)
    
    def _GetDaemonMode(self):
        """ Tell if process is running or not in Daemon mode """
        return self._serverMode
        
    def _SetDaemonMode(self, value):
        """ Set Daemon mode on server. Should be only updated on initialization"""
        
        if (value is None):
            self.Error("Error setting daemon mode. \"None\" value received")

        self._serverMode = value
        
    DaemonMode = property(_GetDaemonMode, _SetDaemonMode)
    
    def _GetXMLRPCObj(self):        
        """ Server Type handling """
        return self._XMLRPCObj
    
    def _SetXMLRPCObj(self,value):        
        """ Server Type handling """
        self._XMLRPCObj = value
        
    XMLRPCObj = property (_GetXMLRPCObj, _SetXMLRPCObj)
    
    def _GetSite(self):        
        """ Server Type handling """
        return self._Site
    
    def _SetSite(self,value):        
        """ Server Type handling """
        
        self._Site = value
        
        Structures = [self.XMLRPCObj, self.Env]
        
        if self._Site is not None:
            for s in Structures:
                if s is None:
                    self.Warn("Structure %s that should be filled with SITE object is NONE" % (s))
                    continue
                
                s.Site = self._Site
                
                self.Print("%s class has been asigned with %s" % \
                           (s.__class__, self._Site))
        else:
            self.Warn("Site is being assigned with a NONE value.")
        
    Site = property (_GetSite, _SetSite)

    def _SetupEnvironment(self):
        """ Build the content provider """
        self.Print("Setting up Environment")
        #XMLRPC Initialization
        self._XMLRPCObj     = None
        self._Site          = None
        self.Env._DBManager = None
        
        self._RegisterDatabaseInterface()
        
    def _RegisterDatabaseInterface(self):
        """ Here we should setup our Database interface, making implementation independent from the arquitecture
        """
        # Here we read enviroment setup and create the propper Object
        
        self.Debug('Registering <%s> as current DBI' % (self.Env.DatabaseInterfaceClass))
        
        if self.Env.DatabaseInterfaceClass == DBI_Postgres:
            self.Debug("DB Interface found. Applying it")
            # Adding DBManager to the environment. This way we take advantage of Availability
            self.Env.DBManager = PGInterface(owner=self)
        else:
            raise dbiException(code=DBI_E01)
        
    def _RegisterServer(self):
        """ Registering server onto the environment """
        
        self.Env.SystemProvider.set_server(self.serverport, os.getpid(), self.GetIP(), self.ServerType)
        self.Debug("[%s] Server Registered with %d" % (self.ServerType,os.getpid()))
                
    def _CreateResources(self):
        """ Resources required on Init """
        pass
    
    def _ShowServerInfo(self):
        """ Creation Info about Servers """
        # Print some useful stuff    
        self.Print( "Path: %s" % self.Env.GetEnvPath() )
        self.Print( "Closed: %s" % self.Env.IsClosed() )
        self.Print( "Version: %s" % self.Env.GetVersion() )
        self.Print( "started on %s::%s" % (self.GetIP(), self.serverport) )
        self.Print( "Daemon Mode: %s" % self.DaemonMode)
        self.Print( "process pid %s" % (os.getpid()) )
    
    def _ShutDown(self):       
        # Stop Server
        reactor.stop()
    
    def ShutDown(self):       
        # Stop Server
        self._ShutDown()
    
    def GetIP(self):
        """
        A very strange way to get you public IP! But it's quite the only way to catch it!
        """
        import urllib2
        import re
        
        url = urllib2.urlopen("http://www.whatismyip.com/automation/n09230945.asp")
        file = url.read()
        
        buffer = ""
        ip = ""
        for char in file:
            if char == "\n":
                if buffer.find("Your IP Address Is") != -1:
                    buffer = buffer[35:buffer.find("</h1>")]
                    buffer = buffer.replace("<span>","")
                    buffer = buffer.replace("</span>","")
                    buffer = buffer.replace(";","")
                    pattern = re.compile("\&\#\d\d")
                    for ascii in pattern.findall(buffer):
                        buffer = buffer.replace(ascii,chr(int(ascii[2:4])))
                    print buffer
                    break
                buffer = ""
            else:
                buffer = buffer + char
        ip = buffer
        
        return ip
    
    def _OnlineCheck(self, type = None):
        """ Proxy for Check availability, dependings on its type """
        
        if self._serverType is None:
            self.Warn("Server type was not defined.", "Servers")
            return False
        
        return self._ServerOnline(self.ServerType)
    
    def _ServerOnline(self, type = None):
        """
        Looks if there is already a server running, returns true in case there is otherwise returns false
        """
        
        if not type:
            self.Warn("Server type was not defined.", "Servers")
            return False
        
        ServerList = self.Env.SystemProvider.get_all_servers()
        if ServerList:
            for Server in ServerList:
                if Server[4] == type:
                    self.Warn("[%s] ip: %s | port: %s | pid: %s" % (Server[4],Server[3],Server[2],Server[1]))
                    return True 
        return False
    
