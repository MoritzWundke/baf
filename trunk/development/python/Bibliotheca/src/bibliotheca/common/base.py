#===============================================================================
# import types
#===============================================================================
import traceback
from bibliotheca.common.env.environment import Environment
from bibliotheca.common.env.serverEnvironment import ServerEnvironment

#--- [BASEOBJECT CLASS]
class BaseObject():
    def __init__(self, **kw):
        
        # Setting Cathegory to be checked on environment actions
        self._cat = str(kw.get('cat','biblio'))
        
        # Recovering and existing environment
        env = kw.get('environment',None)
        
        if env:
            self._env = env
            self.Debug("[%s]Environment keyword received. Environment object propagated." % (self._env.__class__))
        else:
            env = str(kw.get('newenv','client'))
            path = str(kw.get('path',''))
            logid = str(kw.get('logid','0'))
            configpath = ("%sbibliotheca/config/%s.ini") % (path,str(kw.get('configfile','biblio')))
            
            if env.lower() == "client":
                self._SetEnv( Environment(path=path,configpath=configpath,logid=logid))
            if env.lower() == "server":
                self._SetEnv( ServerEnvironment(path=path,configpath=configpath,logid=logid))
                
            self.Debug("No environment received, so we have configured it successfully...")
            self.Debug("[%s] Environment created." % (env.upper()))
        
    #--- -->Getters and Setters
    def _GetEnv(self):
        return self._env
    
    def _SetEnv(self, value):
        self._env = value

    #--- -->PROPERTIES
    Env = property ( _GetEnv, _SetEnv)
    
    def _checklogcat(self,cat):
        if self._env.ShouldCheckLogCat():
            CatList = self._env.GetLogCatList()
            for TestCat in CatList:
                if TestCat.lower() == cat.lower():
                    return True
            return False
        else:
            # if we do not check for log categories print all!
            return True
    
    def Debug(self,msg,cat=""):
        # Use this only in debug mode
        if self.Env.IsDebugMode():
            if cat == "":
                cat = self._cat
    
            # print and/or log     
            if self._env.IsDebugMode():
                self._env.Print("[DEBUG]: %s" % (msg),cat)
            if self._env.IsLogging():
                self._env.debug(msg,cat)
            
    def Print(self,msg,cat=""):
        if cat == "":
            cat = self._cat
        
        # is cat in our list? 
        if self._checklogcat(cat):
            # print and/or log     
            self._env.Print(msg,cat)
            if self._env.IsLogging():
                self._env.info(msg,cat)
                
    def Warn(self,msg,cat=""):
        if cat == "":
            cat = self._cat

        # print and/or log     
        self._env.Print("[WARN]: %s" % (msg),cat)
        if self._env.IsLogging():
            self._env.warning(msg,cat)
     
    def Error(self,msg,cat=""):         
        if cat == "":
            cat = self._cat
        
        # print the trace!
        trace = traceback.format_exc()
        if trace is "None\n":
            trace = "\n%s" % (trace)
        else:
            trace = ""
        # print and/or log     
        self._env.Print("[ERROR]: %s%s" % (msg,trace),cat)
        if self._env.IsLogging():
            self._env.error("%s%s" % (msg,trace),cat)
    
    def Critical(self,msg,cat=""):         
        if cat == "":
            cat = self._cat

        # print and/or log     
        self._env.Print("[CRITICAL]: %s" % (msg),cat)
        if self._env.IsLogging():
            self._env.critical(msg,cat)
                       
#--- [DBOBJECT CLASS]
class DBObject(BaseObject):
    def __init__(self, env):
        BaseObject.__init__(self, env)
        self._initialized = False
        
    def IsInitialized(self):
        return self._initialized
    
    def _Initialize(self):
        self._initialized = True
        
    def Serialize(self):
        # Seriablize Object
        return ""
    def UnSerialize(self):
        # UnSeriablize Object
        return ""
