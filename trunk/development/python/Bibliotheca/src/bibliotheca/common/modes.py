'''
Created on 28/07/2009

@author: joze
'''

# Basic
OPT_EMPTY = ""

# Server Commands
OPT_sLogin      = ["runloginserver", "l"]
OPT_sAdmin      = ["runserveradmin", "a"]
OPT_sContent    = ["runcontentserver", "c"]
OPT_UndefServer = ["ServerUndefined"]

# Servers types
OPT_SERVERS     = [OPT_sLogin, OPT_sAdmin, OPT_sContent]
OPT_DaemonCapableServer = [OPT_sLogin, OPT_sContent]

# Command line options
OPT_DAEMON = ["-d", "--daemon"]
OPTS = [OPT_DAEMON,] 