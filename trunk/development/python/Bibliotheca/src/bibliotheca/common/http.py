# Extended http response codes (we start in 1000 so we will not collide with the base http
HTTP_FIRST                  = 10000
HTTP_IP_BLOCKED             = HTTP_FIRST
HTTP_LOGIN_OK               = HTTP_FIRST + 1
HTTP_LOGOUT_OK              = HTTP_FIRST + 2
HTTP_LOGIN_FAILED           = HTTP_FIRST + 3
HTTP_LOGOUT_FAILED          = HTTP_FIRST + 4
HTTP_SERVER_CLOSED          = HTTP_FIRST + 5
HTTP_PERMISSION_REQUIRED    = HTTP_FIRST + 6
HTTP_SESSION_INVALID        = HTTP_FIRST + 7
HTTP_NO_SUCH_FUNCTION       = HTTP_FIRST + 8
HTTP_SERVER_IS_CLOSED       = HTTP_FIRST + 9

RESPONSES = {
             HTTP_IP_BLOCKED: "IP Blocked",
             HTTP_LOGIN_OK: "Login OK",
             HTTP_LOGOUT_OK: "Logout OK",
             HTTP_LOGIN_FAILED: "Login Failed",
             HTTP_LOGOUT_FAILED: "Logout Failed",
             HTTP_SERVER_CLOSED: "Server Closed",
             HTTP_PERMISSION_REQUIRED: "Permission Required",
             HTTP_SESSION_INVALID: 'Invalid Session',
             HTTP_NO_SUCH_FUNCTION: 'No Such Function',
             HTTP_SERVER_IS_CLOSED: 'Server Is Closed, only an admin on localhost has access',
             }

# Should be use restricted IPs? If true we only let IP's 
# interact that are in our permitted table. The blocked ones are still blocked!
USE_RESTRICTED_IPS = True
VALID_IPS = ['127.0.0.1','192.168.']
BLOCKED_IPS = []

# Session data assoc indexes
SESSIONDATA_DBID = 'dbid'
SESSIONDATA_UID = 'uid'
SESSIONDATA_USER = 'user'
SESSIONDATA_IP = 'ip'
SESSIONDATA_PERMISSION = 'perm'

# Asyn Task defines
ASYNC_RETURN_CODE_NONE = 0

# This is an example on how to create your own return codes
ASYNC_RETURN_CODE_MyNewAsyncTask = ASYNC_RETURN_CODE_NONE + 1
