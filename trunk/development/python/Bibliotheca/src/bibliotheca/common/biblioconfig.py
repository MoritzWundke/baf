""" @Owner Tragnarion Studios.2008

    @Author Joze Castellano
    
    @Description
    
    Container Base class that provides the methods to handle objects
    inside a dictionary.
    
    It also provide the Methods to get and set any elements on it.
"""

from bibliotheca.common.biblioparser import *
from bibliotheca.common.env.enviromentObjects import *
#
#cmd_debugStatus = 'debug'
#cmd_owner = 'owner'

class BiblioConfig(BiblioParser):
    """ Base class for those objects that needs to operates with objects, 
        managing them with a dictionary.
        
        Provides methods to get and set elements using the dictionary
    """
    def __init__( self, **kw ):
        """ Initializing method
        
            Setting up systems:
                Private variables
                - dict  dictionary for objects
            
        """
        BiblioParser.__init__(self, **kw)
        self._dict  = {}
        self._defSections = set(kw.get('DefaultSections', []))
        self._defFields = kw.get('DefaultFields', {})
        
        self._InitializeConfig()
#===============================================================================
#        self.ConfigureDebug( Debug )
#        self._dict[cmd_debugStatus] = Params.get(cmd_debugStatus, False)
#        self._dict[cmd_owner] = Params.get(cmd_owner, None)
#===============================================================================
              

#--- -->Base Methods Methods---#000000#FFFFFF--------------------------------------------
    def GetDictionaryObject( self, key, **kw ):
        """ Encasulates the way we ask class for an object
        """
#===============================================================================
#        print "----->Dictionary Check Start---"
#===============================================================================
        currSect = kw.get('section', None)
        for sect in self._defSections:
            if not currSect is None:
                break
#===============================================================================
#            print "[%s] looking <%s>" % (key,sect)
#===============================================================================
            if self._defFields[sect].has_key(key):
                currSect = sect
#                print "%s found in section %s!" % (key, sect)
            
        if currSect is None: 
#            print "Returning default!"
            return kw.get('default', None)
        
        if not self._dict[currSect].has_key(key):
            return kw.get('default', None)
        
        return self._dict[currSect][key]

    def SetDictionaryObject( self, section, key, value, **kw ):
        """ Encasulates the way we ask class for an object
        """
        if not self._dict.has_key(section):
            self.AddNewSection(section)
            
        if self._dict[section].has_key( key ) and not kw.get('overwrite', True):
            return False
        
        self.AddNewOptionValue(section, key, value)
        return True
    
#--- -->Property Methods
    def _GetDictionary(self):
        """ Return the Full Dictionary
        """
        return self._dict

#===============================================================================
#    def GetDebugStatus( self ):
#        """ Get method for DebugStatus Object
#        """
#        return self.GetDictionaryObject( cmd_debugStatus )
#    
#    def SetDebugStatus( self, value ):
#        """ Set method for DebugStatus Object
#        """
#        return self.SetDictionaryObject( cmd_debugStatus, value )
#    
#    def GetOwner( self ):
#        """ Get method for DebugStatus Object
#        """
#        return self.GetDictionaryObject( cmd_owner )
#    
#    def SetOwner( self, value ):
#        """ Set method for DebugStatus Object
#        """
#        return self.SetDictionaryObject( cmd_owner, value )
#===============================================================================
 
#--- -->Properties---#000000#FFFFFF-------------------------------------------------
    Dictionary = property ( _GetDictionary )
    
#--- -->Class Methods---#000000#FFFFFF-----------------------------------------------
    def _InitializeConfig(self):
        
        # Read the file, done in biblioparser
        self.ReadConfigFile()

        # Look at defSections and defFields on init to check how they are populated
        self._CheckConfigSections(self._defSections, self._defFields)
           
    def ReadConfigFile( self, **kw ):
        """ 
            ReadConfig file passing our property dictionary holder
        """
        
        localFlags = {'targetDict':self.Dictionary,\
                      }
        
        kw.update(localFlags)
        BiblioParser.ReadConfigFile(self, **kw)
        
    def AddNewSection(self, section):
        """ Add a new section to both current dictionary and config File
        """
        print "Adding Section <%s>" % (section)
        BiblioParser.AddNewSection(self, section)
        
        if not self.Dictionary.has_key(section):
            self.Dictionary[section] = {}
            
    def AddNewOptionValue(self, section, option, value):
        """ Add a new option and its value to an existen section
        """
        print "Adding new value <%s=%s> to section <%s>" % (option,value,section)
        BiblioParser.AddNewOptionValue(self, section, option, value)
        self.Dictionary[section][option] = value
        
    def _CheckConfigSections(self, ReqSections, ReqFields):
        """ Create them giving a set of sections and fields
        """
        for section in ReqSections:
#===============================================================================
#            print section
#===============================================================================
            #Checking required sections existence
            if not self.Dictionary.has_key(section) and len(ReqFields[section]) > 0:
                self.NewSection(section)
                
#===============================================================================
#            print ReqFields[section]
#===============================================================================
            for option, value in ReqFields[section].iteritems():
                if not self.HasConfigOption(section, option):
                    self.AddNewOptionValue(section, option, value)
                        
        self.SaveConfigToDisk()
        