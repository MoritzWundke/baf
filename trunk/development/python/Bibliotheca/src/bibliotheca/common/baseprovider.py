'''
Created on 08/07/2009

@author: Moss, Joze
'''
#Python
import os

#Bibliotheca Imports
from bibliotheca.common.base import BaseObject
#TODO: THIS HELPER SHOULD BE INCLUDED BY THE DBManager!
from bibliotheca.database.dbhelper import *

CnnString_Host  = 0
CnnString_DB    = 1
CnnString_User  = 2
CnnString_Pass  = 3

class BaseProvider(BaseObject,DBHelper): #, DBHelper):
    '''
    Provider Base for any of our three different servers.
    '''
    
    def __init__(self, **kw):
        '''
        Constructor
        '''
        
        # TODO:  Quick hack to maintain status quo.
        #        After 3 servers get defined, change this in a cleaner way
        #dict = {'cat': cat, 'environment': environment}
        BaseObject.__init__(self, **kw)
        
        # Reading specific file configuration for each server
        self.Print("Using config file <%s>" % (os.path.basename(self.Env.IniFile)))
        
        # This is the DB we'll access
        self._db      = None
        self._dbConnString = None
        self._sectionName = kw.get('sectionName', None)
        
    #--- -->GETTERS AND SETTERS
    def __GetIsDBInitialized(self):

        return isinstance(self.DB, pgdb.pgdbCnx)
#        return not self.DB is None
    
    def __GetDB(self):
        return self._db
    
    def __SetDB(self, value):
        self._db = value
    
    def __GetConnString(self):
        return self._dbConnString
 
    def __SetConnString(self, value):
        self._dbConnString = value
    
    def __GetDatabaseName(self):
        return self._dbConnString.split(':')[CnnString_DB]
    
    def __GetUser(self):
        return self._dbConnString.split(':')[CnnString_User]
    
    def __GetPlainPass(self):
        return self._dbConnString.split(':')[CnnString_Pass]
        
    def _get_session_object(self, UID):
        """
        Returns a valid session object. here we just create en empty session
        """

        try:
            Session = self.Env.Site.getSession(UID)
        except Exception, e:
            if self.Env is None:
                self.Error("[%s] - Could not recover sessionObject due to a None value for environment" % \
                           (self._get_session_object.__name__))
                #TODO: RAISE ERROR!
            
                if self.Env.Site is None:
                    self.Error("[%s] - Could not recover sessionObject due to a None value for Site" % \
                               (self._get_session_object.__name__))
                #TODO: RAISE ERROR!
        finally:
            Session = None
            
        return Session
    
    def _GetSectionName(self):
        ''' Return the Section in the config that stores connection configuration for System Provider
        '''
        return self._sectionName

    #--- -->PROPERTIES
    IsDBInitialized = property(__GetIsDBInitialized )    
    ConnectionString = property ( __GetConnString, __SetConnString)
    DB = property( __GetDB, __SetDB) 
    DatabaseName = property(__GetDatabaseName)
    User = property(__GetUser)
    PlainPass = property(__GetPlainPass)
    SessionObject = property ( _get_session_object )
    SectionName = property(_GetSectionName)
