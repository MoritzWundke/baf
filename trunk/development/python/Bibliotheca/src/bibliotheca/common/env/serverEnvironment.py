'''
Created on 08/07/2009

@author: Moss, Joze
'''
from environment import *

__STR__ = "ServerEnvironment"

# Server environment    
class ServerEnvironment(Environment):
    def __init__(self, **kw):
        
        #Update Server Enviroment requried fields to ensure they are passed to params together with
        #Any other layer. Helps to define more specific environments
        reqSections = kw.get('DefaultSections', []) + RequiredSections
        reqFields = kw.get('DefaultFields', dict())
        reqFields.update(EServer_RequiredFields)
        
        kw.update({ 'DefaultSections': reqSections,\
                    'DefaultFields':reqFields})
        
        Environment.__init__(self, **kw)

        # References to some global stuff
        self._Site = None
        self._server = None
        
    def __str__(self):
        return __STR__
    
#===============================================================================
#    def _InitializeConfig(self, path):
#        bReSave = Environment._InitializeConfig(self, path)
#        bReSave = self._CheckConfigSections(RequiredSections, EServer_RequiredFields)        
# 
#        # Save config if we need to    
#        if bReSave:
#            self.SaveConfigToDisk()
#            
#        return bReSave
#===============================================================================
    
    def _SetSite(self, value):
        self._Site = value
    
    def _GetSite(self):
        return self._Site
    
    Site = property ( _GetSite, _SetSite)
    
    def SetServer(self, server):
        self._server = server
    
    def GetServer(self):
        return self._server
    
    def UseAuthentication(self):
        return self.GetDictionaryObject(FE_AUTH, default=FE_AUTH_V[1])
    
    def IsClosed(self):
        return self.GetDictionaryObject(FE_CLOSED, default=FE_CLOSED_V[1])
    
    def IsUsingBlackIPBlacklist(self):
        return self.GetBoolConfig(Sect_Env, FE_USEBLACKLIST, EServer_FieldsEnv[FE_USEBLACKLIST])
    
    def Open(self):
        self._closed = False
        self.SetConfig(self._configSection, "closed", "False", True)
    
    def Close(self):
        self._closed = True
        self.SetConfig(self._configSection, "closed", "True", True)
