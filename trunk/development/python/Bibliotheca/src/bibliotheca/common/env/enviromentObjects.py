'''
Created on 05/08/2009

@author: joze

@summary: Structure that defines how enviroment objects should be loaded when they aren't present on the file.
'''

from bibliotheca.database import systemDBdefinitions
from bibliotheca.common.moduleCodes import *

#Sections that will be checked onto the INI File
Sect_Env            = 'env'
Sect_SystemDefaults = 'SystemDefaults'
Sect_SystemProvider = MODULE_PROVIDER_SYSTEM
Sect_Database       = 'Database'
Sect_PostGres       = MODULE_POSTGRES_CODE

# Sections that will be automatically handled.
# Only those whom are present here will be part of the environment object auto-handling
RequiredSections    = [Sect_Env, \
                       Sect_SystemDefaults, \
                       Sect_SystemProvider, \
                       Sect_Database, \
                       Sect_PostGres]

#===============================================================================
# ENVIROMENT ITEMS
#===============================================================================
# env section
FE_DEBUG                = 'debug'
FE_DEBUG_V              = [FE_DEBUG, False]
FE_LOGCAT               = 'logcat'
FE_LOGCAT_V             = [FE_LOGCAT, 'server|biblio']
FE_CHECKLOGCAT          = 'checklogcat'
FE_CHECKLOGCAT_V        = [FE_CHECKLOGCAT, False]
FE_LOGGING              = 'logging'
FE_LOGGING_V            = [FE_LOGGING, True]
FE_LOGFILEPREFIX        = 'logprefix'
FE_LOGFILEPREFIX_V      = [FE_LOGFILEPREFIX, 'biblio']
FE_LOG                  = 'log'
FE_LOG_V                = [FE_LOG, None]

# System Defaults properties
FSD_DatabaseInterface   = 'DatabaseInterfaceClass'

# PostGres Section
FPG_CLUSTERMODE         = 'clustermode'

# SystemProvider


# Fields attached to its values and sections, all the magic is done here.
Env_FieldsEnv           = dict( [FE_DEBUG_V,\
                                FE_LOGCAT_V ,\
                                FE_CHECKLOGCAT_V, \
                                FE_LOGGING_V,\
                                FE_LOGFILEPREFIX_V,\
                                FE_LOG_V,\
                                ])

Env_fieldsSystemDefaults    = { FSD_DatabaseInterface : Sect_PostGres }
Env_FieldsSystemDB          = {}
Env_FieldsContentDB         = {}
Env_FieldsPostgres          = { FPG_CLUSTERMODE : False }

Env_RequiredFields          = { Sect_Env : Env_FieldsEnv, \
                                Sect_SystemDefaults : Env_fieldsSystemDefaults, \
                                Sect_SystemProvider : Env_FieldsSystemDB,
                                Sect_Database : Env_FieldsContentDB,
                                Sect_PostGres : Env_FieldsPostgres}


#===============================================================================
# SERVER ENVIROMENT ITEMS
#===============================================================================
# env section
FE_AUTH                 = 'auth'
FE_AUTH_V               = [FE_AUTH, False]
FE_CLOSED               = 'closed'
FE_CLOSED_V             = [FE_CLOSED, False]
FE_USEBLACKLIST         = 'useblacklist'
FE_USEBLACKLIST_V       = [FE_USEBLACKLIST, True ] 

# SystemProvider and 
FSBD_SERVERNAME          = 'servername'
FSBD_SERVERNAME_V        = [FSBD_SERVERNAME, None]
FSBD_CONNECTIONSTRING    = 'connectionstring'
FSBD_CONNECTIONSTRING_V  = [FSBD_CONNECTIONSTRING, systemDBdefinitions.CONNECTSTRING_SYSTEM]
FSBD_HOST                = 'host'
FSBD_HOST_V              = [FSBD_HOST, None]
FSBD_ISLOCALSERVER       = 'islocalserver'
FSBD_ISLOCALSERVER_V     = [FSBD_ISLOCALSERVER, True]
FSBD_SCHEMAFILE          = 'schemafile'
FSBD_SCHEMAFILE_V        = [FSBD_SCHEMAFILE, None]

# Content Database Section
FCBD_SERVERNAME          = 'servername'
FCBD_SERVERNAME_V        = [FCBD_SERVERNAME, None]
FCBD_CONNECTIONSTRING    = 'connectionstring'
FCBD_CONNECTIONSTRING_V  = [FCBD_CONNECTIONSTRING, systemDBdefinitions.CONNECTSTRING_SYSTEM]
FCBD_HOST                = 'host'
FCBD_HOST_V              = [FCBD_HOST, None]
FCBD_ISLOCALSERVER       = 'islocalserver'
FCBD_ISLOCALSERVER_V     = [FCBD_ISLOCALSERVER, True]
FCBD_SCHEMAFILE          = 'schemafile'
FCBD_SCHEMAFILE_V        = [FCBD_SCHEMAFILE, None]


# Fields attached to its values and sections, all the magic is done here.
EServer_FieldsEnv               = dict([ FE_AUTH_V,\
                                        FE_CLOSED_V,\
                                        FE_USEBLACKLIST_V
                                        ])
EServer_fieldsSystemDefaults    = dict()
EServer_FieldsSystemDB          = dict([FSBD_SERVERNAME_V,\
                                        FSBD_CONNECTIONSTRING_V,\
                                        FSBD_HOST_V,\
                                        FSBD_ISLOCALSERVER_V,\
                                        FSBD_SCHEMAFILE_V
                                        ])

EServer_FieldsContentDB         = dict([FCBD_SERVERNAME_V,\
                                        FCBD_CONNECTIONSTRING_V,\
                                        FCBD_HOST_V,\
                                        FCBD_ISLOCALSERVER_V,\
                                        FCBD_SCHEMAFILE_V
                                        ])
EServer_FieldsPostgres          = {}

EServer_RequiredFields = { Sect_Env : EServer_FieldsEnv, \
                            Sect_SystemDefaults : EServer_fieldsSystemDefaults, \
                            Sect_SystemProvider : EServer_FieldsSystemDB,
                            Sect_Database : EServer_FieldsContentDB,
                            Sect_PostGres : EServer_FieldsPostgres}