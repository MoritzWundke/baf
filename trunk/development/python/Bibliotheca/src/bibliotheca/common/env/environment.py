'''
Created on 08/07/2009

@author: Moss
'''
import logging.handlers
import ConfigParser

from bibliotheca.common.biblioconfig import BiblioConfig
import bibliotheca.common.types as types
from enviromentObjects import *
import bibliotheca.common.version as version
#===============================================================================
# from bibliotheca.net.server.serverexceptions import *
#===============================================================================

__STR__ = "Environment"

class Environment(BiblioConfig):
    _configSection="env" 
    def __init__(self, **kw):
        # Init config
        
        reqSections = kw.get('DefaultSections', []) + RequiredSections
        reqFields = kw.get('DefaultFields', dict())
        
        reqFields.update(Env_RequiredFields)
            
        BiblioConfig.__init__(self, \
                               configFile=kw.get('configpath',None),
                               DefaultSections=reqSections,\
                               DefaultFields=reqFields)

        #TODO: EnvLog should be implemented as a class on his own
        basePath = str(kw.get('path',''))
        self.SetDictionaryObject(self._configSection, 'path', basePath)
        logid = '_' + str(kw.get('logid','0'))
        currentlogfile = basePath + 'bibliotheca/logs/'+ self.GetLogPrefix() + logid + '.log'
        
        self.SetDictionaryObject(self._configSection, FE_LOG, currentlogfile )
                
        self._envlog = logging.getLogger(types.LOG_NAME)
        self._envlog.setLevel(types.LOG_LEVEL)
        self._envloghandler = logging.handlers.RotatingFileHandler(currentlogfile,\
                                                                   maxBytes=types.LOG_MAXBYTES,\
                                                                   backupCount=types.LOG_BACKUPCOUNT)
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        self._envloghandler.setFormatter(formatter)
        self._envlog.addHandler(self._envloghandler)

        # Database Manager
        self._DBManager = None
        
    def __str__(self):
        return __STR__
    
    def ShouldCheckLogCat(self):
        return self.GetDictionaryObject(FE_CHECKLOGCAT, default=FE_CHECKLOGCAT_V[1])
            
    def Print(self, msg, cat):
        print("%s: %s" % (cat,msg))
            
    def debug(self, msg, cat):
        self._envlog.debug("%s: %s" % (cat,msg))
    
    def info(self, msg, cat):
        self._envlog.info("%s: %s" % (cat,msg))
    
    def warning(self, msg, cat):
        self._envlog.warning("%s: %s" % (cat,msg))
    
    def error(self, msg, cat):
        self._envlog.error("%s: %s" % (cat,msg))
    
    def critical(self, msg, cat):
        self._envlog.critical("%s: %s" % (cat,msg))
        
    #--- -->GETTERS AND SETTERS
    def _GetClusterMode(self):
        return self.GetDictionaryObject(FPG_CLUSTERMODE, default=FPG_CLUSTERMODE_V[1])
    
    def _GetDatabaseInterface(self):
        """ Return the Databse interface defined on enviromentObjects.py over 
        Sect_SystemDefaults section
        """
        return self.GetConfig(Sect_SystemDefaults, FSD_DatabaseInterface)
    
    def _GetSystemProvider(self):
        return self._systemProvider
    
    def _SetSystemProvider(self, value):
        
        if value is None:
            #TODO: Raise a custom exception!
            raise SystemProviderNotCreated
            return
        
        self._systemProvider = value
        
    def _GetDBManager(self):
        return self._DBManager
    
    def _SetDBManager(self, value):
        self._DBManager = value
    
    #--- -->PROPERTIES
    ClusterMode = property ( _GetClusterMode, None)
    SystemProvider = property(_GetSystemProvider, _SetSystemProvider)
    DatabaseInterfaceClass = property (_GetDatabaseInterface)
    DBManager = property (_GetDBManager, _SetDBManager)
    

    #--- -->METHODS
    def RegisterSystemProvider(self, value):
        '''
            @TODO: NEED TO CHECK CODE FLOW HERE!
            
            Operations while registering a Provider
            
            1.- Verify Connection
            2.- Verify Schema
        '''
        
        # Verifying Connection
        self.DBManager.InitializeConnection(provider=value)
        
        try:
            self.SystemProvider = value
        except Exception, e:
            print e
            print self.error('Please, check your connection parameters', 'Connection')
    
    #--- -->ACCESSORS    
    def GetEnvPath(self):
        #TODO: Add this value to the environmentObjects file, but no to environments defaults
        return self.GetDictionaryObject('path', default='')
    
    def GetLogFile(self):
        return self.GetDictionaryObject(FE_LOG, default=FE_LOG_V[1])
    
    def GetLogPrefix(self):
        return self.GetDictionaryObject(FE_LOGFILEPREFIX, default=FE_LOGFILEPREFIX_V[1])
    
    def GetConfigFilePath(self):
        return self.IniFile
    
    def GetVersion(self):
        return version.biblio_versionnumber
    
    def GetVersionName(self):
        return version.biblio_versionname
    
    def GetDBVersion(self):
        return version.bibliodb_versionnumber
    
    def GetDBVersionName(self):
        return version.bibliodb_versionname
    
    def GetRunMode(self):
        return self._runmode
    
    def IsLogging(self):
        return self.GetDictionaryObject(FE_LOGGING, default=FE_LOGGING_V[1])
    
    def IsDebugMode(self):
        return self.GetDictionaryObject(FE_DEBUG, default=FE_DEBUG_V[1])
    
    def IsReleaseMode(self):
        return not self._debug
    
    def GetLogCatList(self):
        return self.GetDictionaryObject(FE_LOGCAT, default=FE_LOGCAT_V[1])
    