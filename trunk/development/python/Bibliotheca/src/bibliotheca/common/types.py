import logging

# If true all prints will be logged too!
LOG_NAME = 'biblio'
LOG_LEVEL= logging.DEBUG
LOG_MAXBYTES = 200000
LOG_BACKUPCOUNT = 5

# Config file definitions
CONFIG_FILE = 'biblio.ini'

# List of user permissions
ADMIN = 0
PUBLISHER = ADMIN + 1
USER = ADMIN + 2
GUEST = ADMIN + 3
INVALID = ADMIN + 4

# Default user names
GUEST_NAME = "guest"