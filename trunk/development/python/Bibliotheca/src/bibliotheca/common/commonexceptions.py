'''
Created on 15/08/2009

@author: joze

@summary: Module where the Common modules exceptions will be handled
'''

from bibliotheca.common.bibExceptions.bibliothecaCustomExceptions import *
from bibliotheca.common.moduleCodes import *

#ERR0R DESCRIPTION
CMM_E00 = ['CODE_UnkownCommonModuleError', 'Undefined error occurred durring an operation']

#ERROR LISTING
CMM_ERROR_CODES = [CMM_E00, \
                  ]
DEFAULT_ERROR_CODE = CMM_E00

class commonException(bibCustomExceptions):
    """
    DBI management Exceptions
    """
    def __init__(self, **kw):
        """ CONSTRUCTOR
        """
        MODULE_CODE     = getModuleCode(MODULE_COMMON_CODE) 
        code = kw.get('code', DEFAULT_ERROR_CODE)
        
        if not code in CMM_ERROR_CODES:
            code = DEFAULT_ERROR_CODE
            
        message = kw.get('message', code[1])
        bibCustomExceptions.__init__(self, \
                                     moduleCode = MODULE_CODE, \
                                     exceptionCode = CMM_ERROR_CODES.index(code), \
                                     exceptionMessage = message)