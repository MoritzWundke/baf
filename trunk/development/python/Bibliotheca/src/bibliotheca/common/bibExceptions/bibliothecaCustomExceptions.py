'''
Created on 05/08/2009

@author: joze

@summary: Exception base class on Bibliotheca project
'''

UNDEFINED_MODULE_CODE = -1
UNDEFINED_EXCEPTION_CODE = -1
UNDEFINED_EXCEPTION_MESSAGE = "NODEFINED"

class bibCustomExceptions(Exception):
    '''
    This is a Base class where the regular methods should be implemented
    '''

    def __init__(self, **kw):
        '''
        Constructor
        '''
        Exception.__init__(self)
        
        self.moduleCode = kw.get('moduleCode',UNDEFINED_MODULE_CODE)
        self.exceptionCode = kw.get('exceptionCode',UNDEFINED_EXCEPTION_CODE)
        self.exceptionMessage = kw.get('exceptionMessage',UNDEFINED_EXCEPTION_MESSAGE)
        
    def __str__(self):
        return "[Error:%i] %s" % (self.moduleCode + self.exceptionCode, \
                            self.exceptionMessage)