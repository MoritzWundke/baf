"""
    @copyright: 

    @author: Joze Castellano, Moritz Wundke

    @summary: 
        Simple logger, just to get some basic loggin funcionality like categories different outputs and such.
    
    @change:    
        - Created on 31/08/2009: Part of refactor process and framework definition
        
    @todo: 
        - Add category logging
"""
import logging
import logging.handlers
import sys
import os

_DEFAULT_LOG_FORMAT = 'Biblio[%(module)s] %(levelname)s: %(message)s'

def get_default_log_format():
    return _DEFAULT_LOG_FORMAT

def get_escaped_default_log_format():
    return _DEFAULT_LOG_FORMAT.replace('%', '$')

def logger_factory(logtype='syslog', logfile=None, level='WARNING',
                   logid='biblio', format=None):
    logger = logging.getLogger(logid)
    logtype = logtype.lower()
    if logtype == 'file':
        hdlr = logging.FileHandler(logfile)
    elif logtype in ('winlog', 'eventlog', 'nteventlog'):
        # Requires win32 extensions
        hdlr = logging.handlers.NTEventLogHandler(logid,
                                                  logtype='Application')
    elif logtype in ('syslog', 'unix'):
        hdlr = logging.handlers.SysLogHandler('/dev/log')
    elif logtype in ('stderr'):
        hdlr = logging.StreamHandler(sys.stderr)
    else:
        hdlr = logging.handlers.BufferingHandler(0)
        # Note: this _really_ throws away log events, as a `MemoryHandler`
        # would keep _all_ records in case there's no target handler (a bug?)

    if not format:
        format = _DEFAULT_LOG_FORMAT
        if logtype in ('file', 'stderr'):
            format = '%(asctime)s ' + format
    datefmt = ''
    if logtype == 'stderr':
        datefmt = '%X'
    level = level.upper()
    if level in ('DEBUG', 'ALL'):
        logger.setLevel(logging.DEBUG)
    elif level == 'INFO':
        logger.setLevel(logging.INFO)
    elif level == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif level == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    else:
        logger.setLevel(logging.WARNING)
    formatter = logging.Formatter(format, datefmt)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)

    # Remember our handler so that we can remove it later
    logger._custom_handler = hdlr 

    return logger

class Log():
    """
    Logger class that handles several types of loggin including categories and such
    """

    def __init__(self, env):
        """
        Constructor for our logger. Will initialize all needed structures.

        Keyword arguments:
            cat -- category name where this log will print to (default info)
            env -- cached environment the logger will make use off (default None)
        
        Throws:
            CMM_E01 -- Needs an env in it's argument list
        """
        
        logtype = env.log_type
        logfile = env.log_file
        if logtype == 'file' and not os.path.isabs(logfile):
            logfile = os.path.join(env.get_logs_dir(), logfile)
        format = env.log_format
        if format:
            format = format.replace('$(', '%(') \
                     .replace('%(path)s', env.basepath) \
                     .replace('%(basename)s', os.path.basename(env.basepath)) \
                     .replace('%(project)s', env.project_name)
        self.logger = logger_factory(logtype, logfile, env.log_level, env.basepath,
                                  format=format)
    
    

