'''
    @copyright:

    @author: Joze Castellano, Moritz Wundke

    @summary:
        Config parser class. Use it to configure advanced methods that provide helpers
        for config (and any other file type in the future) reading and property configuration
    
    @change:   
        - Created on 31/08/2009: Part of refactor process and framework definition
'''

from ConfigParser import ConfigParser, NoSectionError
from bibliotheca.system.core.exceptions.commonexceptions import *
from bibliotheca.system.core.exceptions.customexceptions import CustomExceptions

class Parser():
    '''
    classdocs
    '''


    def __init__(self, **kw):
        '''
        Constructor
        '''
        
        self._config = None
        self._bReSave = False
        self._iniFile = kw.get('configFile', None)
        
        if not self._iniFile:
            raise CustomExceptions(code=CMM_E00)
        
#===============================================================================
#        self.ReadConfigFile()
#===============================================================================
        
    #--- -->Getters and Setters    
    def _GetIniFile(self):
        return self._iniFile
    
    def _SetIniFile(self, value):
        self._iniFile = value

    def _GetParserFileObject(self):
        return self._config
    
    def _SetParserFileObject(self, value):
        self._config = value
        
    def _GetResaveStatus(self):
        return self._bReSave
    
    def _SetResaveStatus(self, value):
        self._bReSave = value
            
     #--- -->Properties
    IniFile = property(_GetIniFile, _SetIniFile)
    Parser  = property(_GetParserFileObject, _SetParserFileObject)
    bResave = property(_GetResaveStatus, _SetResaveStatus)
    
    #--- -->METHODS
    def ReadConfigFile(self, **kw):
        """ Method for config file parsing

            We put each pair (key, value) in Parameters dictionary
        """
        
        # Dictionary where we're going to store both default or config params
        targetDict = kw.get('targetDict', None)

        self._config = ConfigParser()
        file_names = [self.IniFile,]

        for cfg in file_names:
            self.Parser.read( cfg )
            # Iterate over each config file entry
            for section in self.Parser.sections():
                if not targetDict is None:
                    targetDict[section] = dict()
                for option in self.Parser.options(section):
                    #This help us to read values wihtin a list
                    value = self.Parser.get(section, option).strip('[\'').strip('\']')
                    
                    if not targetDict is None:
                        targetDict[section][option] = value
                        
                    print "[%s] %s = %s" % (section, option, value)
                    
    def HasConfigOption(self, section, option):
        return self.Parser.has_option(section, option)
    
    def HasConfigSection(self, section):
        return self.Parser.has_section(section)
                   
    def AddNewSection(self, section):
        """ Add a new section toconfig File
        """
        if not self.Parser is None:
            self.Parser.add_section(section)
            self.bResave = True
            
    def AddNewOptionValue(self, section, option, value):
        """ Add a new section to config File
        """
        if not self.Parser is None:
            self.Parser.set(section, option, value)
            self.bResave = True
                      
    def SaveConfigToDisk(self):
        
        if self.bResave:
            ConfigFile = open(self.IniFile,'w')
            self.Parser.write(ConfigFile)
            ConfigFile.close()
            
    #--- --> Accessors
    def GetConfig(self, section, option, raw=True, vars=None):
        try:
            return self.Parser.get(section, option, raw, vars)
        except Exception, e:
            try:
                self.error("%s" % (e), "GetConfig")
            except:
                print("CRITICAL: %s" % e )
                
    def GetIntConfig(self, section, option, default):
        try:
            return self.Parser.getbint(section, option)
        except Exception, e:
            self.Print(e)
            return default                       
    
    def GetFloatConfig(self, section, option, default):
        try:
            return self.Parser.getfloat(section, option)
        except Exception, e:
            self.Print(e)
            return default   
        
    def GetBoolConfig(self, section, option, default):
        try:
            return self.Parser.getboolean(section, option)
        except Exception, e:
            #self.Print(e)
            return default
        
    def GetListConfig(self, section, option, raw=True, vars=None):
        try:
            return self.Parser.get(section, option, raw, vars).split("|")
        except Exception, e:
            self.Print(e)
            return
        
    def MoveOpt(self, section1, section2, option):
        try:
            self.Parser.set(section2, option, self._config.get(section1, option, True))
        except NoSectionError:
            # Create non-existent section
            self.Parser.add_section(section2)
            self.MoveOpt(self._config, section1, section2, option)
        else:
            self.Parser.remove_option(section1, option)
