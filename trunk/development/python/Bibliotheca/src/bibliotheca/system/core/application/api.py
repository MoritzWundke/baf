"""
    @copyright:

    @author: Joze Castellano, Moritz Wundke

    @summary:
        Interfaces related to application development
    
    @change:   
        - Created on 17/09/2009: Part of refactor process and framework definition
"""

from bibliotheca.system.core.components.component import Interface

__all__ = ['IApp']

class IApp(Interface):
    """
    Extension point that defines a component that will be handled as an application.
    Applications are components that run in an BAF environment
    """
    
    def app_main():
        """
        Main entry point for the application. The app should return a return code or None
        """