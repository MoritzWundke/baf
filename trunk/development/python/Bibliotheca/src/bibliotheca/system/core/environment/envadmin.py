"""
    @copyright:

    @author: Joze Castellano, Moritz Wundke

    @summary:
        Command line application to handle env's
    
    @change:   
        - Created on 02/09/2009: Part of refactor process and framework definition
"""

import cmd
from datetime import datetime
import getpass
import locale
import os
import pkg_resources
import shlex
import shutil
import StringIO
import sys
import time
import traceback
import urllib

from bibliotheca import __version__ as VERSION
from bibliotheca.system.core.util.date import parse_date, format_date, format_datetime, utc
from bibliotheca.system.core.util.texttransforms import to_unicode, wrap, unicode_quote, unicode_unquote, \
                           print_table, console_print, raw_input, _, ngettext, printerr, printout
from bibliotheca.system.core.exceptions.baseerror import BaseError
from bibliotheca.system.core.environment.environment import Environment
from bibliotheca.system.core.util.filehandling import copytree, zip_is_valid,purge_dir
from bibliotheca.system.core.config.config import _INI_FILENAME, get_true_values
from bibliotheca.system.core.application.application import SUCCESS_RET_CODES
    
def print_line():
        """
        Printa line :P
        """
        printout(_("""--------------------------------------------------------------------------"""))

# Our version numbers are handeled by setuptools
BIBLIO_VERSION = pkg_resources.get_distribution('bibliotheca').version

class RecoverBackupFileError(BaseError):
    """
    Simple error used when trying to recover an env from a backup file
    """
    title="[Recover Backup Error]"

class EnvAdmin(cmd.Cmd):
    """
    Bash application that helps us with our env's.
    """
    
    # Definitions we need (see cmd.Cmd for more info)
    intro = ''
    doc_header = 'Biblio Admin Console %(version)s\n' \
                 'Available Commands:\n' \
                 % {'version': BIBLIO_VERSION}
    ruler = ''
    prompt = "Biblio> "
    __env = None
    _date_format = '%Y-%m-%d'
    _datetime_format = '%Y-%m-%d %H:%M:%S'
    _date_format_hint = 'YYYY-MM-DD'
    
    def __init__(self, envdir=None):
        cmd.Cmd.__init__(self)
        self.interactive = False
        if envdir:
            self.env_set(os.path.abspath(envdir))
        self._permsys = None

    def emptyline(self):
        pass

    def onecmd(self, line):
        """`line` may be a `str` or an `unicode` object"""
        try:
            if isinstance(line, str):
                if self.interactive:
                    encoding = sys.stdin.encoding
                else:
                    encoding = locale.getpreferredencoding() # sys.argv
                line = to_unicode(line, encoding)
            if self.interactive:
                line = line.replace('\\', '\\\\')
            rv = cmd.Cmd.onecmd(self, line) or 0
        except SystemExit:
            raise
        except BaseError, e:
            printerr(_("Command failed:"), e)
            rv = 2
        if not self.interactive:
            return rv

    def run(self):
        self.interactive = True
        printout(_("""[Welcome to Bibliotheca Admin] %(version)s
Copyright (c) 2009 DarkCultureGames

Type:  '?' or 'help' for help on commands.
        """, version=BIBLIO_VERSION))
        self.cmdloop()
        
#===============================================================================
# Environmnet Helpers
#===============================================================================

    def set_env(self, name, env=None):
        """
        Set a new env to be used
        """
        if env is not None:
            self.__env = env
        self.envname =name
        
        # Set the new prompt :P
        self.prompt = "Biblio-[%s]> " % (self.envname)
        
    def open_env(self):
        """
        Opens the currently selected environment
        """
        try:
            if not self.__env:
                self.__env = Environment(self.envname)
        except Exception, e:
            printerr(_("Ups... I could not open the environment!"), e )
            traceback.print_exc()
            sys.exit(1)
    
    def ceck_env(self):
        """
        Will return true if the environment can be opened and false otherwise
        """
        try:
            self.__env = Environment(self.envname)
        except Exception, e:
            return False
        return True
 
#===============================================================================
# Miscs
#===============================================================================
 
    def unicod_safe_split (self, argstr):
        """
        `argstr` is an `unicode` string
        """
        return [unicode(token, 'utf-8')
                for token in shlex.split(argstr.encode('utf-8'))] or ['']
                
    def complete_word (self, text, words):
        return [a for a in words if a.startswith (text)]

    def print_help(cls, docs, stream=None):
        if stream is None:
            stream = sys.stdout
        if not docs: return
        for cmd, doc in docs:
            console_print(stream, cmd, newline=False)
            console_print(stream, '\t-- %s' % doc, newline=True)
    print_help = classmethod(print_help)

#===============================================================================
# Commands - Help
#===============================================================================

    _help_help = [('help','Show help')]
    
    def all_docs(cls):
        return (
                cls._help_help + 
                cls._help_quit + 
                cls._help_plugin +
                cls._help_createenv +
                cls._help_launchenv +
                cls._help_backup +
                cls._help_recover +
                cls._help_update + 
                cls._help_delete
                )
    
    all_docs = classmethod(all_docs)
    
    def do_help(self, line=None):
        arg = self.unicod_safe_split(line)
        if arg[0]:
            try:
                doc = getattr(self, "_help_" + arg[0])
                self.print_help(doc)
            except AttributeError:
                printerr(_("No documentation found for '%(cmd)s'", cmd=arg[0]))
        else:
            printout(_("biblio-admin - Admin Console %(version)s", version=BIBLIO_VERSION))
            if not self.interactive:
                print
                printout(_("Usage: biblio-admin </path/to/projenv> "
                           "[command [subcommand] [option ...]]\n")
                    )
                printout(_("Invoking biblio-admin without command starts "
                           "interactive mode."))
            self.print_help(self.all_docs())

#===============================================================================
# Commands - Exit (EOF or quit)
#===============================================================================

    _help_quit = [['quit', 'Exit']]
    _help_exit = _help_quit
    _help_EOF = _help_quit
    
    def do_quit(self, line):
        printout(_("\nSee you soon!"))
        sys.exit()
        
    # make aliasses for the others
    do_exit = do_quit
    do_EOF = do_quit
    
#===============================================================================
# Commands - Plugin stuff (admin what we got enabled and what not :P)
#===============================================================================

    _help_plugin = [
                    ('plugin list','Show a list of available plugins'),
                    ('plugin setapp <name>','Will set a plugin to be the standart app to run in this environment ')
                    ]
    
    def complete_plugin(self, text, line, begidx, endidx):
        comp = ['list', 'setapp']
        return self.word_complete(text, comp)
    
    # TODO: Make plugins configurable here!
    def do_plugin(self, line):
        args = self.unicod_safe_split(line)
        if args[0] == 'list':
            printout(_("TODO: register plugins"))
        elif args[0] == 'setapp' and len(args)==2:
            printout(_("TODO: set app to life in env"))
        else:
            self.do_help ('plugin')

#===============================================================================
# Commands - Environment launching
# TODO: Finish components/config in environment
# TODO: Add daemonize process into launch option!
#===============================================================================
    _help_launchenv = [('launchenv','Launch app in env (default app)')]
    
    def do_launchenv(self, line):
        """
        launch the current app or a different one in the env.
        Different apps are usful to testing reasons!
        """
        def launchenv_error(msg):
            printerr(_("Launchenv for '%(env)s' failed:", env=self.envname),
                     "\n", msg)
        
        # Open env
        if not self.ceck_env():
            launchenv_error("Environment not created or corrupted!")
            return 2

        # launch application
        retcode = self.__env.launch_main_app()
        if retcode in SUCCESS_RET_CODES:
            print("Executed successfully!")
        else:
            print("Execution failed!")
                
#===============================================================================
# Commands - Environment Creation
#===============================================================================
            
    _help_createenv = [
                       ('createenv','Create and initialize a new environment interactively'),
                       ('createenv </path/to/project> <project_name> <targetapp>','Create and initialize a new environment')
                      ]
    
    def get_createenv_data(self):
        returnvals = []
        
        # Basic instructions
        print_line()
        printout(_("""Starting Interactive environment creation at <%(envname)s>

Hi mate! This is the interactive guide on environment creation.
Just follow the instructions and all go fine!""",
                   envname=self.envname))
        print_line()
        
        # Env path           
        printout(_(""" Now enter the absolute path where your environment should be created."""))
        path = self.envname
        returnvals.append(raw_input(_("</path/to/project> [%(default)s]> ",
                                      default=path)).strip() or path)
        print_line()
        
        # Environment app
        printout(_(""" Enter the name of the project you are about to create"""))
        path = "My Project"
        returnvals.append(raw_input(_("project_name [%(default)s]> ",
                                      default=path)).strip() or path)
        print_line()
        return returnvals
    
    def do_createenv(self, line):
        """
        Create environment by passed arguments or interactively
        """
        def createenv_error(msg):
            printerr(_("Createenv for '%(env)s' failed:", env=self.envname),
                     "\n", msg)
  
        if self.ceck_env():
            createenv_error("Environment already created in specified path!")
            return 2

        if os.path.exists(self.envname) and os.listdir(self.envname):
            createenv_error("Target folder not empty!")
            return 2
        
        project_name = None
        # get arguments
        args = self.unicod_safe_split(line)
        if len(args) == 1 and not args[0]:
            returnvals = self.get_createenv_data()
            path, project_name = returnvals
        elif len(args) != 2:
            createenv_error('Wrong number of arguments: %d' % len(args))
            return 2
        else:
            path, project_name = args[:2]
            
        try:
            # Uppdate promt and internal stuff            
            self.set_env(path)
            
            # Start env creation
            printout(_("Creating and Initializing Environment"))
            options = [
                ('env', 'path', self.envname),
                ('project', 'name', project_name),
            ]
            
            # OVER-WRITE THESE OPTIONS from a file 
            
            try:
                self.__env = Environment(self.envname, create=True, args=options)
            except Exception, e:
                # Bad thing happened!
                createenv_error('Failed to create environment. Created files will be deleted')
                printerr(e)
                traceback.print_exc()
                purge_dir( path )     
                sys.exit(1)
                
        except Exception, e:
            createenv_error(to_unicode(e))
            traceback.print_exc()
            return 2
        
        print_line()
        printout(_("""Project environment for '%(project_name)s' created.

You may now configure the environment by editing the file:

  %(config_path)s

For more info and documentation about BAF visit:

  http://www.darkcukturegames.com

Have a nice day!
""", project_name=project_name, project_path=self.envname,
           project_dir=os.path.basename(self.envname),
           config_path=os.path.join(self.__env.get_configs_dir(), _INI_FILENAME)))

#===============================================================================
# Update environment
#===============================================================================
        
    _help_update = [
                    ('update', 'Update environment interactively'),
                    ('update </path/to/project>', 'Update environment')
                   ]
    def do_update(self, line):
        pass

#===============================================================================
# Delete environment
#===============================================================================
        
    _help_delete = [
                    ('delete', 'Delete environment interactively'),
                    ('delete </path/to/project>', 'Delete environment')
                   ]
    
    def get_delete_data(self):
        returnvals = []
        # Basic instructions
        print_line()
        printout(_("""Starting Interactive environment deletion at <%(envname)s>""",
                   envname=self.envname))
        print_line()
        
        # Env path           
        printout(_(""" Now enter the absolute path to the environment that will be deleted."""))
        path = self.envname
        returnvals.append(raw_input(_("</path/to/project> [%(default)s]> ",
                                      default=path)).strip() or path)
        print_line()
        return returnvals
    
    def do_delete(self, line):
        def delete_error(msg):
            printerr(_("Delete for '%(env)s' failed:", env=self.envname),
                     "\n", msg)
        
        # get arguments
        env_dir = None
        args = self.unicod_safe_split(line)
        
        # Interactive or not
        interactive_delete = False
        
        if len(args) == 1 and not args[0]:
            returnvals = self.get_delete_data()
            env_dir = returnvals[0]
            interactive_delete = True         
        elif len(args) != 1:
            delete_error('Wrong number of arguments: %d' % len(args))
            return 2
        else:
            env_dir = args[0]
        
        # Set the right env dir
        self.envname = env_dir
        
        # Open env
        if not self.ceck_env():
            delete_error("Environment not created or corrupted!")
            return 2
        
        if interactive_delete:
            no_option = "no"
            value = raw_input(_("Delete Environment? [%(default)s]> ", 
                                default=no_option)).strip() or no_option
            # Ask again if we want to delete the env!
            value = value.lower() in get_true_values()
            if not value:
                printout(_("Delete Canceled!"))
                return
        
        # Delete it!
        printout(_("Starting Delete Process"))
        self.__env.delete()
        
        # It's deleted, so purge it!
        printout(_(" Purge env directory %(purge_dir)s", purge_dir=self.envname))
        purge_dir(self.envname)
        printout(_("Environment successfully deleted"))

#===============================================================================
# Backup environment
#===============================================================================
        
    _help_backup = [
                       ('backup','backup an environment interactively'),
                       ('backup </path/to/project> <dest_file>','backup an environment')
                      ]
    
    def get_backup_data(self):
        returnvals = []
        # Basic instructions
        print_line()
        printout(_("""Starting Interactive environment backup at <%(envname)s>

Follow the instructions to backup the environment!""",
                   envname=self.envname))
        print_line()
        
        # Env path           
        printout(_(""" Now enter the absolute path to the environment that you will backup."""))
        path = self.envname
        returnvals.append(raw_input(_("</path/to/project> [%(default)s]> ",
                                      default=path)).strip() or path)
        print_line()
        
        # Environment app
        printout(_(""" Destination file for tghe backup"""))
        dest = self.__env.backup_get_default_file_name()
        returnvals.append(raw_input(_("</dest/file> [%(default)s]> ",
                                      default=dest)).strip() or dest)
        
        return returnvals
    
    def do_backup(self, line):
        """
        backup the current env
        """
        def backup_error(msg):
            printerr(_("Backup for '%(env)s' failed:", env=self.envname),
                     "\n", msg)
        
        # Open env
        if not self.ceck_env():
            backup_error("Environment not created or corrupted!")
            return 2

        # get arguments
        backup_source = None
        backup_dest = None
        args = self.unicod_safe_split(line)
        
        if len(args) == 1 and not args[0]:
            returnvals = self.get_backup_data()
            backup_source, backup_dest = returnvals            
        elif len(args) != 2:
            backup_error('Wrong number of arguments: %d' % len(args))
            return 2
        else:
            backup_source, backup_dest = args[:2]
        
        self.__env.backup(backup_source, backup_dest)

#===============================================================================
# Recover environment from backup
#===============================================================================
    _help_recover = [
                       ('recover','recover an environment interactively'),
                       ('recover </path/to/project> <backup_file>','recover an environment')
                      ]
    
    def get_recover_data(self):
        returnvals = []
        # Basic instructions
        print_line()
        printout(_("""Starting Interactive environment recovering at <%(envname)s>

Follow the instructions to recover environment!""",
                   envname=self.envname))
        print_line()
        
        # Env path           
        printout(_(""" Now enter the absolute path where your environment should be created."""))
        path = self.envname
        returnvals.append(raw_input(_("</path/to/project> [%(default)s]> ",
                                      default=path)).strip() or path)
        print_line()
        
        # Environment app
        printout(_(""" Enter path where the backup file is locates"""))
        prompt = _("Backup File [</path/to/file>]> ")
        returnvals.append(raw_input(prompt).strip())
        
        return returnvals
    
    def recover_env(self, backupfile, dest_env=None, ):
        """
        recreate an env from a backup file
        """
        if zip_is_valid( backupfile ):
            if not dest_env:
                dest_env = self.envname
            print_line()
            printout(_("Starting recovery"))
            printout(_("\trecovery file: \t%(backupfile)s",backupfile=backupfile))
            printout(_("\tenv path: \t%(dest_env)s",dest_env=dest_env))
            
            #TODO: call self.__env.restored() on the recovered env!
        else:
            raise RecoverBackupFileError(_("Invalid zip file for recovery!"))
        
    def do_recover(self, line):
        """
        backup the current env
        """            
        def recover_error(msg):
            printerr(_("Recover for '%(env)s' failed:", env=self.envname),
                     "\n", msg)

        # get arguments
        backup_file = None
        dest_env = None
        args = self.unicod_safe_split(line)
        if len(args) == 1 and not args[0]:
            returnvals = self.get_recover_data()
            dest_env, backup_file = returnvals            
        elif len(args) != 2:
            recover_error('Wrong number of arguments: %d' % len(args))
            return 2
        else:
            dest_env, backup_file = args[:2]
               
        self.recover_env(backup_file, dest_env)
#===============================================================================
# This will run the whole magic
#===============================================================================

def main(args=None):
    if args is None:
        args = sys.argv[1:]
    admin = EnvAdmin()
    
    
    if len(args) > 0:
        if args[0] in ('-h', '--help', 'help'):
            return admin.onecmd('help')
        elif args[0] in ('-v','--version'):
            printout(os.path.basename(sys.argv[0]), BIBLIO_VERSION)
        else:
            env_path = os.path.abspath(args[0])
            try:
                unicode(env_path, 'ascii')
            except UnicodeDecodeError:
                printerr(_("non-ascii environment path '%(path)s' not "
                           "supported.", path=env_path))
                sys.exit(2)
            admin.set_env(env_path)
            if len(args) > 1:
                s_args = ' '.join(["'%s'" % c for c in args[2:]])
                command = args[1] + ' ' +s_args
                return admin.onecmd(command)
            else:
                while True:
                    try:
                        admin.run()
                    except KeyboardInterrupt:
                        admin.do_quit('')
    else:
        return admin.onecmd("help")


if __name__ == '__main__':
    pkg_resources.require('bibliotheca==%s' % VERSION)
    sys.exit(main())