'''
    @copyright: 

    @author: Joze Castellano, Moritz Wundke

    @summary: 
        Base custom exception exception class. This is used for automatic module specific exceptions
    
    @change:   
        - Created on 31/08/2009: Part of refactor process and framework definition
'''

UNDEFINED_MODULE_CODE = -1
UNDEFINED_EXCEPTION_CODE = -1
UNDEFINED_EXCEPTION_MESSAGE = "NODEFINED"

class CustomExceptions(Exception):
    '''
    This is a Base class where the regular methods should be implemented
    '''

    def __init__(self, **kw):
        '''
        Constructor
        '''
        Exception.__init__(self)
        
        self.moduleCode = kw.get('moduleCode',UNDEFINED_MODULE_CODE)
        self.exceptionCode = kw.get('exceptionCode',UNDEFINED_EXCEPTION_CODE)
        self.exceptionMessage = kw.get('exceptionMessage',UNDEFINED_EXCEPTION_MESSAGE)
        
    def __str__(self):
        return "[Error:%i] %s" % (self.moduleCode + self.exceptionCode, \
                            self.exceptionMessage)
    
    def __unicode__(self):
        return unicode(self.__str__())
