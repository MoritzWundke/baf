'''
    @copyright: 

    @author:  Joze Castellano, Moritz Wundke

    @summary: 
        Module where the Common modules exceptions will be handled
    
    @change:  
        - Created on 31/08/2009: Part of refactor process and framework definition
        - Update on 01/09/2009: Fixed syntax errors.
'''

from bibliotheca.system.core.exceptions.customexceptions import CustomExceptions

# TODO @Joze: Make this more modular
from bibliotheca.system.core.constants.modulecodes import *

#ERR0R DESCRIPTION
CMM_E00 = ['CODE_UnkownCommonModuleError', 'Undefined error occurred durring an operation']
CMM_E01 = ['CODE_NoEnvironmnetError', 'No Environment found in constructor arguments, this occurs when you are '\
           'trying to cretae an instance of an BaseObject like class without and environment in it\' argument list']

#ERROR LISTING
CMM_ERROR_CODES = [CMM_E00, \
                   CMM_E01 ]
DEFAULT_ERROR_CODE = CMM_E00

class CommonException(CustomExceptions):
    """
    DBI management Exceptions
    """
    def __init__(self, **kw):
        """ CONSTRUCTOR
        """
        MODULE_CODE     = getModuleCode(MODULE_COMMON_CODE) 
        code = kw.get('code', DEFAULT_ERROR_CODE)
        
        if not code in CMM_ERROR_CODES:
            code = DEFAULT_ERROR_CODE
            
        message = kw.get('message', code[1])
        CustomExceptions.__init__(self, \
                                     moduleCode = MODULE_CODE, \
                                     exceptionCode = CMM_ERROR_CODES.index(code), \
                                     exceptionMessage = message)
