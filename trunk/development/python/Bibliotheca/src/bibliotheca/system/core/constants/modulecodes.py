'''
    @Owner

    @Author Joze Castellano, Moritz Wundke

    @Description 
        Module that will ensure a proper ID per each project module
    
    @History
        - Created on 05/08/2009: First approach    
        - Refactored on 31/08/2009: Part of refactor process and framework definition
'''

# BASIC CONSTS
MODULE_INITIAL = 1
MODULE_GAP = 1000
MODULE_ERROR_GAP = 700 #Not used yet

# MODULE LISTING
MODULE_POSTGRES_CODE    = 'PostgreSQL'
#TODO: put names to these module
MODULE_DBI_CODE         = 1
MODULE_COMMON_CODE      = 2
MODULE_PROVIDER_SYSTEM  = 'SystemProvider'

DEFINED_MODULES = [MODULE_POSTGRES_CODE, \
                   MODULE_DBI_CODE,\
                   MODULE_COMMON_CODE,\
                   MODULE_PROVIDER_SYSTEM,\
                   ]
#TODO: CREATE AN AUTOREGISTRANT MODULE INTERFACE TO AVOID HANDLING IT MANUALLY

def getModuleCode( modCode ):
    """ Return the proper code for each requested module
    """
    
    if not modCode in DEFINED_MODULES:
        raise "Module Code provided not implemented"
        return 0
    
    return ( DEFINED_MODULES.index(modCode) + MODULE_INITIAL ) * MODULE_GAP 
