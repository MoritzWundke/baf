'''
Created on 10/07/2009

@author: Moss, Joze
'''

# System imports
import sys
import os

# Bibliotheca imports
from bibliotheca.common.daemonize import *;
from bibliotheca.common.modes import *;
from bibliotheca.net.server.run import *;
from bibliotheca.net.server.serverexceptions import *;

def determine_path():
    """Borrowed from wxglade.py"""
    try:
        root = __file__
        if os.path.islink (root):
            root = os.path.realpath (root)
        basePath = os.path.dirname (os.path.abspath (root))
        return basePath.replace('\\','/') + '/';
    except Exception, e:
        print("I'm sorry, but something is wrong %s." % (e))
        print("There is no __file__ variable. Please contact the author.")
        sys.exit()

def help():
    print(" ====[HELP ]====")
    #for cmd in cmmds:
    #    print(" %s" % (cmd))
        
def CheckDaemonMode( args ):
    """ Return True or False for daemon mode"""
    
    returnValue = False
    for opt in OPT_DAEMON:
        if opt in args:
            returnValue = True
            break
        
    return returnValue

def CheckServerMode( args ):
    """ Return True or False for daemon mode"""
    
    returnValue = OPT_UndefServer 
    for serverType in OPT_SERVERS:
        for serverFlag in serverType:
            if serverFlag in args:
                returnValue = serverType
                break
        if ( returnValue != OPT_UndefServer ):
            break
        
    return returnValue

def RunTests( args ):
    """ Here we switch to test mode
        parameter should be:
        - 'python bibliotheca.py test' for default verbosity(2)
        - 'python bibliotheca.py test=[0-2]' if you want to override verbosity
    """
    testFlag='--test'
    xmlFlag='--xml'
    pathFlag='--path'
    verbosityDefault = '2'
    xmlDefault = False
    pathDefault = '.'
    
#===============================================================================
#    print "ARGS:",args
#    sys.stdout.write("%s\n" % (str(args)))
#===============================================================================
    
    verbosityValue = None
    bUseXML = None
    sPathValue = None
    for flag in args:
#        print flag
        if xmlFlag in flag :
            bUseXML = True
            
        if verbosityValue is None and testFlag in flag:
            verbosityValue = flag.partition('=')[2]
            if verbosityValue == "":
                verbosityValue = verbosityDefault
                
        if sPathValue is None and pathFlag in flag:
            sPathValue = flag.partition('=')[2]
            if sPathValue == "":
                sPathValue = pathDefault
    
    if bUseXML is None:
        bUseXML = xmlDefault
        
    if verbosityValue is None:
        return
    
    if sPathValue is None:
        sPathValue = pathDefault
    
    from bibliotheca.tools.unittest import test_bibliotheca
    test_bibliotheca.main(verbosity=int(verbosityValue),\
                          xmlOutput=bUseXML,\
                          path=sPathValue)
        
        
def main( args ):
    
    RunTests(args)
    BasePath = determine_path()
    DaemonMode = CheckDaemonMode( args )
	#TODO: Currently not using this value, check usability on the future!
    #actionResult = -1

    try:
        ServerMode = CheckServerMode( args )
    except Exception, e:
        print "Error Checking server mode"
        ServerMode = OPT_UndefServer
    
    if ( ServerMode == OPT_UndefServer ):
        print "Exiting..."
        exit()
    
    if ( ServerMode in OPT_DaemonCapableServer and DaemonMode ):
        actionResult = createDaemon(BasePath)
        
        if ( actionResult != 0 ):
            raise OsProcessCouldNotBeenDeamonized
        
    if ( ServerMode == OPT_sLogin ):
        RunLoginServer( basePath=BasePath, \
                        daemonMode=DaemonMode )
    elif ( ServerMode == OPT_sContent ):
        RunContentServer( basePath=BasePath, \
                        daemonMode=DaemonMode )
    elif ( ServerMode == OPT_sAdmin ):
        RunServerAdmin( BasePath )
    else:
        help()
    
#===============================================================================
#    procParams = """
#   return code = %s
#   process ID = %s
#   parent process ID = %s
#   process group ID = %s
#   session ID = %s
#   user ID = %s
#   effective user ID = %s
#   real group ID = %s
#   effective group ID = %s
#   """ % (actionResult, os.getpid(), os.getppid(), os.getpgrp(), os.getsid(0),
#   os.getuid(), os.geteuid(), os.getgid(), os.getegid())
# 
#    print procParams
#===============================================================================

if __name__ == '__main__':
    main( sys.argv[1:] )

