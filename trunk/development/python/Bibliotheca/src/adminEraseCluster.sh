#!/bin/sh
echo "Stoping cluster $1"
sudo -u postgres /usr/lib/postgresql/8.3/bin/pg_ctl -D /prod/BBDDs/$1 -m smart stop
sudo rm -Rf /etc/postgresql/8.3/$1
sudo rm -Rf /prod/BBDDs/$1
