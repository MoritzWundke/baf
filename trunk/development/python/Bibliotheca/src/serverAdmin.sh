#!/bin/bash

# Server shell execute

if [[ -f bibliotheca.py ]]; then
	echo "Launching server with $1 ..."
    python bibliotheca.py runserveradmin
else
    echo "Entry point does not found."
fi
