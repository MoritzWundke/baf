# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 DarkCultureGames
# All rights reserved.
#
# Bibliotheca Server administrative console
#
# Author: Moritz Wundke <b_thax@darkculturegames.com>
# Change: 04/10/2009 - Created

import pkg_resources
import sys
import os

from bibliothecaserver import __version__ as VERSION
from baf.core.environment.envadmin import EnvAdmin
from baf.core.util.texttransforms import _, printout

class BiblioServerAdmin(EnvAdmin):
    """
    Administrative console for Bibliotheca servers
    """
    
    # Definitions we need (see cmd.Cmd for more info)
    doc_header = 'Bibliotheca Server Admin Console %(version)s\n' \
                 'Available Commands:\n' \
                 % {'version': VERSION}
    prompt = "BAF> "
    
    def print_interactive_header(self):
        printout(_("""[Welcome to Bibliotheca Server Admin] %(version)s
Copyright (c) 2009 DarkCultureGames

Type:  '?' or 'help' for help on commands.
        """, version=VERSION))
    
    def print_version(self):
        """
        Print version of app
        """
        printout(os.path.basename(sys.argv[0]), VERSION)

#===============================================================================
# Commands - Help
#===============================================================================
    def print_help_header(self):
        printout(_("biblio-admin"))
    
    def print_usage_help(self):
        printout(_("Usage: biblio-admin </path/to/projenv> "
                   "[command [subcommand] [option ...]]\n")
            )
        printout(_("Invoking biblio-admin without command starts "
                   "interactive mode."))
        
    def all_docs(cls):
        """
        Add specifc help content
        """
        return ( EnvAdmin.all_docs() )
    
    all_docs = classmethod(all_docs)
    
def main(args=None):
    admin = BiblioServerAdmin()
    admin.main(args)

if __name__ == '__main__':
    pkg_resources.require('BibliothecaServer==%s' % VERSION)
    sys.exit(main())