# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 DarkCultureGames
# All rights reserved.
#
# Permission subsytem api
#
# Author: Moritz Wundke <b_thax@darkculturegames.com>
# Change: 04/10/2009 - Created

from baf.core.components.component import Interface

__all__ = ['IPermissionCreator','IPermissionStore']

class IPermissionCreator(Interface):
    """
    Extension used to add new permissions to the system
    """
    
    def get_permissions():
        """
        Will be called by the permission subsystem.
        
        Returns a list of permissions that needs to be handled. Can be a 
        list of simple items or tuples (permission,permissionlist). If a permission is a tupple it's
        used to group permissions
        """

class IPermissionGroupCreator(Interface):
    """
    Extension point to create user groups to which permissions can be applied
    """
    
    def get_group():
        """
        Will be called by the permission subsystem to add new usergroups
        """

class IPermissionStore(Interface):
    """
    Extension point that defines storage of user permissions
    """

    def get_permissions_by_user(username):
        """
        Returns a list of all permissions paired by a boolean value to define
        if the user has or has not a specific permission
        """

    def get_users_by_permissions(permissions):
        """
        Returns a list of users that do have any of the specified permissions
        """
    
    def get_usergroups_by_user(username):
        """
        Return a list of groups this user is a member of
        """

    def get_all_permissions():
        """
        Returns a list of all permissions that are stored for each user
        """
    
    def give_permission_to(username, permission):
        """
        Gives a permission to a specific user (user can be a group name!)
        """

    def remove_permission_from(username, permission):
        """
        Removes a permission from a specific user (user can be a group name!)
        """

class IPermissionHandler(Interface):
    """
    Our permission system is based on a handler chain. Every component in the chain
    implement the IPermissionHandler. 
    
    A permission handler can return 3 values:
     * True: Permission granted!
     * False: Permission failed!
     * None: not decided, next element in the chain will treat it
    """
    
    def check_permission(permission, username):
        """
        Look if we can grant access to the specific permission by the user.
        If granted return True, False if not and None if we could not decide
        """
