#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 DarkCultureGames
# All rights reserved.

from setuptools import setup, find_packages

setup(
    name = 'BibliothecaServer',
    version = '0.0.1',
    description = 'Bibliotheca Server Application',
    long_description = """Server Application for Bibliotheca""",
    author = 'DarkCultureGames',
    author_email = 'info@darkculturegames.com',
    license = 'BSD',
    url = 'http://bibliotheca.darkculturegames.com/',
    download_url = 'http://baf.darkculturegames.com/',
    classifiers = [
        'Environment :: General Purpose',
        'Framework :: BibliothecaServer',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Server Development',
    ],
    
    # Exclude stuff: exclude=['*.tests']
    packages = find_packages(),
    
    # None by now but who knows
    #package_data = {},

    #test_suite = 'bibliotheca.tools.unittest.TestBibliotheca',
    zip_safe = False,

    install_requires = [
        'BibliothecaApplicationFramework>=0.0.1'
    ],
    
    # example: 'Pygments': ['Pygments>=0.6'],
    #extras_require = { },

    entry_points = """
        [console_scripts]
        biblioserver-admin = bibliothecaserver.tools.biblioadmin:main

        [baf.components]
    """,
)
